class Alumno {
    // propiedades
    nombre;
    edad;
    poblacion;
    genero;
    datos() {
        return this.nombre + " " + this.edad
    }
    constructor(nombre = "", edad = 0, poblacion = "", genero = "") {
        this.nombre = nombre;
        this.edad = edad;
        this.poblacion = poblacion;
        this.genero = genero;
    }

}


const alumno1 = new Alumno();
const alumno2 = new Alumno('silvia', 33, 'santander', 'femenino');
const alumno3 = new Alumno("pepe");

alumno1.nombre = 'ramon';
alumno1.edad = 23;
alumno1.poblacion = 'santander';
alumno1.genero = 'masculino';

console.log(alumno1.datos());
console.log(alumno2.datos());

