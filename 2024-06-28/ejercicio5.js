// tengo estos datos
/*
const datos = [
    // 1 registro
    {
        id: 1,
        nombre: 'xfg34'

    },
    // 2 registro
    {
        id: 3,
        nombre: 'JKH67'

    },
    // 3 registro
    {
        id: 8,
        nombre: 'ABC90'

    }
];
*/
// mostrar todos los registro con esta estructura
/*
<div>
    <p>1</p>
    <p>xfg34</p>
</div>
<div>
    <p>3</p>
    <p>JKH67</p>
</div>
<div>
    <p>8</p>
    <p>ABC90</p>
</div>

// ejemplo 
// para 1 registro

const div1 = document.createElement('div');
const p1 = document.createElement('p');
const p2 = document.createElement('p');
div1.appendChild(p1);
div1.appendChild(p2);
p1.textContent = '1';
p2.textContent = 'xfg34';
document.body.appendChild(div1);

*/

// necesito datos

const datos = [
    // 1 registro
    {
        id: 1,
        nombre: 'xfg34'

    },
    // 2 registro
    {
        id: 3,
        nombre: 'JKH67'

    },
    // 3 registro
    {
        id: 8,
        nombre: 'ABC90'

    }
];

// recorrer el array de datos

datos.forEach(function (registro) {
    // ahora registro es un objeto
    // creo la estructura de elementos para rellenar con los datos

    const estructura = {
        div1: document.createElement('div'),
        p1: document.createElement('p'),
        p2: document.createElement('p')
    }

    estructura.div1.appendChild(estructura.p1);
    estructura.div1.appendChild(estructura.p2);

    // meto los datos del objeto en los p

    estructura.p1.textContent = registro.id;
    estructura.p2.textContent = registro.nombre;

    // añado el div al body

    document.body.appendChild(estructura.div1);
});