class Alumno {
    nombre = '';
    edad = 0;
    poblacion = "";
    genero = "";
    datos() {
        return this.nombre + " " + this.edad
    }
}


const alumno1 = new Alumno();
const alumno2 = new Alumno();

alumno1.nombre = 'ramon';
alumno1.edad = 23;
alumno1.poblacion = 'santander';
alumno1.genero = 'masculino';

alumno2.nombre = 'maria';
alumno2.edad = 25;
alumno2.poblacion = 'santander';
alumno2.genero = 'femenino';

console.log(alumno1.datos());
console.log(alumno2.datos());

