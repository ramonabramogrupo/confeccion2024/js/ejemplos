// quiero mostrar los datos recibidos del servidor API REST
// en mi pagina

fetch("https://gorest.co.in/public/v2/users/6992066")
    .then(response => response.text())
    .then(function (data) {
        // convierto los datos recibidos desde el servidor en un objeto
        // JSON
        const json = data;


        // convierto el objeto JSON en un objeto
        const datos = JSON.parse(json);

        // ya tienes los datos en un objeto para poderlos mostrar
        // opcion 1 
        // utilizando una constante por cada li
        // const liId = document.querySelector('#id');
        // const liNombre= document.querySelector('#nombre');
        // const liEmail = document.querySelector('#email');
        // const liGenero = document.querySelector('#genero');
        // const liStatus = document.querySelector('#status');

        // // para escribir
        // liId.textContent = datos.id;
        // liNombre.textContent = datos.name;
        // liEmail.textContent = datos.email;
        // liGenero.textContent = datos.gender;
        // liStatus.textContent = datos.status;


        // utilizando una constante que apunte a todos los li

        // Nodelist
        // const lis = document.querySelectorAll("li");

        // array de nodos
        // const lis = [
        //     document.querySelector('#id'),
        //  lis[0] ==> document.querySelector("#id")
        //     document.querySelector('#nombre'),
        //     document.querySelector('#email'),
        //     document.querySelector('#genero'),
        //     document.querySelector('#status'),
        // ];


        // lis[0].textContent = datos.id;
        // lis[1].textContent = datos.name;
        // lis[2].textContent = datos.email;
        // lis[3].textContent = datos.gender;
        // lis[4].textContent = datos.status;


        // utilizando un objeto
        const lisObjeto = {
            id: document.querySelector('#id'),
            // lisObjeto.id ==> document.querySelector("#id")
            nombre: document.querySelector('#nombre'),
            email: document.querySelector('#email'),
            genero: document.querySelector('#genero'),
            status: document.querySelector('#status'),

        };

        lisObjeto.id.textContent = datos.id;
        lisObjeto.nombre.textContent = datos.name;
        lisObjeto.email.textContent = datos.email;
        lisObjeto.genero.textContent = datos.gender;
        lisObjeto.status.textContent = datos.status;




    });

// lo que realice aqui tiene un problema y es que todavia
// no sabes si tienes los datos
// console.log(datos);







