/*
Array y objectos
*/

// array con datos de un alumno
const alumno = [
    'ramon', // alumno[0]
    23, // alumno[1]
    'santander', // alumno[2]
    'masculino' // alumno[3]
];

// alumno como objeto
const objAlumno = {
    nombre: 'ramon', // objAlumno.nombre o objAlumno['nombre']
    edad: 23,
    poblacion: 'santander', // objAlumno.poblacion o objAlumno['poblacion']
    genero: 'masculino'
};

// leer todas las propiedades del alumno

// como array

// utilizando forEach
alumno.forEach(function (dato) {
    console.log(dato);
});

// utilizando for of
for (const dato of alumno) {
    console.log(dato);
}




// quiero recorrer el objeto objAlumno y mostrar sus propiedades

// mediante for in
for (const propiedad in objAlumno) {
    // muestro la propiedad
    console.log(propiedad); // nombre, edad, poblacion, genero
    // muestro el valor
    console.log(objAlumno[propiedad]); // ramon, 23, santander, masculino

}

