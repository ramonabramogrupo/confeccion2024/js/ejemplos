
// he creado un objeto de forma literal con 
// los datos de un cliente

const cliente = {
    nombre: 'Juan',
    email: 'j@j.com',
    edad: 23,
    poblacion: 'santander',
    genero: 'masculino'
}

// apunto a los li donde voy a escribir
const liNombre = document.querySelector('#nombre');
const liEmail = document.querySelector('#email');
const liEdad = document.querySelector('#edad');

// quiero que coloqueis el nombre del cliente en el li nombre
liNombre.textContent = cliente.nombre;

// quiero que coloqueis el email del cliente en el li email
liEmail.textContent = cliente.email;

// quiero que coloqueis la edad del cliente en el li edad
liEdad.textContent = cliente.edad;


