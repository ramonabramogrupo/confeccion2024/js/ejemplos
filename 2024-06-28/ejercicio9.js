/*

Descripción:
Crea una clase llamada Estudiante que represente a un estudiante. 
La clase debe tener las siguientes propiedades:
nombre (cadena)
edad (número)
grado (cadena)

La clase también debe tener un método llamado obtenerDetalles que devuelva una cadena con la información detallada del estudiante en la siguiente estructura:
<div>
    <ul>
    <li>Nombre: nombre del estudiante</li>
    <li>Edad: edad del estudiante</li>
    <li>Grado: grado del estudiante</li>
    </ul>
</div>

Instrucciones:

1- Define la clase Estudiante con las propiedades y el método descritos.
2- Crea una instancia de la clase Estudiante con valores de ejemplo.
3- Muestra los detalles del estudiante en el body utilizando el método obtenerDetalles.

*/

class Estudiante {

}

const alumno1 = new Estudiante();
alumno1.nombre = 'ramon';
alumno1.edad = 23;
alumno1.grado = 'santander';
document.body.appendChild(alumno1.obtenerDetalles());