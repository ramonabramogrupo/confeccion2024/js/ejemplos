/*
utilizar metodos dentro del objeto
Son funciones metidas dentro del objeto
*/

// array con datos de un alumno
const alumno = [
    'ramon',
    23,
    'santander',
    'masculino',
    function () {  // alumno[4]()
        return this[0] + " " + this[1]
    }
];


// alumno como objeto
const objAlumno = {
    nombre: 'ramon',
    edad: 23,
    poblacion: 'santander',
    genero: 'masculino',
    datos: function () {
        return this.nombre + " " + this.edad
    }
};

// utilizar metodos

// como array

console.log(alumno[0]);
console.log(alumno[1]);
console.log(alumno[4]());


// como objeto
console.log(objAlumno.nombre);
console.log(objAlumno.edad);
console.log(objAlumno.datos());



