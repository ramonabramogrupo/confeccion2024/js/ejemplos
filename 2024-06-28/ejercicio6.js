// quiero mostrar los datos recibidos del servidor API REST
// en mi pagina
/*
const datos=[
    {
        "userId": 1,
        "id": 1, // datos[0].id
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "userId": 1,
        "id": 2,
        "title": "qui est esse",
        "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    }
];

// mostrar todos los registro con esta estructura
/*
<div id="salida">
    <div>
        <h2>id</h2>
        <p>title</p>
        <p>body</p>
    </div>
    <div>
        <h2>id</h2>
        <p>title</p>
        <p>body</p>
    </div>
</div>
*/
const url = "https://jsonplaceholder.typicode.com/posts";

fetch(url)
    .then(function (response) {
        return response.json(); // formato json (sin comillas)
        // response.text() // formato json (con comillas)
    })
    .then(function (datos) {

        // si utilizo response.text()
        // los datos me llegan en formato json
        // los convierto a forma objeto de js
        // const datos = JSON.parse(datos);

        const divSalida = document.createElement("div");
        divSalida.id = "salida";
        // recorrer el array de objetos

        datos.forEach(function (registro) {
            // crear la estructura
            const estructura = {
                div: document.createElement("div"),
                h2: document.createElement("h2"),
                p: [
                    document.createElement("p"),
                    document.createElement("p")
                ]
            }

            // termino la estructura
            estructura.div.appendChild(estructura.h2);
            estructura.div.appendChild(estructura.p[0]);
            estructura.div.appendChild(estructura.p[1]);

            // añado los datos
            estructura.h2.textContent = registro.id;
            estructura.p[0].textContent = registro.title;
            estructura.p[1].textContent = registro.body;

            // añado el div al divSalida

            divSalida.appendChild(estructura.div);

        });

        document.body.appendChild(divSalida);

    });

