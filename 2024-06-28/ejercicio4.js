// quiero mostrar los datos recibidos del servidor API REST
// en mi pagina

const url = "https://api.thecatapi.com/v1/images/search";

fetch(url)
    .then(response => response.text())
    .then(function (data) {
        // los datos me llegan en formato json
        // los convierto a forma objeto de js

        const datos = JSON.parse(data);

        // crear un objeto que apunte a cada elemento de la web
        // donde voy a escribir

        const salida = {
            // propiedad que apunta img
            img: document.querySelector('img'),

            // propiedad que apunta figcaption
            figcaption: document.querySelector('figcaption')
        };


        // modifico el src de img (url de datos)
        salida.img.src = datos[0].url;

        // modificar el ancho y el alto de la imagen (width y height de datos)
        salida.img.style.width = datos[0].width + 'px';
        salida.img.style.height = datos[0].height + 'px';


        // escribo el contenido en figcaption (id de datos)
        salida.figcaption.textContent = datos[0].id;
        salida.figcaption.style.width = datos[0].width + 'px';

    });