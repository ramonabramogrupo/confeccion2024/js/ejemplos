// quiero mostrar los datos recibidos del servidor API REST
// en mi pagina

const url = "https://jsonplaceholder.typicode.com/posts/1";

fetch(url)
    .then(response => response.text())
    .then(function (data) {
        // los datos me llegan en formato json
        // los convierto a forma objeto de js

        const datos = JSON.parse(data);

        // crear un objeto que apunte a cada elemento de la web
        // donde voy a escribir

        const salida = {
            // propiedad que apunta h1
            h1: document.querySelector('h1'),
            // propiedad que apunta div donde coloca el contenido
            div: document.querySelector('#salida>div')
        };


        // escribo el contenido en h1 (title de datos)
        salida.h1.textContent = datos.title;

        // escribo el contenido en div (body de datos)
        salida.div.textContent = datos.body;

    });