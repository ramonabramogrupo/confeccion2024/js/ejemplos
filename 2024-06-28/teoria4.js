/*
Array y objectos
*/

// array de arrays
// cada elemento del array es un alumno (array)
const alumnos = [
    [
        'ramon',
        23,
        'santander', // alumnos[0][2]
        'masculino'
    ],
    [
        'maria',
        25,
        'santander', // alumno[1][2]
        'femenino'
    ]
];

// array de objetos
// cada elemento del array es un alumno (objeto)
const alumnosObjetos = [
    {
        nombre: 'ramon',
        edad: 23,
        poblacion: 'santander',  // alumnoObjetos[0].poblacion o alumnosObjetos[0]['poblacion']
        genero: 'masculino'
    },
    {
        nombre: 'maria',
        edad: 25,
        poblacion: 'santander', // alumnoObjetos[1].poblacion o alumnoObjetos[1]['poblacion']
        genero: 'femenino'
    }
];

// leer los los nombres y edades de los alumnos 1 y 2

// como array de arrays
// leer el nombre del primer alumno
alumnos[0][0];

//leer la edad del primer alumno
alumnos[0][1];

// leer el nombre del segundo alumno
alumnos[1][0];

//leer la edad del segundo alumno
alumnos[1][1];

// como arrays de objetos

// leer el nombre del primer alumno
alumnosObjetos[0].nombre;
alumnosObjetos[0]['nombre'];

//leer la edad del primer alumno
alumnosObjetos[0].edad;
alumnosObjetos[0]['edad'];

// leer el nombre del segundo alumno
alumnosObjetos[1].nombre;
alumnosObjetos[1]['nombre'];

//leer la edad del segundo alumno
alumnosObjetos[1].edad;
alumnosObjetos[1]['edad'];

// leer todos los alumnos

// como arrays de arrays
alumnos.forEach(function (alumno, indice) {
    console.log(indice);
    alumno.forEach(function (dato) {
        console.log(dato);
    });
});

// como arrays de objetos

alumnosObjetos.forEach(function (alumno, indice) {
    console.log(indice);
    console.log('nombre');
    console.log(alumno.nombre);
    console.log('edad');
    console.log(alumno.edad);
    console.log('poblacion');
    console.log(alumno.poblacion);
    console.log('genero');
    console.log(alumno.genero);
    // si quiero realizar un bucle para mostrar todas las propiedades
    // for in

    for (const propiedad in alumno) {
        console.log(propiedad);
        console.log(alumno[propiedad]);
    }
});