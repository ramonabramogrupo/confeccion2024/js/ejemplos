// quiero mostrar los datos recibidos del servidor API REST
// en mi pagina
/*
const datos=[
    {
    "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "https://via.placeholder.com/600/92c952",
    "thumbnailUrl": "https://via.placeholder.com/150/92c952"
    },
    {
    "albumId": 1,
    "id": 2,
    "title": "reprehenderit est deserunt velit ipsam",
    "url": "https://via.placeholder.com/600/771796",
    "thumbnailUrl": "https://via.placeholder.com/150/771796"
    },
    {
    "albumId": 1,
    "id": 3,
    "title": "officia porro iure quia iusto qui ipsa ut modi",
    "url": "https://via.placeholder.com/600/24f355",
    "thumbnailUrl": "https://via.placeholder.com/150/24f355"
    }
];


// mostrar todos los registro con esta estructura
/*
<div id="salida">
    <div>
        <h2>id</h2>
        <figure>
        <img src="url">
        <figcaption>title</figcaption>
        </figure>
    </div>
    <div>
        <h2>id</h2>
        <figure>
            <img src="url">
            <figcaption>title</figcaption>
        </figure>
    </div>
</div>
*/
const url = "https://jsonplaceholder.typicode.com/photos?_limit=5";

fetch(url)
    .then(response => response.json())
    .then(function (datos) {

        const divSalida = document.createElement("div");
        divSalida.id = "salida";

        // recorrer el array de objetos
        datos.forEach(function (registro) {
            // crear la estructura

            const div = document.createElement("div");
            const estructura = {
                h2: document.createElement("h2"),
                figure: document.createElement("figure"),
                imagen: {
                    img: document.createElement("img"),
                    figcaption: document.createElement("figcaption")
                }
            };

            // termino la estructura
            div.appendChild(estructura.h2);
            div.appendChild(estructura.figure);
            estructura.figure.appendChild(estructura.imagen.img);
            estructura.figure.appendChild(estructura.imagen.figcaption);

            // meto los datos

            estructura.h2.textContent = registro.id;
            estructura.imagen.figcaption.textContent = registro.title;
            estructura.imagen.img.src = registro.url;


            // añado el div al divSalida

            divSalida.appendChild(div);

        });

        document.body.appendChild(divSalida);

    });

