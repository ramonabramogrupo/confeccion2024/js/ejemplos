/*
Descripción:
Crea una clase llamada Libro que represente un libro.
La clase debe tener las siguientes propiedades:

titulo(cadena)
autor(cadena)
año(número)
La clase también debe tener un método llamado obtenerResumen que devuelva una cadena que resuma la información del libro.

Instrucciones:

1- Define la clase Libro con las propiedades y el método descritos.
2- Crea una instancia de la clase Libro con valores de ejemplo. (new)
3- Muestra el resumen del libro en la consola utilizando el método obtenerResumen.
*/

// clases

class Libro {
    //miembros

    //propiedades
    titulo = "";
    autor = "";
    anio = 0;

    //metodos
    obtenerResumen() {
        return `Titulo:${this.titulo},Autor: ${this.autor},Año:${this.anio} `;
        // return 'Titulo:' + this.titulo + ',Autor: ' + this.autor + ',Año:' + this.anio;

    }
}

// funciones

// constantes y variables
const libro1 = new Libro(); // creando un objeto de tipo Libro

// entradas
libro1.autor = "Antoine de Saint-Exupéry";
libro1.titulo = "El principe";
libro1.anio = 1943;

// escuchadores

// procesamiento

// salidas

console.log(libro1.obtenerResumen());