/*
Array y objectos
*/

// array con datos de un alumno
const alumno = [
    'ramon',
    23,
    'santander',
    'masculino'
];

// alumno como objeto
const objAlumno = {
    nombre: 'ramon',
    edad: 23,
    poblacion: 'santander',
    genero: 'masculino'
};

// modificar nombre y edad del alumno

// como array
alumno[0] = 'silvia';
alumno[1] = 33;

// como objeto
objAlumno.nombre = 'silvia';
objAlumno['nombre'] = 'silvia';
objAlumno.edad = 33;
objAlumno['edad'] = 33;
