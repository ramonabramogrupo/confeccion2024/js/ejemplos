/**
 * Crear una funcion que muestre las fotos pasadas en un array
 * si no pasas nada mostrar una foto de anonimo
 * 
 * para realizar el ejercicio utilizamos la etiqueta template
 * para mostrar las imagenes
 */

// function mostrarFotos(fotos = ["anonimo.jpg"]) {
//     let resultado = "";
//     const div = document.querySelector("#galeria");

//     for (const nombre of fotos) {
//         resultado += `<figure>
//             <img src="img/${nombre}">
//         </figure>`;
//     }
//     div.innerHTML = resultado;

// }

// function mostrarFotos(fotos = ["anonimo.jpg"]) {
//     const div = document.querySelector("#galeria");

//     for (const nombre of fotos) {
//         const figure = document.createElement("figure");
//         const img = document.createElement("img");

//         img.src = `./img/${nombre}`;
//         figure.appendChild(img);
//         div.appendChild(figure);
//     }

// }




function mostrarFotos(fotos = ["anonimo.jpg"]) {
    const div = document.querySelector('#galeria');

    // en la constante template tengo el contenido de la eiqueta template
    const template = document.querySelector('template').content;
    // la propiedad content solo la tienen la etiqueta template

    let fragment = document.createDocumentFragment();

    for (const nombre of fotos) {
        const clone = template.cloneNode(true);
        clone.querySelector('img').src = `./img/${nombre}`;
        fragment.appendChild(clone);
    }
    div.appendChild(fragment);
}

// mostrarFotos(['foto1.jpg', 'foto2.jpg', 'foto3.jpg', 'foto4.jpg']);

// mostrarFotos();

mostrarFotos(['foto1.jpg', 'foto2.jpg', 'foto3.jpg', 'foto4.jpg', 'foto5.jpg', 'foto6.jpg', 'foto7.jpg', 'foto8.jpg', 'foto8.jpg']);


