/**
 *  funcion con argumentos variables
 */

/**
 * Funcion que recibe una operacion a realizar y un conjunto de numeros. Devuelve el resultado de la operacion
 * @param {string} operador  : operacion a realizar ("sumar", "multiplicar")
 * @param  {...enteros} numeros : numeros a multiplicar
 * @return {entero} el resultado de la operacion
 */

function operacion(operador, ...numeros) {
    let resultado = 0;

    switch (operador) {
        case "sumar":
            resultado = 0;

            numeros.forEach(function (numero) {
                resultado += Number(numero)
            });

            break;
        case "multiplicar":
            resultado = 1;

            numeros.forEach(function (numero) {
                resultado *= numero
            });

            break;
    }
    return resultado;
}


// constantes que apunten a los dos botones
const botones = document.querySelectorAll('button');

botones.forEach(function (boton) {

    boton.addEventListener('click', function (event) {
        let resultado = 0;
        // leo el id del boton pulsado para saber cual es el que pulsas
        // let accion = event.target.id;
        // leo el data-attribute data-tipo del boton pulsado
        let accion = event.target.dataset.tipo;
        let mensaje = event.target.dataset.mensaje;


        // constantes que apunten a la salida
        const divSalida = document.querySelector('#salida');

        // constantes que apunten a los inputs
        // es un array de elementos
        // const inputs = document.querySelectorAll('input[type="number"]');
        const inputs = document.querySelectorAll('.numeros');

        // opcion 1
        // resultado = operacion(accion, inputs[0].value, inputs[1].value, inputs[2].value, inputs[3].value);

        // opcion 2
        const numeros = [];
        inputs.forEach(function (input, indice) {
            // numeros[indice] = input.value;
            numeros.push(input.value);
        });
        resultado = operacion(accion, ...numeros);




        divSalida.textContent = `la ${mensaje} vale ` + resultado;
        // divSalida.textContent = accion + ": " + resultado;
    });
});

