/*
 * funcion para crear una lista de elementos HTML basada en un array de elementos 

Tanto el ul como los li se crean de forma dinamica
Solo utilizamos textContent para añadir los textos
 */

function addMenu(elementos) {
    const ul = document.createElement("ul");
    const div = document.querySelector("#salida");

    // recorrer elementos

    for (const elemento of elementos) {
        const li = document.createElement("li");
        li.textContent = elemento;
        ul.appendChild(li);
    }
    div.appendChild(ul);
}

function addMenu1(...elementos) {
    const ul = document.createElement("ul");
    const div = document.querySelector("#salida");

    // recorrer elementos

    for (const elemento of elementos) {
        const li = document.createElement("li");
        li.textContent = elemento;
        ul.appendChild(li);
    }
    div.appendChild(ul);
}


addMenu(["elemento1", "elemento2", "elemento3"]);
// addMenu1("elemento1", "elemento2", "elemento3");


