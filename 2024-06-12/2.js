/**
 * quiero utilizar argumentos (parametros por defecto) en las funciones
 */

/**
 * funcion que devuelve una lista de html como texto con numeros
 * <ul>
 * <li>1</li>
 * <li>2</li>
 * <li>3</li>
 * </ul>
 */

/**
 * retorna una lista no numerada en html
 * @param {in} fin  el numero donde termina la lista
 * @param {int} inicio el numero donde comienza la lista
 * @returns la lista en formato html
 */
function lista(fin, inicio = 1) {
    let acumulador = "<ul>";

    for (let contador = inicio; contador <= fin; contador++) {
        acumulador += `<li>${contador}</li>`;
    }
    // hasta aqui
    // <ul>
    // <li>1</li>
    // <li>2</li>
    // <li>3</li>

    // cierro el ul
    acumulador += "</ul>";

    return acumulador;

}


// constante que apunta la div donde quiero dibujar la lista
const div = document.querySelector("div");

let menu = "";

// procesamiento
menu = lista(5);

// salida
div.innerHTML = menu;






