/**
 * Utilizar funciones declaradas dentro de js
 */

function sumar(n1, n2) {
    // variable (local)
    let resultado = 0;
    resultado = n1 + n2;
    return resultado;
}

function restar(n1, n2) {
    // variable (local)
    let resultado = 0;

    resultado = n1 - n2;
    return resultado;
}

function multiplicar(n1, n2) {
    // variable (local)
    let resultado = 0;

    resultado = n1 * n2;
    return resultado;
}

function dividir(n1, n2) {
    let resultado = 0;

    resultado = n1 / n2;
    return resultado;
}


// dos variables con las entradas
let numero1 = 15;
let numero2 = 10;

// una constante de tipo array con las salidas
const resultado = [];

resultado[0] = sumar(numero1, numero2); // 25
resultado[1] = sumar(20, 4); // 24
resultado[2] = sumar(10, 10); // 20

resultado[3] = restar(numero1, numero2); // 5

resultado[4] = multiplicar(numero1, numero2); // 150
resultado[5] = multiplicar(20, 4); // 80

resultado[6] = dividir(numero1, numero2); // 1.5


console.log(resultado);
