[[_TOC_]]

# Objetivos

Trabajo con funciones en Js

# Funciones

Las funciones en JavaScript son bloques de código reutilizables que realizan una tarea específica o calculan un valor. Las funciones permiten estructurar y organizar el código, así como evitar la repetición de código al encapsular lógica que puede ser llamada múltiples veces.

![funciones](../imgs/funciones.webp)

>Una **función** es un bloque de código diseñado para realizar una tarea particular. Una función puede ser vista como una "caja negra" que recibe entradas (argumentos), realiza operaciones internas y produce una salida (valor de retorno).


## Tipos de funciones 

## Función Declarada

Las funciones declaradas son las que se definen con la palabra clave function seguida de un nombre, una lista de parámetros entre paréntesis y un bloque de código entre llaves.

```js
function nombreDeLaFuncion(parametro1, parametro2) {
    // Código de la función
    return parametro1 + parametro2;
}

// Llamada a la función
console.log(nombreDeLaFuncion(3, 4)); // 7
```

## Uso de las funciones declaradas


Cuando queremos utilizar una funcion declarada los pasos a seguir son:

- Define la funcion
- Escribir el codigo de la funcion
- Llamar a la funcion 
- La funcion se ejecuta y puede devolver un resultado
- El resultado devuelto se puede mostrar o utilizar

![diagrama1](../imgs/diagrama1.png)

En el siguiente diagrama podemos ver los conceptos mas importantes a la hora de crear una funcion:
- parametros : valores pasados a la funcion
- valor de retorno : valor devuelto por la funcion
- codigo interno : codigo que se ejecuta cuando llamaas a la funcion

![diagrama2](../imgs/diagram2.png)

# DOM
El Document Object Model (DOM) es una interfaz de programación para documentos HTML y XML. Representa la estructura del documento como un árbol de nodos, donde cada nodo corresponde a una parte del documento (como un elemento, un atributo o un texto). El DOM permite a los lenguajes de programación, como JavaScript, acceder y manipular la estructura, el estilo y el contenido del documento de manera dinámica.


La estructura del DOM puede visualizarse como un árbol con diferentes tipos de nodos:

- Document: El nodo raíz que representa el documento entero.
- Element: Nodos que representan elementos HTML o XML (<div>, <p>, <a>, etc.).
- Text: Nodos que representan el contenido textual dentro de los elementos.
- Attribute: Nodos que representan los atributos de los elementos.
- Comment: Nodos que representan comentarios en el código HTML o XML.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ejemplo DOM</title>
</head>
<body>
    <div id="miDiv">Hola Mundo</div>
    <p>Este es un párrafo.</p>
</body>
</html>
```

```less
document
└── html
    ├── head
    │   ├── meta
    │   └── title
    └── body
        ├── div (id="miDiv")
        │   └── "Hola Mundo" (Text Node)
        └── p
            └── "Este es un párrafo." (Text Node)
```

# Nodos

En JavaScript, los nodos son las unidades básicas que componen un documento HTML o XML en el Document Object Model (DOM). El DOM es una representación estructurada de un documento que permite a los scripts acceder y manipular su contenido y estructura.

## Tipos de Nodos
Existen varios tipos de nodos en el DOM, cada uno representando diferentes partes del documento. Los más comunes incluyen:

- Element Nodes (Nodos de Elemento):
Representan los elementos HTML o XML.

>Ejemplo: 
\<div>, \<p>, \<a>, etc.

- Text Nodes (Nodos de Texto):
Representan el contenido textual dentro de un elemento.

>Ejemplo: 
El texto "Hola Mundo" dentro de \<p>Hola Mundo\</p> es un nodo de texto.

- Comment Nodes (Nodos de Comentario):
Representan los comentarios en el código HTML o XML.

>Ejemplo: 
\<!-- Este es un comentario -->.

- Document Nodes (Nodos de Documento):
Representan el documento completo.

>Ejemplo: 
El nodo raíz de un documento HTML (\<html>).

- DocumentFragment Nodes (Nodos de Fragmento de Documento):
Representan una porción del documento que no forma parte del DOM principal hasta que se inserta.
Utilizados para crear y manipular grupos de nodos de manera eficiente.

- Attribute Nodes (Nodos de Atributo):
Representan los atributos de un elemento HTML o XML.

>Ejemplo: 
El atributo class="miClase" en 
\<div class="miClase">\</div>.


## Nodo y elemento

En Javascript, la forma de acceder al DOM es a través de un objeto llamado document, que representa el árbol DOM de la página o, más concretamente, la página de la pestaña del navegador donde nos encontramos. 

Lo que tenemos en el document son nodos.

Un nodo es cualquier objeto en la jerarquía del DOM. Existen diferentes tipos de nodos, cada uno representando una parte diferente del documento. Los tipos más comunes de nodos incluyen:

- Element Node (Nodo de Elemento):
Representa un elemento HTML o XML.
Ejemplo: \<div>, \<p>, \<a>.
- Text Node (Nodo de Texto):
Representa el contenido textual de un elemento.
Ejemplo: El texto "Hola Mundo" dentro de \<p>Hola Mundo\</p>.
- Comment Node (Nodo de Comentario):
Representa un comentario en el código HTML o XML.
Ejemplo: \<!-- Este es un comentario -->.
- Attribute Node (Nodo de Atributo):
Representa un atributo de un elemento HTML o XML.
Ejemplo: El atributo class="miClase" en \<div class="miClase">\</div>.
- Document Node (Nodo de Documento):
Representa el documento completo.
Ejemplo: El nodo raíz que representa todo el documento HTML (\<html>).

>Un elemento es un tipo específico de nodo. Más precisamente, un elemento es un nodo de tipo Element, que representa un elemento HTML o XML. Los elementos son nodos que tienen una etiqueta, atributos, y pueden contener otros nodos (tales como nodos de texto o comentarios).

# Contenido de los elementos

Podemos necesitar acceder al contenido de los elementos como texto. 

En JavaScript, hay varios métodos y propiedades para acceder y manipular el contenido de los elementos HTML. 

Estos métodos y propiedades te permiten trabajar tanto con el contenido de texto como con el contenido HTML. 

## innerHTML
Descripción: Obtiene o establece el contenido HTML de un elemento, incluyendo los elementos hijos.
Sintaxis: element.innerHTML
```js
let div = document.getElementById('miDiv');
div.innerHTML = '<p>Nuevo contenido</p>';
```

## outerHTML
Descripción: Obtiene o establece el contenido HTML completo del elemento, incluyendo el elemento en sí.
Sintaxis: element.outerHTML

```js
let div = document.getElementById('miDiv');
console.log(div.outerHTML);
div.outerHTML = '<section id="miDiv"><p>Contenido reemplazado</p></section>';
```

## textContent
Descripción: Obtiene o establece el contenido textual de un elemento y sus descendientes.
Sintaxis: element.textContent
```js
let div = document.getElementById('miDiv');
div.textContent = 'Texto actualizado';
```
## innerText
Descripción: Similar a textContent, pero respeta el estilo CSS (como display: none).
Sintaxis: element.innerText
```js
let div = document.getElementById('miDiv');
div.innerText = 'Texto actualizado con estilo';
```

## Crear HTML desde JS

Hasta ahora hemos creado elementos html con innerHTML o utilizando textContent.

Vamos a empezar a crear elementos html de una forma mas potente


## Trabajo con nodos en JS

La creación y manipulación de nodos es una parte fundamental del desarrollo web con JavaScript. 

>Los nodos son los componentes básicos de un documento HTML. 

Existen diferentes tipos de nodos, pero los más comunes son:
- los elementos (Element), 
- los nodos de texto (Text), 
- y los nodos de comentarios (Comment). 

JavaScript proporciona una variedad de métodos y propiedades para seleccionar, crear, insertar, modificar y eliminar estos nodos en el Document Object Model (DOM).

## Seleccionar nodos
En JavaScript, existen varios métodos para seleccionar nodos del DOM. Estos métodos permiten acceder a elementos específicos del documento HTML de manera eficiente y precisa. 

### getElementById
Sintaxis: document.getElementById(id)
Descripción: Selecciona un elemento por su atributo id.
```js
let element = document.getElementById('miElemento');
```

### getElementsByClassName
Sintaxis: document.getElementsByClassName(className)
Descripción: Selecciona todos los elementos que tienen una clase específica. Devuelve una colección (HTMLCollection) de elementos.

```js
let elements = document.getElementsByClassName('miClase');
```

### getElementsByTagName
Sintaxis: document.getElementsByTagName(tagName)
Descripción: Selecciona todos los elementos con un nombre de etiqueta específico. Devuelve una colección (HTMLCollection) de elementos.

```js
let elements = document.getElementsByTagName('div');
```

### querySelector
Sintaxis: document.querySelector(selectors)
Descripción: Selecciona el primer elemento que coincide con el grupo de selectores especificado.

```js
let element = document.querySelector('.miClase');
```
### querySelectorAll
Sintaxis: document.querySelectorAll(selectors)
Descripción: Selecciona todos los elementos que coinciden con el grupo de selectores especificado. Devuelve una colección estática (NodeList) de elementos.

```js
let elements = document.querySelectorAll('.miClase');
```

### getElementsByName
Sintaxis: document.getElementsByName(name)
Descripción: Selecciona todos los elementos con un atributo name específico. Devuelve una colección (NodeList) de elementos.

```js
let elements = document.getElementsByName('miNombre');
```
| Método                    | Sintaxis                                | Descripción                                                                             | Ejemplo                                                   |
|---------------------------|-----------------------------------------|-----------------------------------------------------------------------------------------|-----------------------------------------------------------|
| `getElementById`          | `document.getElementById(id)`           | Selecciona un elemento por su atributo `id`.                                            | `let element = document.getElementById('miElemento');`    |
| `getElementsByClassName`  | `document.getElementsByClassName(className)` | Selecciona todos los elementos que tienen una clase específica. Devuelve una colección (HTMLCollection) de elementos. | `let elements = document.getElementsByClassName('miClase');` |
| `getElementsByTagName`    | `document.getElementsByTagName(tagName)`| Selecciona todos los elementos con un nombre de etiqueta específico. Devuelve una colección (HTMLCollection) de elementos. | `let elements = document.getElementsByTagName('div');`    |
| `querySelector`           | `document.querySelector(selectors)`     | Selecciona el primer elemento que coincide con el grupo de selectores especificado.      | `let element = document.querySelector('.miClase');`       |
| `querySelectorAll`        | `document.querySelectorAll(selectors)`  | Selecciona todos los elementos que coinciden con el grupo de selectores especificado. Devuelve una colección estática (NodeList) de elementos. | `let elements = document.querySelectorAll('.miClase');`   |
| `getElementsByName`       | `document.getElementsByName(name)`      | Selecciona todos los elementos con un atributo `name` específico. Devuelve una colección (NodeList) de elementos. | `let elements = document.getElementsByName('miNombre');`  |


## Crear Nodos

Si te encuentras en fase de aprendizaje, lo normal suele ser crear código HTML desde un fichero HTML. Sin embargo, y sobre todo con el auge de las páginas SPA (Single Page Application) y los frameworks o librerías Javascript, esto ha cambiado bastante y es muy frecuente crear código HTML desde Javascript de forma dinámica.

Esto tiene sus ventajas y sus desventajas. Un fichero .html siempre será más sencillo, más «estático» y más directo, ya que lo primero que analiza un navegador web es un fichero de marcado HTML. Por otro lado, un fichero .js es más complejo y menos directo, pero mucho más potente, «dinámico» y flexible, con menos limitaciones a la hora de desarrollar.

En esta seccion os voy a mostrar los metodos mas importantes para crear contenido de forma dinamica.


### document.createElement()
Este método crea un nuevo elemento HTML especificado por su nombre de etiqueta.

```javascript
// Crear un nuevo elemento <div>
let newDiv = document.createElement('div');
newDiv.textContent = 'Este es un nuevo div.';
document.body.appendChild(newDiv);
```

### document.createTextNode()
Este método crea un nuevo nodo de texto que contiene una cadena de texto especificada.

```javascript
// Crear un nuevo nodo de texto
let textNode = document.createTextNode('Este es un nodo de texto.');
document.body.appendChild(textNode);
```
### element.appendChild()
Este método agrega un nodo como el último hijo de un elemento.

```javascript
// Crear y agregar un nuevo párrafo a un div existente
let newParagraph = document.createElement('p');
newParagraph.textContent = 'Este es un párrafo dentro del div.';
let existingDiv = document.getElementById('existingDiv');
existingDiv.appendChild(newParagraph);
```

### element.insertBefore()
Este método inserta un nodo antes de un nodo hijo específico de un elemento.

```javascript
// Crear un nuevo párrafo y insertarlo antes del primer hijo de un div
let newParagraph = document.createElement('p');
newParagraph.textContent = 'Este párrafo se inserta antes del primer hijo.';
let parentDiv = document.getElementById('parentDiv');
let firstChild = parentDiv.firstChild;
parentDiv.insertBefore(newParagraph, firstChild);
```

### element.removeChild()
Este método elimina un nodo hijo específico de un elemento.

```javascript
// Eliminar el primer hijo de un div
let parentDiv = document.getElementById('parentDiv');
let childToRemove = parentDiv.firstChild;
parentDiv.removeChild(childToRemove);
```
### element.replaceChild()
Este método reemplaza un nodo hijo de un elemento con otro nodo.

```javascript
// Reemplazar un nodo hijo con un nuevo nodo
let newParagraph = document.createElement('p');
newParagraph.textContent = 'Este es el nuevo párrafo que reemplaza al viejo.';
let parentDiv = document.getElementById('parentDiv');
let oldChild = parentDiv.firstChild;
parentDiv.replaceChild(newParagraph, oldChild);
```

### element.cloneNode()
Este método clona un nodo y, opcionalmente, sus hijos.

```javascript
// Clonar un nodo y sus hijos
let originalDiv = document.getElementById('originalDiv');
let cloneDiv = originalDiv.cloneNode(true); // true para clonar sus hijos también
document.body.appendChild(cloneDiv);
```

## Resumen 

 Métodos y propiedades relacionados con la creación, eliminación y manipulación de nodos en JavaScript.

| Categoría              | Método/Propiedad              | Descripción                                                                              | Ejemplo                                                           |
|------------------------|-------------------------------|------------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| **Creación de Nodos**  | `document.createElement()`    | Crea un nuevo elemento HTML especificado por su nombre de etiqueta.                      | `let div = document.createElement('div');`                        |
|                        | `document.createTextNode()`   | Crea un nuevo nodo de texto con la cadena especificada.                                  | `let textNode = document.createTextNode('Hello, World!');`        |
|                        | `document.createComment()`    | Crea un nuevo nodo de comentario con la cadena especificada.                             | `let comment = document.createComment('This is a comment.');`     |
|                        | `document.createDocumentFragment()` | Crea un `DocumentFragment` vacío.                                                     | `let fragment = document.createDocumentFragment();`               |
| **Eliminación de Nodos** | `element.removeChild()`        | Elimina un nodo hijo específico de un elemento.                                           | `parent.removeChild(child);`                                      |
|                        | `element.remove()`            | Elimina el elemento del árbol del DOM al que pertenece.                                  | `element.remove();`                                               |
| **Manipulación de Nodos** | `element.appendChild()`         | Agrega un nodo como el último hijo de un elemento.                                        | `parent.appendChild(child);`                                      |
|                        | `element.insertBefore()`      | Inserta un nodo antes de un nodo hijo específico de un elemento.                         | `parent.insertBefore(newNode, referenceNode);`                    |
|                        | `element.replaceChild()`      | Reemplaza un nodo hijo de un elemento con otro nodo.                                     | `parent.replaceChild(newNode, oldNode);`                          |
|                        | `element.cloneNode()`         | Clona un nodo y, opcionalmente, sus hijos.                                               | `let clone = element.cloneNode(true);`                            |
|                        | `element.innerHTML`           | Propiedad que obtiene o establece el HTML sintácticamente válido describiendo los descendientes del elemento. | `element.innerHTML = '<p>New content</p>';`                       |
|                        | `element.textContent`         | Propiedad que obtiene o establece el contenido textual de un nodo y sus descendientes.    | `element.textContent = 'New text content';`                       |
|                        | `element.parentNode`          | Propiedad que devuelve el nodo padre del elemento.                                       | `let parent = element.parentNode;`                                |
|                        | `element.firstChild`          | Propiedad que devuelve el primer nodo hijo de un elemento.                               | `let firstChild = element.firstChild;`                            |
|                        | `element.lastChild`           | Propiedad que devuelve el último nodo hijo de un elemento.                               | `let lastChild = element.lastChild;`                              |
|                        | `element.nextSibling`         | Propiedad que devuelve el siguiente nodo hermano del elemento.                           | `let nextSibling = element.nextSibling;`                          |
|                        | `element.previousSibling`     | Propiedad que devuelve el nodo hermano anterior del elemento.                            | `let prevSibling = element.previousSibling;`                      |
|                        | `element.childNodes`          | Propiedad que devuelve una colección de los nodos hijos del elemento.                    | `let children = element.childNodes;`                              |
|                        | `element.children`            | Propiedad que devuelve una colección de los elementos hijos (no nodos de texto/comentarios) del elemento. | `let children = element.children;`                               |

Esta tabla proporciona una referencia detallada de los métodos y propiedades más importantes para la creación, eliminación y manipulación de nodos en JavaScript, junto con su sintaxis y ejemplos en formato de código.

| Categoría              | Método/Propiedad              | Sintaxis                               | Descripción                                                                                                                                           | Ejemplo                                                                                 |
|------------------------|-------------------------------|----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| **Creación de Nodos**  | `document.createElement()`    | `document.createElement(tagName)`      | Crea un nuevo elemento HTML especificado por su nombre de etiqueta.                                                                                   | `let div = document.createElement('div');`                             |
|                        | `document.createTextNode()`   | `document.createTextNode(data)`        | Crea un nuevo nodo de texto con la cadena especificada.                                                                                               | `let textNode = document.createTextNode('Hello, World!');`             |
|                        | `document.createComment()`    | `document.createComment(data)`         | Crea un nuevo nodo de comentario con la cadena especificada.                                                                                          | `let comment = document.createComment('This is a comment.');`          |
|                        | `document.createDocumentFragment()` | `document.createDocumentFragment()`  | Crea un `DocumentFragment` vacío.                                                                                                                     | `let fragment = document.createDocumentFragment();`                    |
| **Eliminación de Nodos** | `element.removeChild()`        | `parentNode.removeChild(childNode)`    | Elimina un nodo hijo específico de un elemento.                                                                                                       | `parent.removeChild(child);`                                           |
|                        | `element.remove()`            | `element.remove()`                     | Elimina el elemento del árbol del DOM al que pertenece.                                                                                               | `element.remove();`                                                    |
| **Manipulación de Nodos** | `element.appendChild()`         | `parentNode.appendChild(newNode)`      | Agrega un nodo como el último hijo de un elemento.                                                                                                     | `parent.appendChild(child);`                                           |
|                        | `element.insertBefore()`      | `parentNode.insertBefore(newNode, referenceNode)` | Inserta un nodo antes de un nodo hijo específico de un elemento.                                                                                      | `parent.insertBefore(newNode, referenceNode);`                         |
|                        | `element.replaceChild()`      | `parentNode.replaceChild(newNode, oldNode)` | Reemplaza un nodo hijo de un elemento con otro nodo.                                                                                                  | `parent.replaceChild(newNode, oldNode);`                               |
|                        | `element.cloneNode()`         | `node.cloneNode(deep)`                 | Clona un nodo y, opcionalmente, sus hijos si `deep` es `true`.                                                                                        | `let clone = element.cloneNode(true);`                                 |
|                        | `element.innerHTML`           | `element.innerHTML`                    | Propiedad que obtiene o establece el HTML sintácticamente válido describiendo los descendientes del elemento.                                          | `element.innerHTML = '<p>New content</p>';`                            |
|                        | `element.textContent`         | `element.textContent`                  | Propiedad que obtiene o establece el contenido textual de un nodo y sus descendientes.                                                                | `element.textContent = 'New text content';`                            |
|                        | `element.parentNode`          | `element.parentNode`                   | Propiedad que devuelve el nodo padre del elemento.                                                                                                    | `let parent = element.parentNode;`                                     |
|                        | `element.firstChild`          | `element.firstChild`                   | Propiedad que devuelve el primer nodo hijo de un elemento.                                                                                             | `let firstChild = element.firstChild;`                                 |
|                        | `element.lastChild`           | `element.lastChild`                    | Propiedad que devuelve el último nodo hijo de un elemento.                                                                                            | `let lastChild = element.lastChild;`                                   |
|                        | `element.nextSibling`         | `element.nextSibling`                  | Propiedad que devuelve el siguiente nodo hermano del elemento.                                                                                         | `let nextSibling = element.nextSibling;`                               |
|                        | `element.previousSibling`     | `element.previousSibling`              | Propiedad que devuelve el nodo hermano anterior del elemento.                                                                                         | `let prevSibling = element.previousSibling;`                           |
|                        | `element.childNodes`          | `element.childNodes`                   | Propiedad que devuelve una colección de los nodos hijos del elemento.                                                                                 | `let children = element.childNodes;`                                   |
|                        | `element.children`            | `element.children`                     | Propiedad que devuelve una colección de los elementos hijos (no nodos de texto/comentarios) del elemento.                                             | `let children = element.children;`                                     |

# Trabajo con Atributos

Las etiquetas HTML tienen ciertos atributos que definen el comportamiento de la etiqueta. 
Existen atributos comunes a todas las etiquetas HTML, y atributos que sólo existen para determinadas etiquetas HTML. 

>El orden de los atributos en HTML no es importante, da igual que este primero o segundo, no influye en nada.

Tenemos dos formas de afrontar el trabajo con atributos :
- crear nodos de atributos
- utilizar directamente los atributos

## Crear nodos de atributos

En JavaScript, los nodos de atributo representan los atributos de un elemento HTML o XML. Sin embargo, es importante notar que en la práctica moderna, generalmente no manipulamos atributos como nodos, sino que usamos métodos y propiedades más directos del DOM para trabajar con ellos. Dicho esto, aún es posible crear y manipular nodos de atributo utilizando la interfaz Attr.

```js
// Paso 1: Crear un nodo de atributo
let newAttr = document.createAttribute('class');

// Paso 2: Asignar un valor al nodo de atributo
newAttr.value = 'miClase';

// Crear un elemento al que se le va a agregar el atributo
let newDiv = document.createElement('div');

// Paso 3: Agregar el nodo de atributo al elemento
newDiv.setAttributeNode(newAttr);

// Agregar el elemento al cuerpo del documento
document.body.appendChild(newDiv);

// Verificar el resultado
console.log(newDiv); // <div class="miClase"></div>

```
### Métodos y Propiedades Relacionadas

- document.createAttribute(name): Crea un nuevo nodo de atributo con el nombre especificado.
- element.setAttributeNode(attr): Agrega un nodo de atributo a un elemento.
- element.getAttributeNode(name): Devuelve el nodo de atributo con el nombre especificado.
- element.removeAttributeNode(attr): Elimina el nodo de atributo especificado del elemento.

## Utilizar directamente los atributos

Para la mayoría de las tareas comunes, los métodos como setAttribute y getAttribute son más convenientes y directos, sin necesidad de crear un nodo de atributo.

Supongamos que tienes un elemento <div> con un atributo id y deseas acceder y modificar ese atributo utilizando JavaScript.

```html
<div id="miDiv" class="miClase">Contenido del Div</div>
```

```js
// Seleccionar el elemento
let div = document.getElementById('miDiv');

// Acceder al atributo 'id'
console.log(div.getAttribute('id'));  // Salida: 'miDiv'

// Acceder al atributo 'class'
console.log(div.getAttribute('class'));  // Salida: 'miClase'

// Modificar un atributo existente
div.setAttribute('class', 'nuevaClase');
console.log(div.getAttribute('class'));  // Salida: 'nuevaClase'

// Añadir un nuevo atributo
div.setAttribute('data-nuevo', 'valorNuevo');
console.log(div.getAttribute('data-nuevo'));  // Salida: 'valorNuevo'

// Eliminar un atributo
div.removeAttribute('data-nuevo');
console.log(div.getAttribute('data-nuevo'));  // Salida: null

```

### Metodos mas importantes para trabajo con Atributos

Metodos para obtener atributos

| Método                    | Sintaxis                      | Descripción                                                                                     | Ejemplo                                            |
|---------------------------|-------------------------------|-------------------------------------------------------------------------------------------------|----------------------------------------------------|
| `getAttribute`            | `element.getAttribute(name)`  | Devuelve el valor del atributo especificado por `name`.                                         | `let id = element.getAttribute('id');`             |
| `hasAttribute`            | `element.hasAttribute(name)`  | Devuelve `true` si el atributo especificado por `name` existe en el elemento, `false` en caso contrario. | `let tieneClase = element.hasAttribute('class');`  |
| `hasAttributes`           | `element.hasAttributes()`     | Devuelve `true` si el elemento tiene cualquier atributo, `false` en caso contrario.             | `let tieneAtributos = element.hasAttributes();`    |
| `getAttributeNames`       | `element.getAttributeNames()` | Devuelve un array con los nombres de todos los atributos presentes en el elemento.              | `let atributos = element.getAttributeNames();`     |

Métodos para Modificar o Eliminar Atributos

| Método                    | Sintaxis                                 | Descripción                                                                                     | Ejemplo                                                               |
|---------------------------|------------------------------------------|-------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| `setAttribute`            | `element.setAttribute(name, value)`      | Establece el valor del atributo especificado por `name` a `value`. Si el atributo no existe, lo añade al elemento. | `element.setAttribute('class', 'nuevaClase');`                        |
| `removeAttribute`         | `element.removeAttribute(name)`          | Elimina el atributo especificado por `name` del elemento.                                        | `element.removeAttribute('data-nuevo');`                              |
| `toggleAttribute`         | `element.toggleAttribute(name, force)`   | Alterna el atributo especificado por `name`. Si se proporciona `force`, se añade el atributo si es `true` y se elimina si es `false`. | `element.toggleAttribute('hidden');`                                  |

Descripción de los Métodos para Obtener Atributos
- getAttribute: Utilizado para obtener el valor de un atributo específico del elemento.
- hasAttribute: Utilizado para verificar si un atributo específico existe en el elemento.
- hasAttributes: Utilizado para verificar si el elemento tiene algún atributo.
- getAttributeNames: Utilizado para obtener un array con los nombres de todos los atributos del elemento.

Descripción de los Métodos para Modificar o Eliminar Atributos
- setAttribute: Utilizado para establecer o modificar el valor de un atributo en el elemento.
- removeAttribute: Utilizado para eliminar un atributo del elemento.
- toggleAttribute: Utilizado para alternar el atributo especificado, añadiéndolo o eliminándolo según corresponda.

## Atributos acceso directo

En un elemento HTML, hay varios atributos estándar a los que podemos acceder y manipular directamente a través de sus propiedades en JavaScript. 

Estos atributos se mapean a propiedades del objeto del DOM del elemento. 

Aquí están algunos de los más comunes:

- id
Propiedad: element.id
Descripción: Representa el identificador único del elemento.

```js
let element = document.getElementById('miElemento');
element.id = 'nuevoID';
```

- class
Propiedad: element.className o element.classList
Descripción: Representa la(s) clase(s) del elemento.
```js
element.className = 'miClase nuevaClase'; // Usando className
element.classList.add('otraClase');       // Usando classList
```
- src
Propiedad: element.src
Descripción: Representa la URL de la fuente de un elemento <img>, <script>, <iframe>, etc.

```js
let img = document.getElementById('miImagen');
img.src = 'nuevaImagen.jpg';
```
- href
Propiedad: element.href
Descripción: Representa la URL a la que apunta un enlace <a> o <link>.
```js
let link = document.getElementById('miEnlace');
link.href = 'https://nuevaURL.com';
```
- value
Propiedad: element.value
Descripción: Representa el valor de un elemento de formulario <input>, <textarea>, <select>, etc.

```js
let input = document.getElementById('miInput');
input.value = 'nuevoValor';
```
- style
Propiedad: element.style
Descripción: Representa los estilos en línea del elemento.
```js
element.style.color = 'red';
element.style.backgroundColor = 'blue';
```
- title
Propiedad: element.title
Descripción: Representa el texto que aparece como una información sobre herramientas (tooltip).
```js
element.title = 'Nueva información sobre herramientas';
```
- alt
Propiedad: element.alt
Descripción: Representa el texto alternativo para una imagen.
```js
let img = document.getElementById('miImagen');
img.alt = 'Texto alternativo';
```
- disabled
Propiedad: element.disabled
Descripción: Indica si un elemento de formulario está deshabilitado.
```js
let button = document.getElementById('miBoton');
button.disabled = true;
```
- checked
Propiedad: element.checked
Descripción: Indica si un elemento <input> de tipo checkbox o radio está seleccionado.

```js
let checkbox = document.getElementById('miCheckbox');
checkbox.checked = true;
```

| Atributo HTML | Propiedad JavaScript     | Descripción                                                                              | Ejemplo                                                 |
|---------------|---------------------------|------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `id`          | `element.id`              | Representa el identificador único del elemento.                                           | `element.id = 'nuevoID';`                               |
| `class`       | `element.className`       | Representa la(s) clase(s) del elemento.                                                   | `element.className = 'nuevaClase';`                     |
|               | `element.classList`       | Manipula las clases del elemento de forma individual.                                     | `element.classList.add('otraClase');`                   |
| `src`         | `element.src`             | Representa la URL de la fuente de un elemento `<img>`, `<script>`, `<iframe>`, etc.        | `element.src = 'nuevaImagen.jpg';`                      |
| `href`        | `element.href`            | Representa la URL a la que apunta un enlace `<a>` o `<link>`.                              | `element.href = 'https://nuevaURL.com';`                |
| `value`       | `element.value`           | Representa el valor de un elemento de formulario `<input>`, `<textarea>`, `<select>`, etc. | `element.value = 'nuevoValor';`                         |
| `style`       | `element.style`           | Representa los estilos en línea del elemento.                                             | `element.style.color = 'red';`                          |
| `title`       | `element.title`           | Representa el texto que aparece como una información sobre herramientas (tooltip).         | `element.title = 'Nueva información sobre herramientas';`|
| `alt`         | `element.alt`             | Representa el texto alternativo para una imagen.                                          | `element.alt = 'Texto alternativo';`                    |
| `disabled`    | `element.disabled`        | Indica si un elemento de formulario está deshabilitado.                                    | `element.disabled = true;`                              |
| `checked`     | `element.checked`         | Indica si un elemento `<input>` de tipo checkbox o radio está seleccionado.                | `element.checked = true;`                               |

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de Acceso a Atributos</title>
</head>
<body>
    <div id="miDiv" class="miClase" title="Este es un div">Contenido del Div</div>
    <img id="miImagen" src="imagen.jpg" alt="Descripción de la imagen">
    <a id="miEnlace" href="https://ejemplo.com">Enlace de Ejemplo</a>
    <input id="miInput" type="text" value="Valor inicial">
    <button id="miBoton">Botón</button>

    <script>
        let div = document.getElementById('miDiv');
        div.id = 'nuevoID';
        div.className = 'nuevaClase';
        div.title = 'Nuevo título del div';
        
        let img = document.getElementById('miImagen');
        img.src = 'nuevaImagen.jpg';
        img.alt = 'Nuevo texto alternativo';

        let link = document.getElementById('miEnlace');
        link.href = 'https://nuevaURL.com';

        let input = document.getElementById('miInput');
        input.value = 'Nuevo valor';

        let button = document.getElementById('miBoton');
        button.disabled = true;

        console.log('Atributos modificados con éxito.');
    </script>
</body>
</html>
```

# Fragmentos en JS

En JavaScript, un DocumentFragment es un tipo especial de nodo del DOM que actúa como un contenedor ligero para otros nodos. A diferencia de los elementos normales del DOM, un DocumentFragment no forma parte del árbol del DOM. Esto significa que las modificaciones realizadas a los nodos dentro de un DocumentFragment no desencadenan reflujo (re-renderizado) del documento, lo que lo hace muy eficiente para ciertas operaciones.

## Características y Ventajas de DocumentFragment
- Eficiencia: Las operaciones realizadas sobre un DocumentFragment son más eficientes porque no afectan inmediatamente el árbol del DOM. Puedes hacer múltiples modificaciones y luego insertar el fragmento en el DOM de una sola vez, reduciendo así el número de reflujo y re-renderizado del documento.

- Manipulación de Grupos de Nodos: Permite manipular y manejar grupos de nodos de manera más sencilla. Puedes agregar, quitar o mover un conjunto de nodos dentro del fragmento antes de insertarlo en el DOM.

- Sin Contenedor: Cuando un DocumentFragment se inserta en el DOM, sus hijos se insertan en lugar de él. Esto significa que el fragmento mismo no se convierte en un nodo en el DOM, solo sus hijos lo hacen.

## Uso de documentFragment

```js
// Crear un nuevo DocumentFragment
let fragment = document.createDocumentFragment();

// Crear y agregar nuevos elementos al fragmento
for (let i = 0; i < 5; i++) {
  let newDiv = document.createElement('div');
  newDiv.textContent = 'Elemento ' + (i + 1);
  fragment.appendChild(newDiv);
}

// Insertar el fragmento en el DOM
document.body.appendChild(fragment);
```

# Utilizacion de la etiqueta template

El elemento <template> es un contenedor para almacenar fragmentos de HTML que no se renderizan directamente en la página, pero que se pueden clonar e insertar en el DOM mediante JavaScript.

```html
<template id="myTemplate">
  <div class="template-content">Este es el contenido del template.</div>
</template>

<script>
  // Obtén el elemento template
  let template = document.getElementById("myTemplate");

  // Clona el contenido del template
  let clone = document.importNode(template.content, true);

  // Inserta el contenido clonado en el DOM
  document.body.appendChild(clone);
</script>
```

