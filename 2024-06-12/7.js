/*
    Trabajar en funciones con un numero de argumentos variables
*/

function ejemplo1(...numeros) {
    console.log(numeros); // un array de numeros

}

ejemplo1(1);
ejemplo1(1, 2);
ejemplo1(1, 2, 3);

function ejemplo2(numeros) {
    console.log(numeros); // un array de numeros
}

ejemplo2([1]);
ejemplo2([1, 2]);
ejemplo2([1, 2, 3]);


function sumarMal() {
    // resultado = numero1 + numero2;
    // variable global y funcion
    resultado = 0;

    // variable local y no funciona
    // let resultado = 0;
    for (const numero of numeros) {
        resultado += numero;
    }
}


function sumarOpcion1(numeros) {
    let resultado = 0;
    for (const numero of numeros) {
        resultado += numero;
    }
    return resultado;
}

function sumarOpcion2(...numeros) {
    let resultado = 0;
    for (const numero of numeros) {
        resultado += numero;
    }
    return resultado;

}


let numero1 = 10;
let numero2 = 20;
let resultado = 0;

const numeros = [1, 2, 3];



sumarMal();
console.log(resultado);

resultado = sumarOpcion1([numero1, numero2, ...numeros]);
resultado = sumarOpcion1([numero1, numero2, numeros[0], numeros[1], numeros[2]]);

resultado = sumarOpcion2(numero1, numero2, ...numeros);
resultado = sumarOpcion2(numero1, numero2, numeros[0], numeros[1], numeros[2]);

console.log(resultado);