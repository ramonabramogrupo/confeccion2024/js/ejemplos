/**
 * quiero una funcion que reciba dos argumentos
 * 
 * el primer argumento es un array de numeros
 * el segundo argumento es el numero a buscar
 * 
 * la funcion debe retornar el numero de veces que se repite el numero en el array
 * 
 * Si no le pasas ningun numero debe buscar el numero 1
 * 
 * buscar([1,2,3,1,2,3,1,2,3],1) => 3
 * 
 * buscar([1,2,3,1,2,3,1,2,3],4) => 0
 * 
 * buscar([1,2,3,1,2,3,1,2,3]) => 3
 */

// funciones

function buscar(numeros, numero = 1) {

    let resultado = 0;

    // recorrer el array y buscar el numero pasado como segundo argumento

    // for (let c = 0; c < numeros.length; c++) {
    //     let valor = numeros[c];
    //     // compruebo si el numero leido coincide con el segundo argumento
    //     if (valor == numero) {
    //         resultado++;
    //     }
    // }

    // for (let valor of numeros) {
    //     // compruebo si el numero leido coincide con el segundo argumento
    //     if (valor == numero) {
    //         resultado++;
    //     }

    // }

    numeros.forEach(function (valor) {
        // compruebo si el numero leido coincide con el segundo argumento
        if (valor == numero) {
            resultado++;
        }
    });

    return resultado;
}

// variables y constantes

const div = document.querySelector('div');

let repeticiones = 0;

let numero = 3; // numero a buscar
const numeros = [3, 3, 3, 3, 2, 3, 1, 2, 3]; // array de numeros


// entradas

// procesamiento
repeticiones = buscar(numeros, numero);

// salidas

div.innerHTML = numeros; // al intentar mostrar el array en el div llama al metodo toString que convierte el array en un string donde todos los elementos del array se separan con comas
div.innerHTML += '<br>';
div.innerHTML += numero;
div.innerHTML += '<br>';
div.innerHTML += repeticiones;

