/**
 * quiero una funcion llamada menu que devuelva un string
 * que sea una lista no numerada donde como elementos estaran los elementos del array que le paso como argumento
 * menu(['home','about','contact'])
 * 
 * la salida de la funcion sera:
 * <ul>
 * <li>home</li>
 * <li>about</li>
 * <li>contact</li>
 * </ul>
 * 
 * Si no le paso el argumento me debe crear el siguiente menu
 * menu();
 * <ul>
 * <li>home</li>
 * </ul>
 */

/**
 * Una función que genera una lista desordenada basada en el array de elementos de entrada.
 *
 * @param {Array} elementos - Un array de elementos que se listarán en la lista desordenada. Por defecto se utiliza ['home'] si no se proporciona.
 * @return {String} La cadena HTML de la lista desordenada generada.
 */

function menu(elementos = ['home']) {

    //elementos
    //['home','about','contact']
    //['home']

    let acumulador = "<ul>";

    for (const elemento of elementos) {
        acumulador += `<li>${elemento}</li>`
    }

    // for (let contador = 0; contador < elementos.length; contador++) {
    //     let elemento = elementos[contador];

    //     acumulador += `<li>${elemento}</li>`
    // }

    acumulador += "</ul>";
    return acumulador;

}

const div = document.querySelector("#menu");

let salida = "";

salida = menu(['home', 'about', 'contact', 'login']);
// <ul><li>home</li><li>about</li><li>contact</li><li>login</li></ul>
// salida = menu();
// <ul><li>home</li></ul>

div.innerHTML = salida;

