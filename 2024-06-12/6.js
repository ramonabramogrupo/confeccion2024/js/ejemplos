/*
quiero una funcion que le paso un array con los titulos y el autor del libro

Debe mostrar estos datos en el template de html
*/

/**
 * Muestra en el div salida los libros
 * @param {Array} elementos : son libros. Cada elemento es otro array ["titulo","autor"]
 */
function mostrar(elementos) {
    // donde voy a dibujar los datos
    const div = document.querySelector('#salida');

    // fragmento donde en cada vuelto voy a dibujar el template
    const fragmento = document.createDocumentFragment();

    // template a utilizar
    const template = document.querySelector('template').content;

    for (const elemento of elementos) {
        const clon = template.cloneNode(true);
        clon.querySelector('h2').textContent = elemento[0];
        clon.querySelector('div>div').textContent = elemento[1];
        fragmento.appendChild(clon);
        div.appendChild(fragmento);
    }

}

const libros = [
    ["El principito", "Antoine de Saint-Exupéry"],
    ["El gran Gatsby", "F. Scott Fitzgerald"],
    ["Cien años de soledad", "Gabriel Garcia Marquez"],
    ["El amor en tiempos de colera", "Gabriel Garcia Marquez"]
];

mostrar(libros);