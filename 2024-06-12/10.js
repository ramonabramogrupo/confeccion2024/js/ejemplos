/**
 *  funcion con argumentos variables
 */

/**
 * Funcion que recibe una operacion a realizar y un conjunto de numeros. Devuelve el resultado de la operacion
 * @param {string} operador  : operacion a realizar ("sumar", "multiplicar")
 * @param  {...enteros} numeros : numeros a multiplicar
 * @return {entero} el resultado de la operacion
 */
function operacion(operador, ...numeros) {
    let resultado = 0;

    switch (operador) {
        case "sumar":
            resultado = 0;

            // utilizo for of
            // for (const numero of numeros) {
            //     resultado += Number(numero);
            // }

            // opcion2
            // utilizo foreach
            numeros.forEach(function (numero) {
                resultado += Number(numero)
            });
            // opcion3
            // utilizo una funcion separada para sumar
            // resultado=sumar(numeros);
            break;
        case "multiplicar":
            resultado = 1;

            // opcion 1
            // for of
            // for (const numero of numeros) {
            //     resultado *= numero;
            // }

            // opcion 2
            // foreach
            numeros.forEach(function (numero) {
                resultado *= numero
            });

            // opcion 3
            // utilizo una funcion separada para multiplicar
            // resultado = multiplicar(numeros);
            break;
    }
    return resultado;
}

function sumar(numeros) {
    let resultado = 0;
    for (const numero of numeros) {
        resultado += Number(numero);
    }
    return resultado;
}

function multiplicar(numeros) {
    let resultado = 1;
    for (const numero of numeros) {
        resultado *= numero;
    }
    return resultado;
}




// constantes que apunten a los dos botones
const botonSumar = document.querySelector('#sumar');
const botonMultiplicar = document.querySelector('#multiplicar');


// creamos un escuchado a cada boton

botonSumar.addEventListener('click', function (event) {
    let resultado = 0;

    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');
    const inputNumero3 = document.querySelector('#numero3');
    const inputNumero4 = document.querySelector('#numero4');

    resultado = operacion("sumar", inputNumero1.value, inputNumero2.value, inputNumero3.value, inputNumero4.value);


    divSalida.textContent = "la suma vale " + resultado;

});

botonMultiplicar.addEventListener('click', function (event) {
    let resultado = 0;

    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');
    const inputNumero3 = document.querySelector('#numero3');
    const inputNumero4 = document.querySelector('#numero4');

    resultado = operacion("multiplicar", inputNumero1.value, inputNumero2.value, inputNumero3.value, inputNumero4.value);

    divSalida.textContent = "El producto vale " + resultado;
});

// escuchador de boton sumar
// utilizamos la funcion operacion


// escuchador de boton multiplicar
// utilizamos la funcion operacion

// resultado = operacion("sumar", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

// console.log(resultado); // 55

// resultado = operacion("multiplicar", 1, 2, 3);

// console.log(resultado); // 6
