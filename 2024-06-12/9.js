/*
ejemplos de paso de argumentos variables
*/

function ejemplo1(...datos) {
    console.log(datos);
}

ejemplo1("hola", 2, [3, 4], "adios");
// ["hola", 2, [3, 4], "adios"]

function ejemplo2(datos) {
    console.log(datos);
}

ejemplo2("hola", 2, [3, 4], "adios");
// hola

ejemplo2(["hola", 2, [3, 4], "adios"]);
// ["hola", 2, [3, 4], "adios"]


function ejemplo3(numero1, numero2, numero3) {
    console.log(numero1, numero2, numero3);
}

ejemplo3(1, 2, 3);
// 1 2 3

ejemplo3(1, 2, 3, 4);
// 1 2 3

const numeros = [1, 2, 3];
ejemplo3(numeros)
// [1, 2, 3] undefined undefined

ejemplo3(numeros[0], numeros[1], numeros[2]);
// 1 2 3

ejemplo3(...numeros);
// 1 2 3

function ejemplo4() {
    console.log(arguments);
}

ejemplo4(1, 2, 3);
// 1 2 3

function ejemplo5(operacion, ...datos) {
    console.log(operacion, datos);
}

ejemplo5("sumar", 1, 2, 3, 4, 5);
// operacion => "sumar"
// datos => [ 1, 2, 3, 4, 5 ]
