// constantes y variables globales

const imagen = document.querySelector("#imagen");

const palabra = obtenerPalabra(palabras);

// array donde voy a guardar los caracteres que vaya acertando
const aciertos = [];


// comprobar la palabra
console.log(palabra);

// dibujar la palabra en el div salida
// donde cada caracter es un div
dibujar(palabra, '#salida');


// coloca la imagen sin errores
imagen.src = 'imgs/' + imagenes.pop();

// escuchador

document.querySelector("button").addEventListener("click", function (event) {

    // variable para contar numero de aciertos
    let numeroAciertos = 0;

    // constante que apunte a la caja de texto
    const inputLetra = document.querySelector("#itexto");

    // obtengo el valor de la caja de texto
    let letra = inputLetra.value;

    // borro la caja de texto
    inputLetra.value = "";

    // compruebo si la letra es correcta
    if (comprobar(palabra, letra, aciertos)) {

        // acerte
        dibujarAciertos(aciertos, '#salida');

        // compruebo si he acertado todas las letras
        // he ganado

        // necesito saber cuantos elementos tengo en el array
        // aciertos (que tengan algo, que no esten undefined)
        aciertos.forEach(function (caracter) {
            if (caracter != undefined) {
                numeroAciertos++;
            }
        });



        if (numeroAciertos == palabra.length) {

            // quiero bloquear el boton para que el usuario no pueda jugar
            this.disabled = true;

            setTimeout(function () {
                // mensaje 
                alert("Has ganado");
                // recargo la pagina para volver a empezar
                location.reload();
            }, 500);

        }
    } else {
        //falle
        dibujarErrores(letra, '#errores');
        imagen.src = 'imgs/' + imagenes.pop();

        // si en el array de imagenes no hay mas 
        // es que he perdido
        if (imagenes.length == 0) {

            // quiero bloquear el boton para que el usuario no pueda jugar
            this.disabled = true;

            setTimeout(function () {
                // mensaje
                alert("Has perdido");
                location.reload(); // recargo la pagina para volver a empezar
            }, 500);
        }
    }

});






