// palabras para el juego

const palabras = [
    'aeropuerto',
    'aguacate',
    'aguja',
    'alfajor',
    'alfiler',
    'alfiler',
    'boligrafo',
    'botella',
    'caballo',
    "pepinillo",
    "palabras",
    "platanos",
    "ratoncillos",
    "academia",
    "manzana"
];

// array con las imagenes
const imagenes = [
    'ahorcado_0.png', // con 6 errores
    'ahorcado_1.png', // con 5 errores
    'ahorcado_2.png', // con 4 errores
    'ahorcado_3.png', // con 3 errores
    'ahorcado_4.png',  // con 2 errores
    'ahorcado_5.png', // con 1 error
    'ahorcado_6.png' // sin errores
];