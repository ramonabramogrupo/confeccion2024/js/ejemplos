
/**
 * Funcion que devuelve una palabra de forma aleatoria de la lista de palabras
 * pasada como argumento (array)
 * 
 * @param {Array} todasPalabras - Array de palabras
 * @returns {String} - Palabra
 */

function obtenerPalabra(todasPalabras) {
    let palabra = "";
    let indice = 0;

    // calculo el indice de forma aleatoria
    indice = Math.floor(Math.random() * todasPalabras.length);

    // devuelvo la palabra que indica el indice
    palabra = todasPalabras[indice];

    return palabra;
}

/**
 * Esta funcion dibuja las cajas(divs) correspondientes a cada caracter de la palabra en el selector especificado.
 *
 * @param {string} palabra - La palabra que se va a dibujar.
 * @param {string} selector - El selector del elemento donde se dibujarán las cajas.
 * @returns {void}
 */

function dibujar(palabra, selector) {
    // apuntar al elemento donde quiero dibujar
    const salida = document.querySelector(selector);

    // crear un div por cada caracter de la palabra

    // si queremos realizarlo con un foreach
    // const caracteres = palabra.split("");

    // caracteres.forEach(function (caracter) {
    //     const div = document.createElement("div");
    //     div.textContent = " ";
    //     salida.appendChild(div);
    // });

    // si lo realizo con un bucle for
    for (let c = 0; c < palabra.length; c++) {
        const div = document.createElement("div");
        div.innerHTML = "&nbsp;";
        salida.appendChild(div);
    }

}

/**
 * Esta función compara la letra ingresada por el usuario con las letras de la palabra.
 * Si la letra ingresada coincide con alguna letra de la palabra, se muestra en la casilla correspondiente y se añade al array de aciertos. Si la letra acertada se encuentra en el array de aciertos no cuenta como acierto.
 * Si la letra ingresada no coincide con ninguna letra de la palabra, se muestra en la sección de errores.
 * Si el jugador ha acertado todas las letras de la palabra, se muestra un mensaje de victoria y se recarga la página.
 * Si el jugador ha fallado todas las oportunidades, se muestra un mensaje de derrota y se recarga la página.
 *
 * @param {string} palabra - La palabra que el jugador debe adivinar.
 * @param {object} letra - letra ingresada
 * @param {array} selectorCaracteres - Los elementos div donde se muestran las letras de la palabra.
 * @param {array} selectorErrores - Los elementos div donde se muestran las letras falladas
 * @param {number} aciertos - El número de letras que el jugador ha acertado.
 * @return {void} 
 */

function comprobar(palabra, letra, aciertos) {
    // variable llave que me indica si el caracter
    // esta en la palabra
    let acierto = false;

    // recorro la palabra y si acierto añado la letra
    // al array de aciertos
    // si no acierto la variable acierto se queda en false
    for (let c = 0; c < palabra.length; c++) {
        if (palabra[c].toLocaleLowerCase() == letra.toLocaleLowerCase()) {
            acierto = true;
            // añado al array de aciertos la letra
            aciertos[c] = letra.toLocaleLowerCase();
        }
    }

    return acierto;
}


function dibujarAciertos(arrayAciertos, selectorCaracteres) {
    // estos son los divs donde coloco las letras acertadas
    const divsCaracteres = document.querySelectorAll(selectorCaracteres + ">div");

    arrayAciertos.forEach(function (caracter, indice) {
        divsCaracteres[indice].textContent = caracter;
    });
}


function dibujarErrores(letraIncorrecta, selectorErrores) {
    // dibujar la letra incorrecta en el selector de errores
    // este es el div donde añado un div por cada letra fallada
    const divErrores = document.querySelector(selectorErrores);

    const div = document.createElement("div");
    div.textContent = letraIncorrecta;
    divErrores.appendChild(div);
}

