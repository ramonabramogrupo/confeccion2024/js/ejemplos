[TOC]

# Objetivo
Desarrollar un juego del ahorcado en JavaScript, donde los alumnos aprenderán a:

- Seleccionar una palabra aleatoriamente de un array.
- Mostrar divs para los caracteres de la palabra.
- Permitir que el usuario ingrese letras para adivinar la palabra.
- Mostrar partes de una imagen del ahorcado cuando el usuario falle.
- Cuando el usuario adivine la palabra le indicara que gano

# 1 paso

Crear un archivo index.html con la estructura basica de html5

Crear una carpeta denominada css donde vamos a incorporar los archivos de estilos.

Crear una carpeta denominada js donde vamos a incorporar los archivos de js.

Crear una carpeta denominada imgs donde colocamos las imagenes pasadas por el profesor.

# 2 paso

Descargar los archivos css pasados por el profesor y colocarlos en la carpeta css

Cargar los archivos css en la pagina web

# 3 paso

Crear una estructura en la pagina index.html

```html
<section>
		<div>
			<h1>Juego del Ahorcado</h1>
			<div id="tablero">
				<div>
					<form action="#" method="get" accept-charset="utf-8">
						<input type="text" id="itexto" name="texto" value="" placeholder="">
						<button type="button">Comprobar</button>
					</form>
				</div>
			</div>
			<div id="salida">

			</div>

			<img id="imagen" src="imgs/ahorcado_6.png">
			<div id="errores"></div>
		</div>
	</section>
```

# 4 paso

Crear un archivo denominado datos.js donde creamos una variable denominada palabras de tipo array con las palabras a jugar

Cargar el archivo js en el archivo web

# 5 paso

Crear un archivo denominado funciones.js en donde colocaremos las funciones necesarias para el ejercicio

Cargar el archivo funciones.js en la pagina web

# 6 paso

Creo un archivo llamado 1.js es donde esta el codigo de mi pagina (juego
)

Cargar el archivo 1.js en la pagina web


# 7 paso

En el archivo 1.js creamos una constante denominada palabra donde debe almacenar la palabra que tenemos que adivinar.

Para calcular esa palabra lo vamos a realizar a traves de una funcion.

Crear una funcion denominada obtenerPalabra en funciones.js que sea capaz de devolver una palabra de forma aleatoria del array de palabras

```js

/**
 * Funcion que devuelve una palabra de forma aleatoria de la lista de palabras
 * pasada como argumento (array)
 * 
 * @param {Array} todasPalabras - Array de palabras
 * @returns {String} - Palabra
 */

function obtenerPalabra(todasPalabras) {
    let palabra = "";

    return palabra;
}
```

# 8 paso

Crear una funcion denominada dibujar.

Esta funcion dibuja las cajas correspondientes a cada caracter de la palabra en el selector especificado.

```js
/**
 * Esta funcion dibuja las cajas(divs) correspondientes a cada caracter de la palabra en el selector especificado.
 *
 * @param {string} palabra - La palabra que se va a dibujar.
 * @param {string} selector - El selector del elemento donde se dibujarán las cajas.
 * @returns {void}
 */

function dibujar(palabra, selector) {

}

```

Para comprobar el funcionamiento 

```js
dibujar("pepe","#salida");
```

Tendria que crear en la web

```html
<div id="salida">
    <div> </div> <div> </div> <div> </div> <div> </div>
</div>

```

# 9 paso

En el archivo 1.js vamos a crear una :

- constante aciertos:  nos va almacenando las letras acertadas


# 10 paso

Crear una funcion para comprobar si el caracter escrito esta en la palabra a adivinar.

La funcion se denominara comprobar

```js
comprobar(palabra,caracter,selectorDivsLetras,selectorDivsFallos,aciertos);
comprobar("arbol","a","#salida","#errores",aciertos)

```

Esta función compara la letra ingresada por el usuario con las letras de la palabra.
 
Si la letra ingresada coincide con alguna letra de la palabra, se muestra en la casilla correspondiente y se añade al array de aciertos. Si la letra acertada se encuentra en el array de aciertos no cuenta como acierto.
 
Si la letra ingresada no coincide con ninguna letra de la palabra, se muestra en la sección de errores.
 
Si el jugador ha acertado todas las letras de la palabra, se muestra un mensaje de victoria y se recarga la página.
 
Si el jugador ha fallado todas las oportunidades, se muestra un mensaje de derrota y se recarga la página.

```js
/**
 * Esta función compara la letra ingresada por el usuario con las letras de la palabra.
 * Si la letra ingresada coincide con alguna letra de la palabra, se muestra en la casilla correspondiente y se añade al array de aciertos. Si la letra acertada se encuentra en el array de aciertos no cuenta como acierto.
 * Si la letra ingresada no coincide con ninguna letra de la palabra, se muestra en la sección de errores.
 * Si el jugador ha acertado todas las letras de la palabra, se muestra un mensaje de victoria y se recarga la página.
 * Si el jugador ha fallado todas las oportunidades, se muestra un mensaje de derrota y se recarga la página.
 *
 * @param {string} palabra - La palabra que el jugador debe adivinar.
 * @param {object} letra - letra ingresada
 * @param {array} selectorCaracteres - Los elementos div donde se muestran las letras de la palabra.
 * @param {array} selectorErrores - Los elementos div donde se muestran las letras falladas
 * @param {number} aciertos - El número de letras que el jugador ha acertado.
 * @return {void} 
 */

function comprobar(palabra, letra, selectorCaracteres, SelectorErrores,aciertos) {
}
```

