// crear un coche como objeto

const coche1 = {
    // miembros

    // propiedades
    // clave: valor
    marca: "renault",
    modelo: "clio",
    color: "gris",
    cilindrada: 1.6,

    // metodos

    cambiarColor: function (nuevoColor) {
        this.color = nuevoColor;
    }

};


// leer el color del coche
console.log(coche1.color); // gris

coche1.cambiarColor("azul");
console.log(coche1.color); // azul