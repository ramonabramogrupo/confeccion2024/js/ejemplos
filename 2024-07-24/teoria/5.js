/*
Array y objectos
*/

// array con datos de un alumno

// un array es un objeto de tipo array
const alumno = [
    'ramon', // alumno[0]
    23, // alumno[1]
    'santander', // alumno[2]
    'masculino' // alumno[3]
];

// alumno como objeto
// creado de forma literal
const objAlumno = {
    nombre: 'ramon', // objAlumno.nombre o objAlumno['nombre']
    edad: 23, // objAlumno.edad o objAlumno['edad']
    poblacion: 'santander', // objAlumno.poblacion o objAlumno['poblacion']
    genero: 'masculino' // objAlumno.genero o objAlumno['genero']
};

// leer nombre del alumno
console.log(alumno[0]);
console.log(objAlumno['nombre'], objAlumno.nombre)

// leer genero del alumno
console.log(alumno[3]);
console.log(objAlumno['genero'], objAlumno.genero)


// leer edad del alumno
console.log(alumno[1]);
console.log(objAlumno['edad'], objAlumno.edad)

// quiero crear otro alumno

const alumno1 = [
    'ana', // alumno1[0]
    40, // alumno1[1]
    'laredo', // alumno1[2]
    'femenino' // alumno1[3]
];

const objAlumno1 = {
    nombre: 'ana', // objAlumno1.nombre o objAlumno1['nombre']
    edad: 40, // objAlumno1.edad o objAlumno1['edad']
    poblacion: 'laredo', // objAlumno1.poblacion o objAlumno1['poblacion']
    genero: 'femenino' // objAlumno1.genero o objAlumno1['genero']
};


// me vendria muy bien tener un molde para crear alumnos