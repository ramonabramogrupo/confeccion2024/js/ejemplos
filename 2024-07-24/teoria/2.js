// crear un objeto propio
// utilizando la nomemclatura de json

const coche1 = {
    // propiedades
    // clave: valor
    marca: "renault",
    modelo: "clio",
    color: "gris",
    cilindrada: 1.6
};

// leer el color del coche
console.log(coche1.color); // gris

// leer la cilindrada
console.log(coche1.cilindrada); // 1.6

// crear un array
const coche1Array = [
    "renault",
    "clio",
    "gris",
    1.6
];

// leer el color del coche
console.log(coche1Array[2]);

// leer la cilindrada
console.log(coche1Array[3]);

// resumiendo

// un objeto es una coleccion de propiedades (es como un array asociativo)
// un array es una coleccion de elementos