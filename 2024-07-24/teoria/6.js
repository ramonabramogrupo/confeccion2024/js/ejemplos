class Alumno {
    nombre;
    edad = 0;
    poblacion;
    genero;
    datos() {
        return this.nombre + " " + this.edad
    }
}


// creo un alumno nuevo
const alumno1 = new Alumno();

// creo un alumno nuevo
const alumno2 = new Alumno();

// inicializar el alumno1
alumno1.nombre = 'ramon';
alumno1.edad = 23;
alumno1.poblacion = 'santander';
alumno1.genero = 'masculino';

// inicializar el alumno2
alumno2.nombre = 'maria';
alumno2.edad = 25;
alumno2.poblacion = 'santander';
alumno2.genero = 'femenino';

console.log(alumno1.datos());
console.log(alumno2.datos());

