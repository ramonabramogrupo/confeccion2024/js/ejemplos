// utilizar el metodo constructor y el metodo toString

// el metodo constructor se utiliza para inicializar el objeto

// el metodo toString se utiliza para mostrar el objeto cuando se imprime

class Alumno {
    id;
    nombre;
    email;

    // este metodo se llama cuando creo el objeto
    constructor(id, nombre, email) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
    }

    // este metodo se llama cuando se imprime el objeto
    toString() {
        return this.id + " " + this.nombre + " " + this.email;
    }
}

// creo un alumno nuevo
const alumno1 = new Alumno(1, 'ramon', 'ramon@alpe.es');
console.log(alumno1);

// imprimo el objeto
console.log(alumno1.toString());
document.write(alumno1); // llama automaticamente al toString cuando quiero imprimir


