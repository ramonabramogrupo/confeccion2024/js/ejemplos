// creo un objeto utilizando new
const coche1 = new Object();

// le agrego propiedades
coche1.marca = "renault";
coche1.modelo = "clio";
coche1.color = "gris";
coche1.cilindrada = 1.6;

// crear un objeto propio
// utilizando la nomemclatura de json (literal)

const coche2 = {
    // propiedades
    // clave: valor
    marca: "renault",
    modelo: "clio",
    color: "gris",
    cilindrada: 1.6,

};

// agregar propiedades
coche2.ruedas = 4;
coche2.marc = "ford"; // hay que tener cuidado a la hora de escribir las propiedades

console.log(coche2);