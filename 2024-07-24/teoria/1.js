// dato primitivo de tipo string
let a = "ramon";

console.log(a.length); // 5

console.log(typeof (a)); // string

// lo que hace js es crear un objeto de tipo string
// con el valor de la variable a al pedir la longitud
// y despues lo elimina

// puedo crear un objeto de tipo string
// pero es absurdo (no lo vamos a realizar)
let b = new String("ramon");
console.log(b.length);
console.log(typeof (b)); // object
