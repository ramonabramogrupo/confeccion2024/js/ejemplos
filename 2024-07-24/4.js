/**
 * Crear una clase llamada Libro
 * 
 * Con las siguientes propiedades
 * titulo
 * autor
 * isbn
 * 
 * Quiero un metodo llamado obtenerDetalles
 * que me devuelva una cadena con los datos del libro
 * 
 * 
 * Crear el metodo constructor que me inicialice todas las propiedades
 * a los valores pasados como argumentos
 * 
 * Crear otra clase llamada Libreria
 * 
 * propiedad libros que es un array donde puedo meter objetos de tipo libro
 * 
 * crear un metodo llamado agregarLibro que le pasas un libro y lo añade
 * 
 * Crear un metodo llamado listar
 * que me devuelva una cadena con el todos los libros de la librería
 */

// crear clase Libro
class Libro {
    //propiedades
    titulo;
    autor;
    isbn;
    // constructor
    constructor(titulo, autor, isbn) {
        this.titulo = titulo;
        this.autor = autor;
        this.isbn = isbn;
    }

    // metodos

    obtenerDetalles() {
        return `<div>Titulo:${this.titulo},Autor: ${this.autor},ISBN:${this.isbn}</div> `;
        // return 'Titulo:' + this.titulo + ',Autor: ' + this.autor + ',ISBN:' + this.isbn;
    }


}

// crear clase Libreria
class Libreria {
    //propiedades
    libros = [];

    // constructor
    constructor() {
        this.libros = [];
    }

    // metodos

    agregarLibro(libro) {
        this.libros.push(libro);
    }

    listar() {
        let salida = "";
        this.libros.forEach(function (libro) {
            salida += libro.obtenerDetalles();
        });
        return salida;
    }
}


const libro1 = new Libro("El princito", "Antoine de Saint-Exupéry", "123456789");
const libro2 = new Libro("El gran Gatsby", "F. Scott Fitzgerald", "987654321");

const libreria1 = new Libreria();


libreria1.agregarLibro(libro1);
libreria1.agregarLibro(libro2);
libreria1.agregarLibro(new Libro("Cien años de soledad", "Gabriel Garcia Marquez", "987654321"));


document.body.innerHTML += 'Listar los datos del primer libro<br>';
document.body.innerHTML += libro1.obtenerDetalles();


document.body.innerHTML += 'Listar los datos de todos los libros<br>';
document.body.innerHTML += libreria1.listar();