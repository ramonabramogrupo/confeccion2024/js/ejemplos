// quiero crear una clase con las siguientes propiedades
// para almacenar clientes
// id,nombre,apellidos,email,telefono
// quiero que tenga los metodos constructor y toString

// el metodo constructor quiero que inicialice el id, nombre y apellidos

// el metodo toString que devuelve un string con los valores de las propiedades
// dentro de una lista
/*
<ul>
    <li>id</li>
    <li>nombre</li>
    <li>apellidos</li>
    <li>email</li>
    <li>telefono</li>
</ul>
*/


// el metodo toUl quiero que devuelva un element de tipo ul con li para cada campo
/*
<ul>
    <li>id</li>
    <li>nombre</li>
    <li>apellidos</li>
    <li>email</li>
    <li>telefono</li>
</ul>
*/

// crearme tres objetos de tipo cliente y probar el objeto creado
// los objetos meterlos en un array llamado clientes

// 1,ramon,sanchez,ramon@,123456
// 2,maria,gonzalez,maria@,654321
// 3,pedro,lopez,pedro@,789012


// crear la clase

class Cliente {
    // propiedades
    id;
    nombre;
    apellidos;
    email;
    telefono;

    // constructor
    constructor(id, nombre, apellidos) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = '';
        this.telefono = '';
    }


    // toString
    toString() {
        return `<ul>
        <li>${this.id}</li>
        <li>${this.nombre}</li>
        <li>${this.apellidos}</li>
        <li>${this.email}</li>
        <li>${this.telefono}</li>
    </ul>`;
    }

    // creo el metodo toUl
    toUl() {
        const ul = document.createElement('ul');
        const li1 = document.createElement('li');
        const li2 = document.createElement('li');
        const li3 = document.createElement('li');
        const li4 = document.createElement('li');
        const li5 = document.createElement('li');

        li1.textContent = this.id;
        li2.textContent = this.nombre;
        li3.textContent = this.apellidos;
        li4.textContent = this.email;
        li5.textContent = this.telefono;

        ul.appendChild(li1);
        ul.appendChild(li2);
        ul.appendChild(li3);
        ul.appendChild(li4);
        ul.appendChild(li5);

        return ul;
    }


}

// crear los objetos
const clientes = [];

clientes.push(new Cliente(1, 'ramon', 'sanchez'));
clientes.push(new Cliente(2, 'maria', 'gonzalez'));
clientes.push(new Cliente(3, 'pedro', 'lopez'));

// clientes[0] = new Cliente(1, 'ramon', 'sanchez');
// clientes[1] = new Cliente(2, 'maria', 'gonzalez');
// clientes[2] = new Cliente(3, 'pedro', 'lopez');

document.body.appendChild(clientes[0].toUl());
document.body.appendChild(clientes[1].toUl());
document.body.appendChild(clientes[2].toUl());

document.write(clientes[0]);
document.body.innerHTML += clientes[1];