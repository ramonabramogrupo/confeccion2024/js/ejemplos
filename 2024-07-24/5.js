/*
El objetivo de este ejercicio es aplicar los conceptos de programación orientada a objetos en JavaScript creando y utilizando clases para gestionar el inventario de una tienda.

Los estudiantes crearán clases para representar productos y una tienda, y escribirán métodos para añadir, eliminar y listar producto

Clase Producto
    Propiedades:
        id
        nombre
        precio
    Metodos:
        toString() : devolver una cadena con todas las propiedades del producto
        obtenerDetalles(): devuelve un element de tipo con ul con todas las caracteristicas del producto
        constructor() : inicialice las propiedades a los valores pasados como argumentos

Clase Tienda
    Propiedades:
        nombre : el nombre de la tienda
        productos: es un array de productos
    Metodos:
        toString(): devuelve una cadena con todos los datos de la tienda
        listarProductos(): devuelve una cadena con todos los productos de la tienda
        addProducto(producto): anade un producto a la tienda
        removeProducto(id): elimina un producto de la tienda pasando su id
        constructor(): inicializar el nombre de la tienda pasado como argumento

*/

// Clase producto
class Producto {

    constructor(id, nombre, precio) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }
    toString() {
        return `id=${this.id},nombre=${this.nombre}, precio=${this.precio}`

    }
    obtenerDetalles() {
        const ul = document.createElement("ul");
        const li1 = document.createElement("li");
        const li2 = document.createElement("li");
        const li3 = document.createElement("li");

        ul.appendChild(li1);
        ul.appendChild(li2);
        ul.appendChild(li3);

        li1.textContent = `id=${this.id}`;
        li2.textContent = `nombre=${this.nombre}`;
        li3.textContent = `precio=${this.precio}`;

        return ul;


    }




}

// Clase Tienda
class Tienda {


    constructor(nombre) {
        this.nombre = nombre;
        this.productos = [];
    }

    toString() {
        return `nombre=${this.nombre}`;
    }

    listarProductos() {
        let salida = "";
        this.productos.forEach(producto => {
            salida += `<div>${producto}</div>`;
        });
        return salida;
    }

    addProducto(producto) {
        this.productos.push(producto);
    }


    removeProducto(id) {
        this.productos.forEach((producto, index) => {
            if (producto.id == id) {
                this.productos.splice(index, 1);
            }
        });
    }
}

const producto1 = new Producto(1, 'camisa', 100);
const producto2 = new Producto(2, 'pantalon', 200);

// listar los datos del producto 1
document.body.innerHTML += 'El producto 1 es ' + producto1;

// obtener los datos del producto 1 como element
document.body.appendChild(producto1.obtenerDetalles());

const tienda1 = new Tienda('Zara');

tienda1.addProducto(producto1);
tienda1.addProducto(producto2);

// listar los productos de la tienda
document.body.innerHTML += tienda1.listarProductos();

//eliminar el producto1
tienda1.removeProducto(1);

// listar los productos de la tienda
document.body.innerHTML += tienda1.listarProductos();
