// crear un objeto que contiene dos propiedades que son numero1 y numero 2
// ademas el objeto debe tener 4 metodos que me permitan
// sumar, restar, multiplicar y dividir
// los metodos devuelven el resultado de la operacion

// crear el objeto

const numeros = {
    numero1: 0,
    numero2: 0,

    sumar: function () {
        return this.numero1 + this.numero2;
    },
    restar: function () {
        return this.numero1 - this.numero2;
    },
    multiplicar: function () {
        return this.numero1 * this.numero2;
    },
    dividir: function () {
        if (this.numero2 == 0) {
            return "no se puede dividir por 0";
        }
        return this.numero1 / this.numero2;
    }
}


// probar el objeto creado
numeros.numero1 = 2;
numeros.numero2 = 4;

console.log(numeros.sumar()); // 6
console.log(numeros.restar()); // -2
console.log(numeros.multiplicar()); // 8
console.log(numeros.dividir()); // 0.5

