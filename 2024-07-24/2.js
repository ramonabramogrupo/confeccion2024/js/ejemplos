// quiero crear una clase con las siguientes propiedades
// para almacenar clientes
// id,nombre,apellidos,email,telefono
class Cliente {
    id;
    nombre;
    apellidos;
    email;
    telefono;
}

// quiero que me creeis dos clientes con los siguientes valores

// 1,ramon,sanchez,ramon@,123456
const cliente1 = new Cliente();
cliente1.id = 1;
cliente1.nombre = 'ramon';
cliente1.apellidos = 'sanchez';
cliente1.email = 'ramon@';
cliente1.telefono = '123456';

console.log(cliente1); // {id: 1, nombre: 'ramon', apellidos: 'sanchez', email: 'ramon@', telefono: '123456'}

// 2,maria,gonzalez,maria@,654321
const cliente2 = new Cliente();
cliente2.id = 2;
cliente2.nombre = 'maria';
cliente2.apellidos = 'gonzalez';
cliente2.email = 'maria@';
cliente2.telefono = '654321';

console.log(cliente2); // {id: 2, nombre: 'maria', apellidos: 'gonzalez', email: 'maria@', telefono: '654321'}

// puedo crear un array de clientes
const clientes = [
    new Cliente(), // clientes[0]
    new Cliente() // clientes[1]
];

// para meter los datos del primer cliente
clientes[0].id = 1;
clientes[0].nombre = 'ramon';
clientes[0].apellidos = 'sanchez';
clientes[0].email = 'ramon@';
clientes[0].telefono = '123456';

// para meter los datos del segundo cliente
clientes[1].id = 2;
clientes[1].nombre = 'maria';
clientes[1].apellidos = 'gonzalez';
clientes[1].email = 'maria@';
clientes[1].telefono = '654321';

console.log(clientes); // [{id: 1, nombre: 'ramon', apellidos: 'sanchez', email: 'ramon@', telefono: '123456'}, {id: 2, nombre: 'maria', apellidos: 'gonzalez', email: 'maria@', telefono: '654321'}]