/**
 * Recorrer un string y contar las vocales
 * quiero que en vocales esten el numero de vocales del 
 * string texto
 * 
 * utilizamos el for of 
 * utilizamos el for
 * 
 * mostrar en consola la variable vocales y texto
 * 
 */

let texto = "hola mundo";
let vocales = 0;

// bucle for of

for (const caracter of texto) {
    // comprobar si es una vocal
    // pasandolo a minuculas

    switch (caracter.toLowerCase()) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            vocales++;
            break;
    }

    // otra opcion seria el if
    // caracter=caracter.toLowerCase();
    // if(caracter=='a'||caracter=='e'||caracter=='i'||caracter=='o'||caracter=='u'){
    //     vocales++;
    // }



}


console.log(vocales, texto);
vocales = 0;

// bucle for

for (let contador = 0; contador < texto.length; contador++) {
    const caracter = texto[contador].toLowerCase();

    switch (caracter) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            vocales++;
            break;
    }
}


console.log(vocales, texto);