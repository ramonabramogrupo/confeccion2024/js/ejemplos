/**
 * Producto de los 100 primeros numeros enteros pares
 * 
 * Mostrar el producto en el div Salida
 * 
 * producto=2*4*6*8*.....*100
 * 
 */

const divSalida = document.querySelector('#salida');

let producto = 0;
let contador = 0;

for (contador = 2, producto = 1; contador <= 100; contador++) {

    // comprobar si el contador es par
    if (contador % 2 == 0) { // si el resto de la division es 0 es par
        producto *= contador;
        // console.log('contador ' + contador);
        // console.log('producto ' + producto);
    }

}


// otra forma sin if

for (contador = 1, producto = 1; contador <= 100; contador += 2) {
    producto *= contador;
}

divSalida.innerHTML = producto;