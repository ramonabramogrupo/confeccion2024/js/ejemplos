/**
 * recorrer un array con foreach y mostrar cada elemento en la consola
 */

const arrayNombres = [
    "Juan", "Maria", "Pedro", "Ana", "Luis", "Carlos", "Laura", "Jorge", "Sofia", "Marta"
];


// utilizando funcion anonima
arrayNombres.forEach(function (nombre) {
    console.log(nombre);
});

// utilizando funcion flecha
arrayNombres.forEach((nombre) => {
    console.log(nombre);
});
