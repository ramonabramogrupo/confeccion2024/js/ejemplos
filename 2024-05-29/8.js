/**
 * mostrar un array completo en la consola
 * Utilizando for of
 */

const arrayNombres = ["Juan", "Maria", "Pedro", "Ana", "Luis", "Carlos", "Laura", "Jorge", "Sofia", "Marta"];

/**
 * podemos utilizar
 * for
 * while
 */

for (const valor of arrayNombres) {
    console.log(valor);
}

// si quiero el indice

for (const [indice, valor] of arrayNombres.entries()) {
    console.log(indice, valor);
}


