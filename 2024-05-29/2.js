/**
 * Mostrar la suma de los 50 primeros numeros enteros
 * 
 * En la caja salida mostrar la suma de los 50 primeros numeros enteros
 * suma=1+2+3+4+5+6+7+8+ ... + 50
 */

// variables y constantes
const divSalida = document.querySelector('#salida');
const INICIO = 1;
const FIN = 50;

let suma = 0; //acumulador
let contador = 0;

// procesamiento
for (contador = INICIO, suma = 0; contador <= FIN; contador++) {
    suma += contador;
}

// salidas
divSalida.innerHTML = suma;
