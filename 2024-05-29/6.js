/**
 * Leer el array de numeros y escribir los numeros en los div
 *
 * todos : Todos los numeros
 * pares : Los pares
 * impares : Los impares
 *
 * Para realizarlo utilizar
 *
 * - todos : metodo join
 * - pares : for
 * - impares : for
 *
 */

// variables y constantes

// constantes
const divTodos = document.querySelector('#todos');
const divPares = document.querySelector('#pares');
const divImpares = document.querySelector('#impares');

// array con 50 numeros enteros
const arrayNumeros = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50
];

// variables
let todos = "";
let pares = "";
let impares = "";

// entradas

// procesamiento

// todos
todos = arrayNumeros.join("<br>");

// pares e impares
for (let contador = 0; contador < arrayNumeros.length; contador++) {
    // compruebo si es par
    if (arrayNumeros[contador] % 2 == 0) {
        pares += arrayNumeros[contador] + "<br>";
    } else {
        impares += arrayNumeros[contador] + "<br>";
    }
}

// salidas
divTodos.innerHTML += todos;
divPares.innerHTML += pares;
divImpares.innerHTML += impares;


