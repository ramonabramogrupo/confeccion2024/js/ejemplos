/**
 * Mediante el while recorrer el array y mostrar las coordenadas en consola
 * 
 */

// crear un array con las coordenadas de 20 puntos a dibujar
const coordenadas = [
    [0, 0],
    [10, 10],
    [20, 20], // coordenadas[2][1]
    [30, 30],
    [40, 40],
    [50, 50],
    [60, 60],
    [70, 70],
    [80, 80],
    [90, 90],
    [100, 100],
    [110, 110],
    [120, 120],
    [130, 130],
    [140, 140],
    [150, 150],
    [160, 160],
    [170, 170],
    [180, 180],
    [190, 190],
    [200, 200]
];

// variables
let contador = 0;

// inicializacion
contador = 0;
while (contador < coordenadas.length) {
    // instrucciones del bucle
    console.log("Punto " + contador);
    console.log("x" + coordenadas[contador][0]);
    console.log("y" + coordenadas[contador][1]);

    // incremento (control del bucle)
    contador++;
}

// con for
for (contador = 0; contador < coordenadas.length; contador++) {
    // instrucciones del bucle
    console.log("Punto " + contador);
    console.log("x" + coordenadas[contador][0]);
    console.log("y" + coordenadas[contador][1]);
}





