/**
 * Quiero que dibuje en el div salida una lista no numerada
 * con los valores de 1 a 10
 * <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li>10</li>
    </ul>
 */

// variables y constantes

const divSalida = document.querySelector('#salida');
const INICIO = 1; // principio del bucle
const FIN = 9; // fin del bucle

let acumulador = "";
let contador = 0;

// procesamiento
for (contador = INICIO, acumulador = "<ul>"; contador <= FIN; contador++) {
    acumulador += "<li>" + contador + "</li>"; //<li>0</li>
}
acumulador += "</ul>";

// cuando salgo del for, el valor de contador es 11
// el valor de acumulador es
// <ul>
//<li>0</li>
//<li>1</li>
//<li>2</li>
//<li>3</li>
//<li>4</li>
//<li>5</li>
//<li>6</li>
//<li>7</li>
//<li>8</li>
//<li>9</li>
//<li>10</li>
//</ul>

divSalida.innerHTML = acumulador;
