/**
 * Recorrer el array de numeros y mostrar en el div de salida los positivos
 * 
 */

// declarar variables y constantes
const divSalida = document.querySelector('#salida');

// array con 20 numeros 
const arrayNumeros = [
    10, -20, 30, 40, -50, -60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, -180, 190, 200
];


let contador = 0;
let positivos = "";


// procesamiento
for (contador = 0; contador < arrayNumeros.length; contador++) {
    if (arrayNumeros[contador] > 0) {
        positivos += `${arrayNumeros[contador]}<br />`;
    }
}


// salidas
divSalida.innerHTML = positivos;
