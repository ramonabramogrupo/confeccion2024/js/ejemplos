/**
 * Recorrer el String y colocamos cada caracter en la consola
 * 
 * Utilizar for y for of
 */

// variables y constantes

let frase = "hola mundo";


// for of

for (const caracter of frase) {
    console.log(caracter);
}

// for

for (let contador = 0; contador < frase.length; contador++) {
    console.log(frase[contador]);
}