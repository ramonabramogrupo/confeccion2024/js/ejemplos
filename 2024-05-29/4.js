/**
 * Mostrar el contenido del array en el div Salida
 * 
 * utilizando for (puedo utilizar join o toString)
 */

// variables y constantes
const divSalida = document.querySelector("#salida");

// crear un array con 10 nombres
const arrayNombres = ["Juan", "Maria", "Pedro", "Ana", "Luis", "Carlos", "Laura", "Jorge", "Sofia", "Marta"];

let contador = 0;
let acumulador = "";

// entradas

// procesamiento
// for (contador = 0; contador < arrayNombres.length; contador++) {

//     //divSalida.innerHTML += arrayNombres[contador] + "<br>";
//     acumulador += arrayNombres[contador] + "<br>";

// }

// si utilizo el metodo toString
// acumulador = arrayNombres.toString();

// si utilizo el metodo join
// acumulador = arrayNombres.join("<br>");

// salidas

divSalida.innerHTML = acumulador;
