/*
- pedir un numero al usuario por teclado
- si el numero es positivo el div se coloca en azul
- si es negativo el div se coloca en rojo

La anchura del div es el numero multiplicado por 10

*/

// variables y constantes

const divSalida = document.querySelector('#salida');

let numero = 0;
let color = "";
let anchura = 0;

// entradas

numero = Number(prompt("Introduce un numero", 0));

// procesamiento

if (numero > 0) {
    color = "blue";
    anchura = `${numero * 10}px`;
    //anchura = (numero * 10) + "px";
} else {
    color = "red";
    anchura = `${numero * -10}px`;
}

// salidas

divSalida.style.backgroundColor = color;
divSalida.style.width = anchura;