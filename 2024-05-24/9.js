/*
- pedir un numero al usuario entre 1 y 3
- el numero se escribe en la celda siguiendo  los pasos
    - si es 1 se coloca en la primera celda y colorea la celda de rojo
    - si es 2 se coloca en la segunda celda y colorea la celda de verde
    - si es 3 se coloca en la tercera celda y colorea la celda de azul
    - si el numero no es ni 1, ni 2, ni 3 todas las celdas de color negro
*/

// variables y constantes

let numero = 0;



// constante
// es un array de elementos 
// es un array de td
const filaSalida = document.querySelectorAll("#salida td");

// colores
const colores = ["black", "red", "green", "blue"];

// entradas

numero = Number(prompt("Introduce un numero entre 1 y 3", 0));



// salidas
if (numero >= 1 && numero <= 3) {
    filaSalida[numero - 1].style.backgroundColor = colores[numero];
} else {
    filaSalida[0].style.backgroundColor = colores[0];
    filaSalida[1].style.backgroundColor = colores[0];
    filaSalida[2].style.backgroundColor = colores[0];
}
