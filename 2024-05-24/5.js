/*
- Pedir un numero al usuario
- si el numero es positivo el div se coloca en azul
- si es negativo el div se coloca en rojo
- el numero me cambia las coordenadas del div
- left: numero
- top: numero
*/

// variables y constantes
const divSalida = document.querySelector('#salida');

let numero = 0;
let color = "";
let posX = 0;
let posY = 0;

// entradas
numero = Number(prompt("Introduce un numero", 0));

// procesamiento

if (numero > 0) {
    color = "blue";
} else {
    color = "red";
}

numero = Math.abs(numero);

posX = numero + "px";
posY = numero + "px";

// salidas
divSalida.style.backgroundColor = color;
divSalida.style.left = posX;
divSalida.style.top = posY;



