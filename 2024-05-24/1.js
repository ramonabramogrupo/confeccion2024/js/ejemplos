/**
 * leer los dos numeros que tengo en los li
 * Quiero que me indique cual es el mayor en el div
 */

const liNumero1 = document.querySelector('#item1');
const liNumero2 = document.querySelector('#item2');
const divMayor = document.querySelector('#mayor');

let numero1 = 0;
let numero2 = 0;
let mayor = 0;

numero1 = Number(liNumero1.innerHTML);
numero2 = Number(liNumero2.innerHTML);

if (numero1 > numero2) {
    mayor = numero1;
} else {
    mayor = numero2;
}

divMayor.innerHTML = mayor;

