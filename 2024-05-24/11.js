/**
 * Pedir un numero por teclado
 * ese numero debe estar entre 0 y 10
 * 
 * Si el numero es menor que 5 me devuelve "suspenso"
 * Si el numero es mayor o igual que 5 y menor que 6 aprobado
 * Si el numero es mayor o igual que 6 pero menor 7 me coloca "bien"
 * Si el numero es mayor o igual que 7 y menor que 9 me coloca "notable"
 * Si el numero es mayor o igual que 9 me coloca "sobresaliente"
 * 
 * <5 => suspenso
 * >=5 && <6 => aprobado
 * >=6 && <7 => bien
 * >=7 && <9 => notable
 * >=9 => sobresaliente
 * 
 * Quiero que realiceis el ejercicio con switch
 */

// variables y constantes

let nota = 0;
let baremo = "";

const divSalida = document.querySelector('#salida');

// entradas
nota = Number(prompt("Introduce una nota", 0));


// procesamiento

switch (true) {
    case (nota < 5):
        baremo = "suspenso";
        break;
    case (nota >= 5 && nota < 6):
        baremo = "aprobado";
        break;
    case (nota >= 6 && nota < 7):
        baremo = "bien";
        break;
    case (nota >= 7 && nota < 9):
        baremo = "notable";
        break;
    case (nota >= 9):
        baremo = "sobresaliente";
        break;
    default:
        baremo = "nota no valida";
        break;
}

// salida

divSalida.innerHTML = baremo;

