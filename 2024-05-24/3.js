/**
 * Voy a pedir al usuario 5 numeros 
 * mostrar los 5 numeros en los li
 * En el div me debe mostrar cuantos numeros son 
 * positivos
 * 
 */

const liNumero1 = document.querySelector('#numero1');
const liNumero2 = document.querySelector('#numero2');
const liNumero3 = document.querySelector('#numero3');
const liNumero4 = document.querySelector('#numero4');
const liNumero5 = document.querySelector('#numero5');

const divPositivos = document.querySelector('#positivos');

let numero1 = 0;
let numero2 = 0;
let numero3 = 0;
let numero4 = 0;
let numero5 = 0;
let positivos = ""; // acumulador


numero1 = Number(prompt('Introduce el primer numero', 5));
numero2 = Number(prompt('Introduce el segundo numero', 6));
numero3 = Number(prompt('Introduce el tercer numero', -1));
numero4 = Number(prompt('Introduce el cuarto numero', 2));
numero5 = Number(prompt('Introduce el quinto numero', 5));

// procesamiento

if (numero1 > 0) {
    positivos = positivos + " " + numero1;
    //positivos += " " + numero1;
}

if (numero2 > 0) {
    positivos = positivos + " " + numero2;
}

if (numero3 > 0) {
    positivos = positivos + " " + numero3;
}

if (numero4 > 0) {
    positivos = positivos + " " + numero4;
}

if (numero5 > 0) {
    positivos = positivos + " " + numero5;
}

// salidas
// escribir los numeros
liNumero1.innerHTML = numero1;
liNumero2.innerHTML = numero2;
liNumero3.innerHTML = numero3;
liNumero4.innerHTML = numero4;
liNumero5.innerHTML = numero5;
// escribir el resultado
divPositivos.innerHTML = positivos;