/*
- pedir un numero al usuario entre 1 y 3
- el numero se escribe en la celda siguiendo  los pasos
    - si es 1 se coloca en la primera celda y colorea la celda de rojo
    - si es 2 se coloca en la segunda celda y colorea la celda de verde
    - si es 3 se coloca en la tercera celda y colorea la celda de azul
    - si el numero no es ni 1, ni 2, ni 3 todas las celdas de color negro
*/

// variables y constantes

let numero = 0;
let color = "";
let celda = "todas";

// entradas

numero = Number(prompt("Introduce un numero entre 1 y 3", 0));

// procesamiento

if (numero == 1) {
    color = "red";
    celda = "#rojo";
} else {
    if (numero == 2) {
        color = "green";
        celda = "#verde";
    } else {
        if (numero == 3) {
            color = "blue";
            celda = "#azul";
        } else {
            color = "black";
            celda = "todas";
        }
    }
}

// salidas
if (celda == "todas") {
    document.querySelector("#rojo").style.backgroundColor = color;
    document.querySelector("#verde").style.backgroundColor = color;
    document.querySelector("#azul").style.backgroundColor = color;
} else {
    document.querySelector(celda).style.backgroundColor = color;
}

