// la imagen se muestra y se oculta cada 2 segundos

setInterval(function () {
    // el codigo que ponga aqui se ejecuta cada 2 segundos
    const imagen = document.querySelector("#mensaje");

    //  si se esta mostrando la imagen
    if (imagen.style.opacity == 1) {
        imagen.style.opacity = 0;
    } else {
        imagen.style.opacity = 1;
    }
}, 2000);