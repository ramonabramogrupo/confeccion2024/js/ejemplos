// cuando pincho en una celda se coloca en color rojo
// me lee el contenido y lo pone en el div de salida
// solo debe dejarme pulsar durante 5 segundos

// cuando pincho en una celda se coloca en color rojo
// me lee el contenido y lo pone en el div de salida

// clases

// funciones

function cambiar(event) {
    // constante que apunta al div donde quiero escribir
    const divSalida = document.querySelector('#salida');

    // leer la celda sobre la que he realizado clic
    // para conocerla puedo utilizar
    // event.target ==> celda
    // event.currentTarget ==> tabla
    // this ==> tabla

    // colocandole el color de fondo rojo
    // this.style.backgroundColor = 'red';
    event.target.style.backgroundColor = 'red';

    // colocar el contenido de la celda en el divSalida

    divSalida.textContent = event.target.textContent;
    //divSalida.textContent = this.textContent;
}

// constantes y variables

// constante que apunte a la tabla
const tabla = document.querySelector('table');

// entradas

// procesamiento

// escuchadores

// colocar el escuchador a la tabla
tabla.addEventListener('click', cambiar);

// salidas


setTimeout(function () {
    // quitar el escuchador a la tabla
    tabla.removeEventListener('click', cambiar);

}, 5000);