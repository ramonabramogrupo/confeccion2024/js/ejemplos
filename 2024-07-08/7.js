// cuando pincho en una celda se coloca en color rojo
// me lee el contenido y lo pone en el div de salida

// clases

// funciones

// constantes y variables

// un array que apunte a todas las celdas de la tabla
const celdas = document.querySelectorAll("td");

// entradas

// procesamiento

// escuchadores

// recorrer el array y colocar un escuchador a cada celda
celdas.forEach(function (celda) {
    // a cada celda le coloca el escuchador
    // celda.addEventListener(evento,funcion,objeto)
    // es objeto me permite hacer que solamente se ejecute el listener una vez
    // celda.addEventListener(evento, funcion, { once: true });
    celda.addEventListener('click', function (event) {
        // constante que apunta al div donde quiero escribir
        const divSalida = document.querySelector('#salida');

        // leer la celda sobre la que he realizado clic
        // para conocerla puedo utilizar
        // event.target
        // event.currentTarget
        // this

        // colocandole el color de fondo rojo
        // this.style.backgroundColor = 'red';
        event.target.style.backgroundColor = 'red';

        // colocar el contenido de la celda en el divSalida

        divSalida.textContent = event.target.textContent;
        //divSalida.textContent = this.textContent;
    }); // para que se ejecute una vez
});

// salidas