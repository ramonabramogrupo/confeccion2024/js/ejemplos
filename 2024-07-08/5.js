// cada segundo el div crece debe crecer 10 px de ancho

// cada segundo el div decrece debe decrecer 10 px de ancho

// constante para la velocidad
const tiempo = 100;

// constante para el tiempo de Ejecucion en ms
const tiempoEjecucion = 6000;


const temporizador = setInterval(function () {
    // constantes y variables

    // constante paso
    // para ir creciendo o decreciendo
    // modifico el paso 
    const paso = 10;

    // una constante que apunte a cada caja
    const crece = document.querySelector('#crece');
    const decrece = document.querySelector('#decrece');

    // variables donde almaceno las anchuras
    let anchoCrece = 0;
    let anchoDecrece = 0;

    // entrada

    // leer anchuras de las cajas


    // no puedo utilizar la propiedad style directamente porque los estilos
    // se aplican desde reglas CSS
    // devuelve un texto vacio
    // anchoCrece = parseInt(crece.style.width); // ''
    // anchoDecrece = parseInt(decrece.style.width);

    anchoCrece = parseInt(window.getComputedStyle(crece).width);
    anchoDecrece = parseInt(window.getComputedStyle(decrece).width);

    // metodo experimental
    // utilizar el objeto styleMap
    // anchoCrece = parseInt(crece.computedStyleMap().get('width'));
    // anchoDecrece = parseInt(decrece.computedStyleMap().get('width'));


    // procesamiento
    // modificar las anchuras
    anchoCrece += paso;
    anchoDecrece -= paso;

    // salida


    // asignarle las nuevas anchuras a las cajas
    crece.style.width = anchoCrece + 'px';
    decrece.style.width = anchoDecrece + 'px';

}, tiempo);


setTimeout(function () {
    clearInterval(temporizador);
}, tiempoEjecucion);