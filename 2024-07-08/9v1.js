// cuando pincho en una celda se coloca en color rojo
// me lee el contenido y lo pone en el div de salida
// solo debe dejarme pulsar durante 5 segundos

// tengo que adivinar el numero entre 1 y 30 antes de que se acabe el tiempo

// funciones

function parar() {
    const tabla = document.querySelector('table');
    tabla.removeEventListener('click', cambiar);
}
function cambiar(event) {

    //al colocar el escuchador a toda la tabla
    // event.target ==> celda
    // event.currentTarget ==> tabla
    // this ==> tabla

    // constante que apunta al div donde quiero escribir
    const divSalida = document.querySelector('#salida');

    // constante que me indica el valor de la celda sobre la que he realizado clic
    const valor = event.target.textContent;

    // comprobar si el numero es el que se genero aleatoriamente
    if (valor == numero) {
        // cambiar la celda sobre la que he realizado clic a color rojo
        event.target.style.backgroundColor = 'green';
        divSalida.textContent = 'Enhorabuena, has acertado';
        parar();
    } else {
        // cambiar la celda sobre la que he realizado clic a color rojo
        event.target.style.backgroundColor = 'red';
        // colocar el contenido de la celda en el divSalida
        divSalida.textContent = event.target.textContent;
    }

}

// creo un numero entero entre 1 y 30
const numero = Math.floor(Math.random() * 30) + 1;

console.log(numero);


// creo una constante que apunte a la tabla
const tabla = document.querySelector('table');

// coloco el escuchador a la tabla
tabla.addEventListener('click', cambiar);

// quitar el listener a los 5 segundos
setTimeout(function () {
    parar();
}, 5000);




