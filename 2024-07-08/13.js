// quiero que movais la pieza en horizontal (de izquierda a derecha)
// para el efecto de movivimento utilizo setInterval
// detectar cuando choca en el borde derecho del lienzo

// constante para la velocidad
// el tiempo en ms
const tiempo = 100;

// constante para el paso
const paso = 5;


// mediante el setInterval muevo la pieza hacia la derecha

// cada segundo voy a mover la pieza
const idInterval = setInterval(function () {
    // creo una constante que apunte a la pieza
    const pieza = document.querySelector('#pieza');

    // leer la posicion en horizontal de la pieza
    let posicion = parseInt(window.getComputedStyle(pieza).left);

    // ceo una constante que apunte al lienzo
    const lienzo = document.querySelector('#lienzo');

    // leo la anchura del lienzo
    const ancho = parseInt(window.getComputedStyle(lienzo).width);

    // le sumo el paso
    posicion = posicion + paso;
    if (posicion >= ancho) {
        // para el interval
        clearInterval(idInterval);
        // mensaje
        alert('perdi');
        // recargo la web
        location.reload();
    }

    // escribo la nueva posicion
    pieza.style.left = posicion + 'px';

}, tiempo);

