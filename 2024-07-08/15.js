// quiero que cada vez que pulso un cursor se desplace la pieza en la direccion
// del cursor

// la pieza se tiene que estar moviento todo el rato


/**
 * Funcion que recibe un objeto con las propiedades del juego 
 * y devuelve verdadero en caso de colision o falso si no la hay
 * @param {x,y,ancho,alto,direccion} objeto que x e y son las coordenadas de la pieza y ancho y alto son las dimensiones del lienzo y direccion es la direccion de la pieza 
 * @returns bool verdadero si hay colision o falso si no la hay
 * 
 */


function colision(objeto) {
    // variable booleana para saber si hay colision
    let salida = false;
    // comprobar que la pieza no se salga del lienzo
    switch (objeto.direccion) {
        case 'izquierda':
            if (objeto.x <= 0) {
                salida = true;
            }
            break;
        case 'derecha':
            if (objeto.x >= objeto.ancho) {
                salida = true;
            }
            break;
        case 'arriba':
            if (objeto.y <= 0) {
                salida = true;
            }
            break;
        case 'abajo':
            if (objeto.y >= objeto.alto) {
                salida = true;
            }
            break;
    }
    return salida;
}


function colisionv1(objeto) {
    let salida = false;

    salida = objeto.x <= 0 || objeto.x >= objeto.ancho || objeto.y <= 0 || objeto.y >= objeto.alto;

    return salida;

}

// constante para controlar el paso del movimiento de la pieza
const paso = 10;

// tiempo
const tiempo = 100;

// direccion
let direccion = 'derecha';


// coloco un escuchador al document que detecte la pulsacion de una tecla
document.addEventListener('keydown', function (event) {
    // compruebo la tecla pulsada
    // y cambio la variable direccion
    // utilizando un switch
    switch (event.key) {
        case 'ArrowLeft':
            direccion = 'izquierda';
            break;
        case 'ArrowRight':
            direccion = 'derecha';
            break;
        case 'ArrowUp':
            direccion = 'arriba';
            break;
        case 'ArrowDown':
            direccion = 'abajo';
            break;
    }
});


// mediante el setInterval muevo la pieza
const idInterval = setInterval(function () {


    // creo una constante que apunte a la pieza
    const pieza = document.querySelector('#pieza');

    // leer la posicion en horizontal de la pieza
    let posicionX = parseInt(window.getComputedStyle(pieza).left);

    // leer la posicion en vertical de la pieza
    let posicionY = parseInt(window.getComputedStyle(pieza).top);

    // creo una constante que apunte al lienzo
    const lienzo = document.querySelector('#lienzo');

    // leer el ancho del lienzo
    const ancho = parseInt(window.getComputedStyle(lienzo).width);

    // leer el alto del lienzo
    const alto = parseInt(window.getComputedStyle(lienzo).height);

    // compruebo en que direccion tengo que mover la pieza
    switch (direccion) {
        case 'izquierda':
            posicionX -= paso;
            break;
        case 'derecha':
            posicionX += paso;
            break;
        case 'arriba':
            posicionY -= paso;
            break;
        case 'abajo':
            posicionY += paso;
            break;
    }


    // funcion que retorna true en caso de colision
    // y false en cada contrario
    if (colisionv1({
        x: posicionX,
        y: posicionY,
        ancho: ancho,
        alto: alto,
        direccion: direccion
    })) {
        clearInterval(idInterval);
        alert('Has perdido');
        location.reload();
    }


    // escribo la nueva posicion
    pieza.style.left = posicionX + 'px';
    pieza.style.top = posicionY + 'px';

}, tiempo);

