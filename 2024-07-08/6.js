//  quiero que cada segundo me muestre una imagen 
const fotos = [
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image()
];


fotos[0].src = 'imgs/foto1.jpg';
fotos[1].src = 'imgs/foto2.jpg';
fotos[2].src = 'imgs/foto3.jpg';
fotos[3].src = 'imgs/foto4.jpg';
fotos[4].src = 'imgs/foto5.jpg';
fotos[5].src = 'imgs/foto6.jpg';
fotos[6].src = 'imgs/foto7.jpg';
fotos[7].src = 'imgs/foto8.jpg';



// variable que me indica la foto a mostrar
let numeroFoto = 0;

const temporizador = setInterval(function () {
    // constantes y variables
    const imagen = document.querySelector('img');

    // procesamiento
    // sumar uno a la variable que me indica la foto a mostrar
    numeroFoto++;

    if (numeroFoto > fotos.length - 1) {
        numeroFoto = 0;
    }

    // salida
    // coloco del array de fotos la foto que me indique la variable numeroFoto
    // en la etiqueta imagen
    imagen.src = fotos[numeroFoto].src;




}, 1000);





