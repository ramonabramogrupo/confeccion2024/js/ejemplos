// quiero que movais la pieza en vertical (de arriba a abajo)
// para el efecto de movivimento utilizo setInterval
// quiero comprobar cuando choca en la parte inferior del lienzo


// constante para la velocidad
// el tiempo en ms
const tiempo = 100;

// constante para el paso
const paso = 5;


// mediante el setInterval muevo la pieza hacia abajo

// cada segundo voy a mover la pieza
const idInterval = setInterval(function () {
    // creo una constante que apunte a la pieza
    const pieza = document.querySelector('#pieza');

    // leer la posicion en horizontal de la pieza
    let posicion = parseInt(window.getComputedStyle(pieza).top);

    // ceo una constante que apunte al lienzo
    const lienzo = document.querySelector('#lienzo');

    // leo la altura del lienzo
    const alto = parseInt(window.getComputedStyle(lienzo).height);

    // le sumo el paso
    posicion = posicion + paso;

    // compruebo si choca en la parte inferior del lienzo

    if (posicion >= alto) {
        // paro el movimiento
        clearInterval(idInterval);
        // mensaje
        alert('muerto');
        // recargo la pagina para volver a empezar
        location.reload();
    }

    // escribo la nueva posicion
    pieza.style.top = posicion + 'px';

}, tiempo);

