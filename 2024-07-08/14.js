// quiero que cada vez que pulso un cursor se desplace la pieza en la direccion
// del cursor

// constante para controlar el paso del movimiento de la pieza
const paso = 10;


// coloco un escuchador al document que detecte la pulsacion de una tecla
document.addEventListener('keydown', function (event) {
    // constante que apunta a la pieza
    const pieza = document.querySelector('#pieza');

    // leo la posicion en horizontal de la pieza
    let posicionX = parseInt(window.getComputedStyle(pieza).left);

    // leo la posicion en vertical
    let posicionY = parseInt(window.getComputedStyle(pieza).top);

    // compruebo la tecla pulsada
    // y le aplico la accion correspondiente
    // utilizando un switch
    switch (event.key) {
        case 'ArrowLeft':
            posicionX -= paso;
            break;
        case 'ArrowRight':
            posicionX += paso;
            break;
        case 'ArrowUp':
            posicionY -= paso;
            break;
        case 'ArrowDown':
            posicionY += paso;
            break;
    }

    // coloco los nuevos valores de posicion a la pieza
    pieza.style.left = posicionX + 'px';
    pieza.style.top = posicionY + 'px';

});
