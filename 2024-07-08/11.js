// quiero que movais la pieza en vertical (de arriba a abajo) 
// para el efecto de movivimento utilizo setInterval

// constante para la velocidad
// el tiempo en ms
const tiempo = 100;

// constante para el paso
const paso = 5;


// mediante el setInterval muevo la pieza hacia abajo

// cada segundo voy a mover la pieza
setInterval(function () {
    // creo una constante que apunte a la pieza
    const pieza = document.querySelector('#pieza');

    // leer la posicion en horizontal de la pieza
    let posicion = parseInt(window.getComputedStyle(pieza).top);

    // le sumo el paso
    posicion = posicion + paso;

    // escribo la nueva posicion
    pieza.style.top = posicion + 'px';

}, tiempo);

