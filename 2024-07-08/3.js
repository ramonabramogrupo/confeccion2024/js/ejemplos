// el mensaje emergente saldra a los 3 segundos y se quitara a los 6 segundos

// muestra el mensaje
setTimeout(function () {
    const mensaje = document.querySelector('#mensaje');
    mensaje.style.opacity = 1;
    // setTimeout(function () {
    //     mensaje.style.opacity = 0;
    // }, 3000);
}, 3000);

// quita el mensaje
setTimeout(function () {
    const mensaje = document.querySelector('#mensaje');
    mensaje.style.opacity = 0;
}, 6000);



// setTimeout(() => {
//     // instruccion se ejecuta a los 2 segundos
//     setTimeout(() => {
//         // instruccion se ejecuta a los 5 segundos
//     }, 3000);
// }, 2000);

