# Objetivos

Los temporizadores (timers) son funciones especiales que permiten ejecutar una función que realice ciertas tareas después de un determinado tiempo. 

Por un lado, los temporizadores 
- timeout ejecutan cuando pasa un cierto tiempo, 
- mientras que los temporizadores interval se ejecutan constantemente cada cierto tiempo.

# Timeout

setTimeout y clearTimeout son funciones en JavaScript utilizadas para programar la ejecución de una función después de un período específico de tiempo y para cancelar esa ejecución, respectivamente.

## setTimeout

setTimeout programa la ejecución de una función o un fragmento de código después de un tiempo específico. 

La sintaxis es la siguiente:

```js
let timeoutID = setTimeout(function, retardo, [arg1, arg2, ...]);

```

- function: La función que se va a ejecutar.
- retardo: El tiempo en milisegundos que el intérprete debe esperar antes de ejecutar la función.
- arg1, arg2, ...: (Opcional) Argumentos adicionales que se pasarán a la función cuando sea ejecutada.

Un ejemplo de la funcion:

```js
function sayHello() {
  console.log('Hello, World!');
}

const timeoutID = setTimeout(sayHello, 2000); // Imprime "Hello, World!" después de 2 segundos

```

## clearTimeout

clearTimeout cancela una acción programada previamente con setTimeout. 

La sintaxis es la siguiente:

```js
clearTimeout(timeoutID); // timeoutID es lo que devuelve setTimeout

```


Veamos un ejemplo

```js
function sayHello() {
  console.log('Hello, World!');
}

const timeoutID = setTimeout(sayHello, 5000); // Programa "Hello, World!" para 5 segundos en el futuro

// Cancela el temporizador antes de que se ejecute
clearTimeout(timeoutID); // "Hello, World!" no se imprimirá

```

### Ejemplo completo

Este ejemplo muestra cómo setTimeout puede ser cancelado usando clearTimeout basado en una acción del usuario.

En este ejemplo, al hacer clic en "Iniciar Temporizador", se programa un mensaje para aparecer después de 4 segundos. 

Si el usuario hace clic en "Cancelar Temporizador" antes de que transcurran los 4 segundos, el temporizador se cancela y no se muestra el mensaje programado.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>setTimeout y clearTimeout</title>
</head>
<body>
  <button id="startButton">Iniciar Temporizador</button>
  <button id="cancelButton">Cancelar Temporizador</button>
  <p id="message"></p>

  <script>
    let timeoutID;

    document.getElementById('startButton').addEventListener('click', function() {
      timeoutID = setTimeout(function() {
        document.getElementById('message').textContent = '¡Temporizador completado!';
      }, 4000); // Programa un mensaje para 4 segundos en el futuro
    });

    document.getElementById('cancelButton').addEventListener('click', function() {
      clearTimeout(timeoutID); // Cancela el temporizador
      document.getElementById('message').textContent = 'Temporizador cancelado.';
    });
  </script>
</body>
</html>

```

# Interval

setInterval y clearInterval son funciones en JavaScript que se utilizan para manejar la ejecución repetitiva de código. 

setInterval permite ejecutar una función repetidamente a intervalos de tiempo específicos, mientras que clearInterval se usa para detener la ejecución repetitiva iniciada por setInterval.

## setInterval

setInterval es una función que llama repetidamente a una función o evalúa una expresión con un retardo fijo entre cada llamada.

La sintaxis es:

```js
let intervalID = setInterval(función, retardo, argumento1, argumento2, ...);

```

- función: La función que se quiere ejecutar repetidamente.
- retardo: El tiempo (en milisegundos) que debe esperar entre cada llamada a la función.
- argumento1, argumento2, ...: Argumentos adicionales que se pasarán a la función.

Un ejemplo de funcionamiento:

```js
function saludar() {
    console.log('Hola, cada 2 segundos!');
}

let intervalo = setInterval(saludar, 2000); // En este ejemplo, la función saludar se ejecuta cada 2000 milisegundos (2 segundos).
```

## clearInterval

clearInterval se usa para detener una ejecución repetitiva iniciada por setInterval.

La sintaxis:

```js
clearInterval(intervalID);
```

- IntervalID:  es el identificador del intervalo que se desea detener. Este identificador es el valor devuelto por setInterval.

Veamos un ejemplo:

```js
function saludar() {
    console.log('Hola, cada 2 segundos!');
}

let intervalo = setInterval(saludar, 2000);

// Detener el intervalo después de 10 segundos
setTimeout(() => {
    clearInterval(intervalo);
    console.log('Intervalo detenido');
}, 10000);
```


## Ejemplo completo

En este ejemplo al hacer clic en el botón "Iniciar", se inicia un intervalo que incrementa y muestra un contador cada segundo.

Al hacer clic en el botón "Detener", se detiene el intervalo.

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejemplo de setInterval y clearInterval</title>
</head>
<body>
    <button id="iniciar">Iniciar</button>
    <button id="detener">Detener</button>

    <script>
        let contador = 0;
        let intervalo;

        function contar() {
            contador++;
            console.log('Contador:', contador);
        }

        document.getElementById('iniciar').addEventListener('click', () => {
            intervalo = setInterval(contar, 1000);
        });

        document.getElementById('detener').addEventListener('click', () => {
            clearInterval(intervalo);
            console.log('Intervalo detenido');
        });
    </script>
</body>
</html>
```



