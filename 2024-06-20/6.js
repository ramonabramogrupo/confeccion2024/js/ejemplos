/*
Crea una lista desordenada con tres elementos: "Elemento 1", "Elemento 2", "Elemento 3" y añádela al div con id "salida".

Al hacer clic en un elemento quiero que se elimine de la lista
Para ello utilizo el metodo .remove()
*/

// apunto al elemento donde voy a anadir contenido
const contenedor = document.querySelector('#salida');

// creo la lista

const lista = document.createElement('ul');

// sin bucle
// const li1 = document.createElement('li');
// li1.textContent = 'Elemento 1';
// li1.addEventListener('click', function (event) {
//     li1.remove();
// });

// lista.appendChild(li1);
// const li2 = document.createElement('li');
// li2.textContent = 'Elemento 2';
// li2.addEventListener('click', function (event) {
//     li2.remove();
// });
// lista.appendChild(li2);
// const li3 = document.createElement('li');
// li3.textContent = 'Elemento 3';
// li3.addEventListener('click', function (event) {
//     li3.remove();
// });
// lista.appendChild(li3);

// con bucle
for (let c = 0; c < 3; c++) {
    const li = document.createElement('li');
    li.textContent = 'Elemento ' + (c + 1);

    // anadir evento para borrar el elemento
    li.addEventListener('click', function (event) {
        // this.remove();
        li.remove();
        // event.target.remove();
    });

    lista.appendChild(li);
}

// añadir el ul al contenedor
contenedor.appendChild(lista);