// quiero apuntar al p de contenedor

const parrafo = document.querySelector('#contenedor>p');

// apuntar al div con id uno

const divUno = document.querySelector('#uno');

// crear la siguiente estructura
/*
<div id="dos">
<p>Santander</p>  
</div>
*/

// crear un div
const divDos = document.createElement('div');

// crear un parrafo
const parrafoDos = document.createElement('p');

// colocar el parrafoDos dentro del divDos
divDos.appendChild(parrafoDos);

// colocar el id dos al divDos
divDos.id = 'dos';

// al parrafoDos meterle en el texto
parrafoDos.textContent = 'Santander';


// ¿parrafoDos?
// es un nodo de tipo elemento
// no esta en el DOM

// ¿parrafo?
// es un nodo de tipo elemento
// esta en el DOM

// quiero mover el parrafo al divUno
divUno.appendChild(parrafo);


// quiero añadir el divDos al body
document.body.appendChild(divDos);
