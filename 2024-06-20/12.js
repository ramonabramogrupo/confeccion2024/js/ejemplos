
/**
 * Funcion capaz de crear una etiqueta de forma dinamica
 * @param {string} nombre de la etiqueta
 * @param {array} estilos un array con las clases a utilizar
 * @param {string} contenido  el contenido de la etiqueta
 * @returns element con la estructura de la etiqueta
 */
function etiqueta(nombre, estilos, contenido) {
  // creando la etiqueta
  const salida = document.createElement(nombre);
  // añadiendo los estilos
  salida.classList.add(...estilos);

  // añadiendo el contenido
  salida.textContent = contenido;

  // devolver la etiqueta
  return salida;

}


const div = etiqueta("div", ["estilo1", "estilo2"], "");


const p = etiqueta("p", ["estilo1", "estilo2"], "Hola clase");


div.appendChild(p);
// <div class="estilo1 estilo2">Hola mundo</div>

document.body.appendChild(div)





