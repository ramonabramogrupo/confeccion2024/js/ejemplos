// EJERCICIO BASADA EN EL 9_funcion

/*
Crear una funcion que reciba un array bidimensional de datos
y devuelva una estructura dinamica de divs generada a partir del

// un array bidimensional con libros
const datos = [
    ["El principito", "Antoine de Saint-Exupéry"],
    ["El gran Gatsby", "F. Scott Fitzgerald"],
    ["Cien años de soledad", "Gabriel Garcia Marquez"],
    ["El amor en tiempos de colera", "Gabriel Garcia Marquez"]
];

Crear esta estructura
<div>
    <h2>El principito</h2>
    <p>Antoine de Saint-Exupéry</p>
</div>
<div>
    <h2>El gran Gatsby</h2>
    <p>F. Scott Fitzgerald</p>
</div>

La estructura crearla dentro de un documentFragment y este es el que teneis que devolver
*/


function listView(datos) {
    const fragmento = document.createDocumentFragment();

    datos.forEach(function (libro) {
        const div = document.createElement("div");
        const h2 = document.createElement("h2");
        const p = document.createElement("p");
        h2.textContent = libro[0];
        p.textContent = libro[1];
        div.appendChild(h2);
        div.appendChild(p);
        fragmento.appendChild(div);
    });
    return fragmento;
}

const datos = [
    ["El principito", "Antoine de Saint-Exupéry"],
    ["El gran Gatsby", "F. Scott Fitzgerald"],
    ["Cien años de soledad", "Gabriel Garcia Marquez"],
    ["El amor en tiempos de colera", "Gabriel Garcia Marquez"]
];

const divSalida = document.querySelector("#salida");
divSalida.appendChild(listView(datos));
