/**
 *quiero presentar los siguientes nombres mediante cards de bootstrap

 <div class="col-md-4 mt-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Nombre </h5>
            <p class="card-text">Valor del nombre</p>
        </div>
    </div>
</div>


 */


let nombres = ["Juan", "Maria", "Pedro", "Ana", "Luis", "Carlos", "Laura", "Jorge", "Sofia", "Marta"];

// opcion 1
// Realizarlo sin crear nodos solamente con innerHTML
// nombres.forEach(function (nombre) {
//     const divSalida = document.querySelector("#salida");
//     let salida = "";
//     salida = `
//      <div class="col-md-4 mt-3">
//     <div class="card">
//         <div class="card-body">
//             <h5 class="card-title">Nombre </h5>
//             <p class="card-text">${nombre}</p>
//         </div>
//     </div>
// </div>`;

//     divSalida.innerHTML += salida;

// })

// opcion 2
// Realizado con
// CreateElement
// appendChild
// textContent
// classList

// recorrer nombres



nombres.forEach(function (nombre) {
    // apunto al div donde quiero dibujar la card
    const divSalida = document.querySelector("#salida");

    // creo la estructura a dibujar
    const div1 = document.createElement("div");
    const div2 = document.createElement("div");
    const div3 = document.createElement("div");
    const h5 = document.createElement("h5");
    const p = document.createElement("p");

    // puedo configurar los elementos
    // div1.className = "col-md-4 mt-3";
    div1.classList.add("col-md-4", "mt-3");

    // div2.className = "card";
    div2.classList.add("card");

    // div3.className = "card-body";
    div3.classList.add("card-body");

    // h5.className = "card-title"; 
    h5.classList.add("card-title");

    // p.className = "card-text";
    p.classList.add("card-text");

    // coloco el contenido a las etiquetas

    h5.textContent = "Nombre ";
    p.textContent = nombre;

    // crear la estructura de etiquetas
    div1.appendChild(div2);
    div2.appendChild(div3);
    div3.appendChild(h5);
    div3.appendChild(p);

    divSalida.appendChild(div1);
});

