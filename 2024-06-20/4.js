/*
 * cada vez que realizamos movemos el raton en la pantalla
 * hay que crear un div cuadrado  si tenemos el boton izquierdo del raton pulsado
 */

// colocar escuchador al document

document.addEventListener('mousemove', function (event) {
    // crear una variable para leer el boton del raton pulsado
    let boton = event.buttons;
    // si el boton pulsado
    // 1 es el izquierdo
    // 2 es el central
    // 4 es el derecho
    // 0 ninguno
    if (boton == 1) {
        // crear unas variables con las coordenadas desde el objeto event
        let x = event.clientX;
        let y = event.clientY;

        // crear un div
        const divPunto = document.createElement('div');

        // colocarle la clase cuadrado
        divPunto.classList.add('cuadrado');

        // colocarle las coordenadas donde habeis pulsado
        divPunto.style.left = x + 'px';
        divPunto.style.top = y + 'px';

        // añadir el div al body
        document.body.appendChild(divPunto);
    }

});