/**
 * quiero realizar una galeria de imagenes con js
 * Para realizarlo creo una funcion llamada mostrarFotos
 * debe crear una estructura dinamica de divs y devolverla
 * 
Lo que recibe es esto:
const fotos = [
    ['foto1.jpg', 'Foto 1'],
    ['foto2.jpg', 'Foto 2'],
];
El fragmento que tiene que crear es este 

  <div class="col-md-4">
    <div class="card bg-image" style="background-image: url('imgs/img/foto1.jpg');">
        <div class="card-body d-flex align-items-end">
            <h5 class="card-title text-center w-100">Foto 1</h5>
        </div>
    </div>
</div>
  <div class="col-md-4">
    <div class="card bg-image" style="background-image: url('imgs/img/foto2.jpg');">
        <div class="card-body d-flex align-items-end">
            <h5 class="card-title text-center w-100">Foto 2</h5>
        </div>
    </div>
</div>
 
 */

function mostrarFotos(fotos) {
    const fragmento = document.createDocumentFragment();

    fotos.forEach(function (registro) {
        // creo un array para los divs
        const divs = [];
        // creo los divs
        divs[0] = document.createElement('div');
        divs[1] = document.createElement('div');
        divs[2] = document.createElement('div');
        // creo el h5
        h5 = document.createElement('h5');
        // coloco las clases a los elementos creados
        divs[0].classList.add('col-md-4');
        divs[1].classList.add('card', 'bg-image');
        divs[2].classList.add('card-body', 'd-flex', 'align-items-end');
        h5.classList.add('card-title', 'text-center', 'w-100');

        // crear la estructura
        divs[0].appendChild(divs[1]);
        divs[1].appendChild(divs[2]);
        divs[2].appendChild(h5);
        h5.textContent = registro[1];
        divs[1].style.backgroundImage = 'url(imgs/img/' + registro[0] + ')';
        fragmento.appendChild(divs[0]);
    });

    return fragmento;
}

const fotos = [
    ['foto1.jpg', 'Foto 1'],
    ['foto2.jpg', 'Foto 2'],
    ['foto3.jpg', 'Foto 3'],
    ['foto4.jpg', 'Foto 4'],
    ['foto5.jpg', 'Foto 5'],
    ['foto6.jpg', 'Foto 6'],
    ['foto7.jpg', 'Foto 7'],
    ['foto8.jpg', 'Foto 8'],
]

const divSalida = document.querySelector('#galeria');
divSalida.appendChild(mostrarFotos(fotos));