/*
quiero que cada vez que pulsamos sobre el boton añadir debe crear un nuevo enlace en el menu
de navegación
utilizar funciones anonimas
utilizar createElement
utilizar appendChild
textContent o innerHTML
*/

// crear constantes y variables
const boton = document.querySelector('button');

// escuchadores de eventos
boton.addEventListener('click', function (event) {
    // constantes

    // accedo al ul para poder crear li dentro
    const menu = document.querySelector('ul');
    // constantes que me apunten a los input
    const inputEnlace = document.querySelector('#enlace');
    const inputTexto = document.querySelector('#titulo');

    // realizado utilizando innerHTML
    // menu.innerHTML += `
    //                     <li class="nav-item">
    //                     <a class="nav-link" href="${inputEnlace.value}">${inputTexto.value}</a>
    //                 </li>
    // `;

    // creando elementos
    const li = document.createElement('li');
    const a = document.createElement('a');

    // añadir las clases a los elementos
    li.classList.add('nav-item');
    a.classList.add('nav-link');

    // añadiendo los atributos a los elementos
    a.href = inputEnlace.value;

    //añadiendo el texto al enlace
    a.textContent = inputTexto.value;

    // meter el a dentro del li creado
    li.appendChild(a);

    // colocamos el elemento creado en el menu
    menu.appendChild(li);
});






