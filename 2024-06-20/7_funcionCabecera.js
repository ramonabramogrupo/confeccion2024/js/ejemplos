/*
Funcion que recibe un array bidimensional de datos 
y devuelve un elemento con la cabecera de la tabla
Mostrar los datos de los alumnos en una tabla 
Añade la tabla al div con id salida.

const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es']
    ['pedro', 'garcia', 'pedro@alpe.es']
];

<table>
<thead>
    <tr>
        <td>nombre</td>
        <td>apellidos</td>
        <td>email</td>
    </tr>
</thead>
<tbody>
    <tr>
        <td>jose</td>
        <td>perez</td>
        <td>jose@alpe.es</td>
    </tr>
    <tr>
        <td>maria</td>
        <td>lopez</td>
        <td>maria@alpe.es</td>
    </tr>
    <tr>
        <td>pedro</td>
        <td>garcia</td>
        <td>pedro@alpe.es</td>
    </tr>
</tbody>
</table>

*/

/*
ayuda de un operador ternario

if(condicion){
    a=1;
}else{
    a=2;
}

a = (condicion) ? 1 : 2;
*/


function crearTablaCabeceraDatos(datos) {
    const tabla = document.createElement('table');
    const cabecera1 = document.createElement('thead');
    const cabecera2 = document.createElement('tbody');

    datos.forEach(function (registro, indice) {

        const fila = document.createElement('tr');

        // colocando las celdas en la fila
        registro.forEach(function (dato) {
            const celda = document.createElement('td');
            celda.textContent = dato;
            fila.appendChild(celda);
        });


        // comprobando si la fila es la 1 o no
        if (indice == 0) {
            // si es la fila1 la coloco en el thead
            cabecera1.appendChild(fila);
        } else {
            // si es la fila2 la coloco en el tbody
            cabecera2.appendChild(fila);
        }

    });
    tabla.appendChild(cabecera1);
    tabla.appendChild(cabecera2);
    return tabla;

}


// function crearTablaCabeceraDatos(datos) {
//     const tabla = document.createElement('table');
//     const cabecera1 = document.createElement('thead');
//     const cabecera2 = document.createElement('tbody');

//     // esto es para la primera fila
//     datos[0].forEach(function (dato) {
//         const celda = document.createElement('td');
//         celda.textContent = dato;
//         cabecera1.appendChild(celda);
//     });

//     // esto es para el resto de filas
//     for (let c = 1; c < datos.length; c++) {
//         const fila = document.createElement('tr');
//         datos[c].forEach(function (dato) {
//             const celda = document.createElement('td');
//             celda.textContent = dato;
//             fila.appendChild(celda);
//         });
//         cabecera2.appendChild(fila);
//     }

//     tabla.appendChild(cabecera1);
//     tabla.appendChild(cabecera2);
//     return tabla;

// }





const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es'],
    ['pedro', 'garcia', 'pedro@alpe.es']
];

const contenedor = document.querySelector('#salida');
//const tabla = crearTablaDatos(alumnos);
const tabla = crearTablaCabeceraDatos(alumnos);
contenedor.appendChild(tabla);

