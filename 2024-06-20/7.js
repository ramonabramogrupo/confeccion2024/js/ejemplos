/*
Crea una tabla con dos filas y tres columnas. 
Cada celda debe contener el texto "Fila X, Columna Y" donde X es el número de la fila e Y el número de la columna. 
Añade la tabla al div con id salida.

    <table>
        <tr>
            <td>Fila 1, columna 1</td>
            <td>Fila 1, columna 2</td>
            <td>Fila 1, columna 3</td>
        </tr>
        <tr>
            <td>Fila 2, columna 1</td>
            <td>Fila 2, columna 2</td>
            <td>Fila 2, columna 3</td>
        </tr>
    </table>
*/

function crearTabla(nFilas, nColumnas) {
    const tabla = document.createElement('table');
    // filas
    for (let nfila = 1; nfila <= nFilas; nfila++) {
        const fila = document.createElement('tr');
        // columnas
        for (let columna = 1; columna <= nColumnas; columna++) {
            const celda = document.createElement('td');
            celda.textContent = `Fila ${nfila}, Columna ${columna}`;
            fila.appendChild(celda);
        }
        tabla.appendChild(fila);
    }
    return tabla;
}

function dibujarTabla(nFilas, nColumnas, objeto) {
    const tabla = document.createElement('table');
    // filas
    for (let nfila = 1; nfila <= nFilas; nfila++) {
        const fila = document.createElement('tr');
        // columnas
        for (let columna = 1; columna <= nColumnas; columna++) {
            const celda = document.createElement('td');
            celda.textContent = `Fila ${nfila}, Columna ${columna}`;
            fila.appendChild(celda);
        }
        tabla.appendChild(fila);
    }
    objeto.appendChild(tabla);
}

// constante que apunta al div donde quiero dibujar la tabla
const div = document.querySelector('#salida');

// utilizando la funcion crearTabla
// constante que sea la tabla
const tabla = crearTabla(2, 3);

// dibujo la tabla
div.appendChild(tabla);

// utilizando la funcion dibujarTabla
dibujarTabla(2, 3, div);






