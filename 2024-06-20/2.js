/*
 * cada vez que realizamos un click en la pantalla
 * hay que crear un div cuadrado en el punto pulsado
 */

// colocar escuchador al document

document.addEventListener('click', function (event) {
    // crear unas variables con las coordenadas desde el objeto event
    let x = event.clientX;
    let y = event.clientY;

    // crear un div
    const divPunto = document.createElement('div');

    // colocarle la clase cuadrado
    divPunto.classList.add('cuadrado');

    // colocarle las coordenadas donde habeis pulsado
    divPunto.style.left = x + 'px';
    divPunto.style.top = y + 'px';

    // añadir el div al body
    document.body.appendChild(divPunto);

});
