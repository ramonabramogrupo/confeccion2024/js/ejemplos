/*
Crear una funcion que reciba un array bidimensional de datos
y devuelva una estructura dinamica de divs generada a partir del array de datos

const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es']
    ['pedro', 'garcia', 'pedro@alpe.es']
];

<table>
    <tr>
        <td>nombre</td>
        <td>apellidos</td>
        <td>email</td>
    </tr>
    <tr>
        <td>jose</td>
        <td>perez</td>
        <td>jose@alpe.es</td>
    </tr>
    <tr>
        <td>maria</td>
        <td>lopez</td>
        <td>maria@alpe.es</td>
    </tr>
    <tr>
        <td>pedro</td>
        <td>garcia</td>
        <td>pedro@alpe.es</td>
    </tr>
</table>

*/

/**
 * Funcion que crea una tabla de forma dinamica a partir de un array bidimensional de datos
 * @param {*} datos array bidimensional de datos
 * @returns element con la estructura de la tabla creada
 */
function crearTablaDatos(datos) {
    const tabla = document.createElement('table');
    datos.forEach(function (registro) {
        const fila = document.createElement('tr');
        registro.forEach(function (dato) {
            const celda = document.createElement('td');
            celda.textContent = dato;
            fila.appendChild(celda);
        });
        tabla.appendChild(fila);
    });
    return tabla;
}


const alumnos = [
    ['nombre', 'apellidos', 'email'],
    ['jose', 'perez', 'jose@alpe.es'],
    ['maria', 'lopez', 'maria@alpe.es'],
    ['pedro', 'garcia', 'pedro@alpe.es']
];

const contenedor = document.querySelector('#salida');
const tabla = crearTablaDatos(alumnos);

contenedor.appendChild(tabla);

