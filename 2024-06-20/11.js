/*
Crear esta estructura y dibujarla en el body

<div id="contenedor">
  <p>Fijo</p>
</div>
*/

// creo elementos
const div = document.createElement('div'); // <div><div>

const p = document.createElement('p'); // <p><p>

// al div le coloco una id
div.id = 'contenedor';

// al p le coloco un texto
p.textContent = 'Fijo';

// colocar el p dentro del div
div.appendChild(p);

// añadir el div como hijo al body
document.body.appendChild(div);

