/**
creo unos datos en formato de array bidimensional
[
    ["Ramon", "Profesor", "Santander"],
    ["Maria", "Estudiante", "Santander"],
]
y crea un elemento card para cada uno de los datos
<div class="col-md-4 mt-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ramon</h5>
            <p class="card-text">Profesor</p>
            <p class="card-text">Santander</p>
        </div>
    </div>
</div> 
<div class="col-md-4 mt-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Maria</h5>
            <p class="card-text">Estudiante</p>
            <p class="card-text">Santander</p>
        </div>
    </div>
</div> 

Utilizando createElement
Utilizando appendChild
 */


let datos = [
    ["Ramon", "Profesor", "Santander"],
    ["Maria", "Estudiante", "Santander"],
    ["Pedro", "Profesor", "Santander"],
    ["Ana", "Estudiante", "Santander"],
];


// lo realizo sin funciones
// realirlo utilizando creacion de nodos

datos.forEach(function (registro) {
    // moviendo por registros

    // apunto al div donde quiero dibujar
    const divSalida = document.querySelector("#salida");

    // creo la estructura a dibujar
    const div1 = document.createElement("div");
    const div2 = document.createElement("div");
    const div3 = document.createElement("div");


    // colocar las clases

    div1.classList.add("col-md-4", "mt-3");
    div2.classList.add("card");
    div3.classList.add("card-body");

    registro.forEach(function (dato, indice) {
        const contenedor = (indice == 0) ? document.createElement("h5") : document.createElement("p");

        let clase = (indice == 0) ? "card-title" : "card-text";

        contenedor.classList.add(clase);
        contenedor.textContent = dato;
        div3.appendChild(contenedor);
    });


    // generar la estructura

    div1.appendChild(div2);
    div2.appendChild(div3);
    divSalida.appendChild(div1);
});


