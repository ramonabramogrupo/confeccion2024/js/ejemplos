/*
funcion llamada dibujar que crea la siguiente estructura de html
con creacion de nodos

<div id="miDiv">
    <p>Hola Mundo</p>
    <p>Este es un párrafo.</p>
    <a href="http://www.google.es">Ir a google</a>
</div>
dentro del div salida
createElement
appendChild
textContent
innerHTML
querySelector
querySelectorAll
*/

function dibujar() {
    // caja donde voy a dibujar la estructura
    const divSalida = document.querySelector('#salida');

    // creo los elementos que necesito
    const miDiv = document.createElement('div');
    const p1 = document.createElement('p');
    const p2 = document.createElement('p');
    const a = document.createElement('a');

    // modificando los elementos creados
    miDiv.id = 'miDiv';
    p1.textContent = 'Hola Mundo';
    p2.textContent = 'Este es un párrafo.';
    a.href = 'http://www.google.es';
    a.textContent = 'Ir a google';

    // colocar todas las etiquetas dentro de miDiv
    miDiv.appendChild(p1);
    miDiv.appendChild(p2);
    miDiv.appendChild(a);

    // colocar en el div de salida miDiv
    divSalida.appendChild(miDiv);


}

dibujar();
