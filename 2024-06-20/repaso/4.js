/*
funcion que me permita dibujar dos eventos en el calendario
de forma dinamica con creacion de nodos
                    <div class="event">
                        <h2>Evento 2</h2>
                        <p>Descripción del Evento</p>
                    </div>
                    createElement
appendChild
textContent
innerHTML
querySelector
querySelectorAll
cloneNode
*/

function eventos() {
    // apuntando a los divs donde quiero dibujar
    const dias = document.querySelectorAll('.day');

    // crear los elementos a dibujar
    const divEvent = document.createElement('div');
    const h2 = document.createElement('h2');
    const p = document.createElement('p');

    // coloco las etiquetas dentro del divEvent
    divEvent.appendChild(h2);
    divEvent.appendChild(p);

    // colocar los atributos y contenidos a los elementos

    // divEvent.className = 'event'; // funciona pero regular
    divEvent.classList.add('event'); // funcion y bien
    // divEvent.setAttribute('class', 'event'); // funcion pero muy mal

    h2.textContent = 'clase de JS';
    p.textContent = 'trabajo con nodos';

    // añadir el evento el lunes
    dias[0].appendChild(divEvent);

    dias[1].appendChild(divEvent.cloneNode(true));
    dias[1].querySelector('h2').textContent = 'clase de Html';
    dias[1].querySelector('p').textContent = 'trabajo con enlaces';

}

function evento(dia, titulo, contenido) {
    // apuntando a los divs donde quiero dibujar
    const dias = document.querySelectorAll('.day');

    // crear los elementos a dibujar
    const divEvent = document.createElement('div');
    const h2 = document.createElement('h2');
    const p = document.createElement('p');

    // coloco las etiquetas dentro del divEvent
    divEvent.appendChild(h2);
    divEvent.appendChild(p);

    // colocar los atributos y contenidos a los elementos

    // divEvent.className = 'event'; // funciona pero regular
    divEvent.classList.add('event'); // funcion y bien
    // divEvent.setAttribute('class', 'event'); // funcion pero muy mal

    h2.textContent = titulo;
    p.textContent = contenido;

    // añadir el evento el lunes
    dias[dia].appendChild(divEvent);
}

// eventos();
evento(0, 'clase de JS', 'trabajo con nodos');
evento(1, 'clase de Html', 'trabajo con enlaces');
evento(4, 'clase de Bootstrap', 'trabajo con tablas');
evento(0, 'clase de CSS', 'trabajo con estilos');