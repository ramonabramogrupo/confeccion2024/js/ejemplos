/*
Crear un párrafo con texto y añadirlo a un div con id contenedor
Para crear los elementos utilizar createElement
Para insertarlos utilizar appendChild
para añadir texto al parrafo utilizar textContent
<p>
  Texto
</p>
*/

// apunto al elemento donde voy a añadir contenido
const contenedor = document.querySelector('#contenedor');

// creo el parrafo
const parrafo = document.createElement('p');
parrafo.textContent = 'Texto';

// inserto el parrafo dentro del div
contenedor.appendChild(parrafo);

