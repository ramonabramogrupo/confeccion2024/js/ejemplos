// EJERCICIO BASADA EN EL 9

/*
Crear una estructura dinamica de divs generada a partir de un array
bidimensional de datos

// un array bidimensional con libros
const datos = [
    ["El principito", "Antoine de Saint-Exupéry"],
    ["El gran Gatsby", "F. Scott Fitzgerald"],
    ["Cien años de soledad", "Gabriel Garcia Marquez"],
    ["El amor en tiempos de colera", "Gabriel Garcia Marquez"]
];

Crear esta estructura
<div>
    <h2>El principito</h2>
    <p>Antoine de Saint-Exupéry</p>
</div>
<div>
    <h2>El gran Gatsby</h2>
    <p>F. Scott Fitzgerald</p>
</div>

Realizarlo directamente sin funciones

Dibujamos la estructura en el div id Salida
*/


const datos = [
    ["El principito", "Antoine de Saint-Exupéry"],
    ["El gran Gatsby", "F. Scott Fitzgerald"],
    ["Cien años de soledad", "Gabriel Garcia Marquez"],
    ["El amor en tiempos de colera", "Gabriel Garcia Marquez"]
];

// objeto donde dibujo
const divSalida = document.querySelector("#salida");

datos.forEach(function (libro) {
    const div = document.createElement("div");
    const h2 = document.createElement("h2");
    const p = document.createElement("p");
    h2.textContent = libro[0];
    p.textContent = libro[1];
    div.appendChild(h2);
    div.appendChild(p);
    divSalida.appendChild(div);
});