/*
 * cada vez que realizamos un click en la pantalla
 * hay que crear un div cuadrado en el punto pulsado
 * y que cada div tenga un color aleatorio
 */

function obtenerColor() {
    let color = "#";

    // crear un bucle que genere 6 caracteres hexadecimales
    for (let c = 0; c < 6; c++) {
        let numero = 0;
        // crear un numero del 0 al 15
        numero = Math.floor(Math.random() * 16);

        // pasar ese numero a base hexadecimal
        numero = numero.toString(16);

        // añadir el numero a color
        color = color + numero;
    }

    return color;
}

// colocar escuchador al document

document.addEventListener('click', function (event) {
    // crear unas variables con las coordenadas desde el objeto event
    let x = event.clientX;
    let y = event.clientY;

    // crear un div
    const divPunto = document.createElement('div');

    // colocarle la clase cuadrado
    divPunto.classList.add('cuadrado');

    // colocarle las coordenadas donde habeis pulsado
    divPunto.style.left = x + 'px';
    divPunto.style.top = y + 'px';

    // cambio el color
    divPunto.style.backgroundColor = obtenerColor();

    // añadir el div al body
    document.body.appendChild(divPunto);

});