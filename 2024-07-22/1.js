// esta funcion esta mejor realizada porque comprueba
// que el argumento 
// no es null o vacio
function palabras(argumento = null) {
    let array = [];
    // compruebo si es null
    if (argumento == null) {
        array = [];
    }
    // compruebo si el argumento es vacio
    else if (argumento == '') {
        array = [];
    }
    // caso normal
    else {
        array = argumento.split(" ");
    }

    return array.length;

}

// La funcion es correcta pero le faltan cosas
// no realiza comprobaciones de lo que recibe
function palabrasFacil(argumento) {
    return argumento.split(" ").length;
}

console.log(palabras("hola como estas"));
console.log(palabras(""));
console.log(palabras(null));
console.log(palabras());


function extraerSubcadena(string, inicio, final) {
    return string.substring(inicio, inicio + final);
}

console.log(extraerSubcadena("hola como estas", 3, 2));


function quitarRepetidos(...datos) {
    // creo un array vacio
    const solucion = [];
    // recorro el array original pasado como argumento
    datos.forEach(function (dato) {
        // si el dato no esta en el array de datos sin repetidos lo agrego
        if (!solucion.includes(dato)) {
            solucion.push(dato);
        }
    });
    return solucion.toString();
}

function datosPositivos(datos) {
    return datos.filter(function (dato) {
        return dato > 0;
    });
}

console.log(datosPositivos([1, -1, 2, 3, -2]));