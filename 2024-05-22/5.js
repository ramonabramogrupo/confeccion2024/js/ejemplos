/**
 * Leer los dos primeros li
 * En los 4 li de la segunda lista colocar
 * suma
 * resta
 * multiplicacion
 * division
 *
 */

// variables y constantes
// constantes para apuntar al DOM
const liNumero1 = document.querySelector('#numero1');
const liNumero2 = document.querySelector('#numero2');
const liSuma = document.querySelector('#suma');
const liResta = document.querySelector('#resta');
const liMultiplicacion = document.querySelector('#multiplicacion');
const liDivision = document.querySelector('#division');

// variables
let numero1 = 0;
let numero2 = 0;
let suma = 0;
let resta = 0;
let multiplicacion = 0;
let division = 0;

// entradas
numero1 = Number(liNumero1.innerHTML);
numero2 = Number(liNumero2.innerHTML);

// procesamiento
suma = numero1 + numero2;
resta = numero1 - numero2;
multiplicacion = numero1 * numero2;
division = numero1 / numero2;

// salidas

liSuma.innerHTML = suma;
liResta.innerHTML = resta;
liMultiplicacion.innerHTML = multiplicacion;
liDivision.innerHTML = division;