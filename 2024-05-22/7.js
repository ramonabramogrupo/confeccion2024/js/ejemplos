/**
 * Pedir al usuario el lado de un cuadrado
 * 
 * El programa me tiene que 
 * calcular el area y mostrarlo en su li
 * calcular el perimetro y mostrarlo en su li
 * 
 * quiero que dibuje el cuadrado con svg
 */

const rectangulo = document.querySelector('#rectangulo');
const liLado = document.querySelector('#lado');
const liArea = document.querySelector('#area');
const liPerimetro = document.querySelector('#perimetro');

let lado = 0;
let area = 0;
let perimetro = 0;

lado = Number(prompt('Introduce el lado del cuadrado', 5));

area = lado ** 2;
perimetro = lado * 4;

liLado.innerHTML += lado;
liArea.innerHTML += area;
liPerimetro.innerHTML += perimetro;

rectangulo.setAttribute('width', lado);
rectangulo.setAttribute('height', lado);