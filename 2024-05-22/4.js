/**
 * leer los dos numeros de los dos primeros li
 * sumar esos numeros y depositar el resultado en el 3 li
 */

// variables y constantes

// apuntar a los li
const li1 = document.querySelector('#li1');
const li2 = document.querySelector('#li2');
const li3 = document.querySelector('#li3');

// variables para leer los numeros de los li
let numero1 = 0;
let numero2 = 0;
let suma = 0;

// entradas

// leo los textos de los dos li y los convierto en numeros
numero1 = Number(li1.innerHTML);
numero2 = Number(li2.innerHTML);

// procesamiento
suma = numero1 + numero2;

// salidas

// escribo el resultado en el li3
li3.innerHTML = suma;
