/**
 * Metemos el radio de una circunferencia y calculamos
 * el area y el perimetro de la circunferencia de radio dado
 * 
 * Ese area y ese perimetro lo escribimos en los li
 */

// variables y constantes

// constantes que apunten a los elementos del DOM
const liArea = document.querySelector('#area');
const liPerimetro = document.querySelector('#perimetro');
// elemento que apunta a la etiqueta svg circle
const circulo = document.querySelector('#circulo');

// variables
let radio = 0;
let area = 0;
let perimetro = 0;

// entradas

radio = prompt('Introduce el radio de la circunferencia', 5);
//radio = 5;

// procesamiento

area = Math.PI * Math.pow(radio, 2);
// area = Math.PI * (radio ** 2);

perimetro = 2 * Math.PI * radio;

// salidas

//liArea.innerHTML = liArea.innerHTML + area;
liArea.innerHTML += area;
//liPerimetro.innerHTML = liPerimetro.innerHTML + perimetro;
liPerimetro.innerHTML += perimetro;

circulo.setAttribute('r', radio);


