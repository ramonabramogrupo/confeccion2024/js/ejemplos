/**
 * CAMBIAR EL CONTENIDO DE LOS DOS DIVS UTILIZANDO JS
 */

// declarar variables y constantes

// creo unas constantes para apuntar al DOM
const caja1 = document.querySelector('#uno');
const caja2 = document.querySelector('#dos');

// creo unas variables y las inicializo
let texto1 = "";
let texto2 = "";

// introducir informacion

// leo el texto de las dos cajas
texto1 = caja1.innerHTML;
texto2 = caja2.innerHTML;

// procesar informacion

// mostrar informacion

// intercambio los textos de las dos cajas
caja1.innerHTML = texto2;
caja2.innerHTML = texto1;
