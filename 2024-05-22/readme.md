[[_TOC_]]

# objetivos

Repasar las clases anteriores con conceptos basicos de js.

# Insertar JS dentro de una web

Tenemos dos formas:
- mediante codigo js en la propia pagina

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <script>
        document.write('hola mundo');
    </script>
</body>
</html>
```

- Utilizando un fichero js externo

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <script src="2.js"></script>
</body>

</html>
```
Este seria el fichero 2.js

```js
document.write('hola clase');
```

# Comentarios

Utilizo dos formas
- comentarios de linea (//)
- comentarios de bloque(/* */)

```js
        // aqui coloco codigo js
        
        /*
        aqui puedo colocar 
        codigo js
        */

```

# Pasos generales para js

> Estos pasos son generales para realizar las practicas

- crear archivo html
- crear archivo js
- en el archivo html coloco la estructura basica 
    - Colocando el caracter !
- en el archivo html enlazo el js
```html
<script src="archivo.js"> </script>
```

# Variables

Es un elemento del lenguaje que nos permite almacenar informacion que puede cambiar de forma temporal.

## Declarar 

Para declarar una variable

- let nombreVariable (permite ambito de bloque)
- var nombreVariable (esto no lo vamos a utilizar)

## Identificadores

Los nombres que ponemos a las variables tienen que:
- minusculas
- si la palabra es compuesta utilizamos 
    - minusculas: preciofinal, cajauno
    - camelcase: precioFinal, cajaUno
    - snaquecase: precio_final, caja_uno
- no se pueden utilizar palabras reservadas
- nada de caracteres latinos (ñ,tilde,...)
- no se pueden colocar espacios en blanco

## Inicializar

Es colocar un valor a la variable

```js
iva=12;
nombre="Ramon";
apellidos='Abramo Feijoo';
cansado=true;
```

> Es conveniente que cuando creemos una variable la inicialicemos

```js
let nombre = '';
let total = 0;
let contador = null;
``` 

# Constantes

Es un elemento del lenguaje que almacena un valor de forma temporal, pero ese valor no puede cambiarse.

```js
const PI = 3.14;
const NOMBRE = '<div>';
```

En JS se utilizan las constantes para apuntar a los objetos de html.

```js
// no se utiliza una variable porque podria cambiar el valor
let caja=document.querySelector("#entrada");

// utilizamos una constante
const caja=document.querySelector("#entrada");
```

> Cuando las contantes se utilizan para apuntar a objetos del DOM se utilizan en minusculas.

# DOM (modelo de objetos del documento)

Esto me permite desde JS acceder a todas las etiquetas de la pagina web (HTML)

## Formas de acceder

Podemos utilizar los siguientes metodos de document:

- querySelector(selector)
```js
// apunto al elemento
const elemento=document.querySelector('div')

// cambiar el contenido del elemento
elemento.innerHTML="contenido";
```

- querySelectorAll(selector)
```js
// apunto a una coleccion de elementos
const elementos=document.querySelectorAll('div');

// cambio el contenido del primer elemento del array
elementos[0].innerHTML="contenido";
```

# Estructura basica de guion js

- declarar variables y constantes
>Variables: almacenar temporalmente informacion para poder reutilizarla
>Constantes: Tener un valor fijo para reutilizarle. Apuntar a objetos del DOM
- introducir informacion

    - Metodo prompt
    - Metodo confirm
    - Formularios HTML
    - Etiqueta HTML

- procesar informacion

    - Estudiar todas las instrucciones del lenguaje
    - Estudiar las expresiones
    - Estudiar los metodos y las propiedades de los objetos

- mostrar informacion

    - mostrar la informacion para nosotros en la consola
    - mostrar informacion en una alert
    - mostrar informacion en el documento utilizando document.write
    - mostrar informacion innerHTML

# Convertir textos (string) en numeros (number)

Para realizar esto puedo utilizar los siguiente mecanismos
- parseInt(texto) : convierte el texto en numero entero
- parseFloat(texto) : convierte el texto a numero Real.
- Number(texto) : convierte el texto a numero
- texto * 1

# Objetos

Todo lo que trabaja JS son objetos.

Un objeto es un paradigma programacion donde lo que utilizamos son elementos denominados objetos que estan compuestos de metodos y propiedades.

Un objeto en JS :
- textos (String)
- numero (Number)
- arrays (Array)
- Math (operaciones matematicas)
- DOM (objetos del documento)
- WOM (objetos de la ventana)

## Propiedades

Para acceder a una propiedad

nombreObjeto.propiedad

## Metodo

Para acceder a un metodo

nombreObjeto.metodo(argumentos)

# Acceder a los atributos de una etiqueta

Para poder acceder a los atributos de cualquier etiqueta

- necesito una constante que apunte a la etiqueta
- utilizo para leer el atributo getAttribute
- modificar el atributo setAttribute

```js
const etiqueta=document.querySelector("selector");

// leer
variable=etiqueta.getAttribute("atributo")

// escribir 
etiqueta.setAttribute("atributo",variable);
```









