/**
 * 
 */

// constantes y variables

// creo una constante para cada boton
const izquierda = document.querySelector('#izquierda');
const derecha = document.querySelector('#derecha');
const arriba = document.querySelector('#arriba');
const abajo = document.querySelector('#abajo');
const aumentar = document.querySelector('#aumentar');
const disminuir = document.querySelector('#disminuir');

// escuchador a cada boton
izquierda.addEventListener('click', function (event) {
    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena la coordenada x del display
    let valorIzquierda = 0;

    // entrada

    // obtengo la coordenada x del display actualmente
    // metodo 1
    // if (display.style.left == "") {
    //     valorIzquierda = 0;
    // } else {
    //     valorIzquierda = parseInt(display.style.left);
    // }

    // metodo 2
    // utilizando window.getComputedStyle(elemento)
    valorIzquierda = parseInt(window.getComputedStyle(display).left);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorIzquierda -= 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.left = valorIzquierda + "px";

});

derecha.addEventListener('click', function (event) {
    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena la coordenada x del display
    let valorIzquierda = 0;

    // entrada

    // obtengo la coordenada x del display actualmente
    // metodo 1
    // if (display.style.left == "") {
    //     valorIzquierda = 0;
    // } else {
    //     valorIzquierda = parseInt(display.style.left);
    // }

    // metodo 2
    // utilizando window.getComputedStyle(elemento)
    valorIzquierda = parseInt(window.getComputedStyle(display).left);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorIzquierda += 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.left = valorIzquierda + "px";

});

arriba.addEventListener('click', function (event) {

    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena la coordenada y del display
    let valorSuperior = 0;

    // entrada
    // utilizando window.getComputedStyle(elemento)
    valorSuperior = parseInt(window.getComputedStyle(display).top);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorSuperior -= 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.top = valorSuperior + "px";



});

abajo.addEventListener('click', function (event) {

    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena la coordenada y del display
    let valorSuperior = 0;

    // entrada
    // utilizando window.getComputedStyle(elemento)
    valorSuperior = parseInt(window.getComputedStyle(display).top);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorSuperior += 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.top = valorSuperior + "px";
});

aumentar.addEventListener('click', function (event) {

    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena el ancho
    let valorAncho = 0;

    // entrada
    // utilizando window.getComputedStyle(elemento)
    valorAncho = parseInt(window.getComputedStyle(display).width);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorAncho += 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.width = valorAncho + "px";

});

disminuir.addEventListener('click', function (event) {

    // constantes y variables
    const display = document.querySelector('#display');

    // variable que almacena el ancho
    let valorAncho = 0;

    // entrada
    // utilizando window.getComputedStyle(elemento)
    valorAncho = parseInt(window.getComputedStyle(display).width);

    // procesamiento
    // sumarle 10 a la coordenada x
    valorAncho -= 10;

    // salida
    // mover el display a la nueva coordenada
    display.style.width = valorAncho + "px";

});

// para que funcionen las teclas le coloco un escuchador al body

document.addEventListener('keydown', function (event) {
    // creo una constante que apunte al div que simula la pantalla
    const display = document.querySelector('#display');

    // constante que lea la tecla
    const tecla = event.key;

    // variables
    let valorIzquierda = 0;
    let valorSuperior = 0;

    // entrada
    // leo las posiciones actuales del display
    valorIzquierda = parseInt(window.getComputedStyle(display).left);
    valorSuperior = parseInt(window.getComputedStyle(display).top);

    // procesamiento
    // en funcion de la tecla pulsada cambio el valor de la coordenada
    switch (tecla) {
        case 'ArrowLeft':
            valorIzquierda -= 10;
            break;
        case 'ArrowRight':
            valorIzquierda += 10;
            break;
        case 'ArrowUp':
            valorSuperior -= 10;
            break;
        case 'ArrowDown':
            valorSuperior += 10;
            break;
    }

    // salida

    display.style.left = valorIzquierda + "px";
    display.style.top = valorSuperior + "px";


});