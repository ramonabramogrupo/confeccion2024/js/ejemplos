[[_TOC_]]

# Objetivos

Trabajar con formulario para introducir datos y mostrarlos. Para ello tenemos que comenzar a utilizar eventos y sus manejadores en JS.

Por ultimo comenzamos a estudiar el trabajo con funciones en JS.

# Funciones

Las funciones en JavaScript son bloques de código reutilizables que realizan una tarea específica o calculan un valor. Las funciones permiten estructurar y organizar el código, así como evitar la repetición de código al encapsular lógica que puede ser llamada múltiples veces.

## Tipos de funciones 

- Función Declarada

Las funciones declaradas son las que se definen con la palabra clave function seguida de un nombre, una lista de parámetros entre paréntesis y un bloque de código entre llaves.

```js
function nombreDeLaFuncion(parametro1, parametro2) {
    // Código de la función
    return parametro1 + parametro2;
}

// Llamada a la función
console.log(nombreDeLaFuncion(3, 4)); // 7
```

# Eventos

Cada vez que el usuario interactua sobre la pagina se produce un evento.

Desde JS podemos controlar esos eventos.

Para ello podemos utilizar 3 formas:

- colocar los manejadores de eventos en html 
- colocar los manejadores de eventos en js utilizando directamente el manejador
- colocar los manejadores de eventos en js utilizando addEventListener

## Utilizando los manejadores de eventos en html

Puedes manejar eventos directamente en el HTML usando atributos de eventos como onclick. 

>Este método es el más antiguo y es menos recomendado porque mezcla la lógica JavaScript con el HTML, lo que puede hacer que el código sea más difícil de mantener.

Veamos un ejemplo

```html
<div onclick="funcionLlamada()">
```

```js
function funcionLlamada(){
    alert('pulsado');
}
```

Es decir en las etiquetas de html colocamos los manejadores de eventos y en javascript las funciones a ejecutar cuando el usuario interactue sobre la etiqueta.

```html
<!DOCTYPE html>
<html>
<head>
  <title>Event Handling</title>
</head>
<body>
    
  <button id="myButton" onclick="handleClick()">Pulsa</button>

  <script>
    function handleClick() {
      alert('pulsado');
    }
  </script>
</body>
</html>

```





