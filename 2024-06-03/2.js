/**
 * En un formulario con 2 cajas de tipo numero
 * Colocar 4 botones que nos permitan 
 * sumar
 * restar
 * multiplicar 
 * dividir 
 * 
 * Al pulsar un boton mostrar el resultado lo quiero en un div llamado salida
 */


function sumar() {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 + numero2;

    // salidas

    divSalida.innerHTML = "la suma vale " + salida;

}

function restar() {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 - numero2;

    // salidas

    divSalida.innerHTML = "la resta vale " + salida;

}

function multiplicar() {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 * numero2;

    // salidas

    divSalida.innerHTML = "el producto vale " + salida;

}

function dividir() {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 / numero2;

    // salidas

    divSalida.innerHTML = "el cociente vale " + salida;

}
