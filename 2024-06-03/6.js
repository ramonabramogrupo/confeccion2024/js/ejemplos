/**
 * 
 * Cuando pulso el boton (a) me debe encender (cambiar el color de fondo a amarillo) el div correspondiente al boton
 * 
 * ejemplo:
 *  pulso boton encendersalon ==> fondo amarillo al div id="salon"
 * 
 * Consideraciones:
 * 
 * Solo una funcion llamada encender
 */

function encender(destino) {
    // variables y constantes

    const divDestino = document.querySelector('#' + destino);

    // entradas

    // procesamiento
    // salidas
    divDestino.style.backgroundColor = 'yellow';





}

// function encender(destino) {
//     // variables y constantes
//     const salon = document.querySelector('#salon');
//     const habitacion = document.querySelector('#habitacion');
//     const bano = document.querySelector('#bano');
//     const cocina = document.querySelector('#cocina');

//     // entradas

//     // procesamiento
//     // salida
//     switch (destino) {
//         case 'salon':
//             salon.style.backgroundColor = 'yellow';
//             break;
//         case 'habitacion':
//             habitacion.style.backgroundColor = 'yellow';
//             break;
//         case 'bano':
//             bano.style.backgroundColor = 'yellow';
//             break;
//         case 'cocina':
//             cocina.style.backgroundColor = 'yellow';
//             break;
//     }
// }

// function encender(destino) {
//     switch (destino) {
//         case 'salon':
//             document.querySelector('#salon').style.backgroundColor = 'yellow';
//             break;
//         case 'habitacion':
//             document.querySelector('#habitacion').style.backgroundColor = 'yellow';
//             break;
//         case 'bano':
//             document.querySelector('#bano').style.backgroundColor = 'yellow';
//             break;
//         case 'cocina':
//             document.querySelector('#cocina').style.backgroundColor = 'yellow';
//             break;
//     }

// }