/**
 * Cuando pulso sobre los botones de los numeros
 * me debe ir escribiendo esos numeros en el display
 * 
 * Utilizamos bootstrap 5 para maquetar
 * 
 * No podemos utilizar onclick en html
 * 
 * Tenemos que poner un escuchador a cada boton utilizando un array de botones
 */

// crear una constante que apunte a todos los botones
// un array con todos los botones

// coloco los escuchadores
// con un bucle for of
const botones = document.querySelectorAll('.btn');

for (const boton of botones) {
    boton.addEventListener('click', function (event) {
        // constantes
        const display = document.querySelector('#display');

        // variables
        let numero = "";

        // entradas
        numero = event.target.innerHTML;

        // procesamiento

        // comprobar que el display no tenga solamente un 0
        // en caso de que lo tenga borro el display
        if (Number(display.innerHTML) == 0) {
            display.innerHTML = "";
        }
        // salida
        display.innerHTML += numero;
    });
}