/**
 * un formulario en donde el usuario escribe su nombre y edad
 * y cuando pulsa enviar nuestra funcion debe escribirlo en la consola
 */

/**
 * funcion que se llama cuando pulso el boton del formulario
 * Mostrar en la consola el valor de la caja de texto y del input number
 */
function leerDatos() {

    // variables y constantes
    const inputNombre = document.querySelector('#nombre');
    const inputEdad = document.querySelector('#edad');

    let nombre = "";
    let edad = 0;

    // entradas
    nombre = inputNombre.value;
    edad = inputEdad.value;

    // procesamiento

    // salidas

    console.log(nombre, edad);
}

