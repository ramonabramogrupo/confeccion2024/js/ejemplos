/**
 * Quiero que me permita realizar las operaciones en cada uno de los botones con JS
 * 
 * Condiciones:
 * 
 * Solo una funcion llamada calcular para los 3 botones
 * Esa funcion debe realizar el calculo solicitado y mostrar el resultado en un div denominado salida
 * 
 * NO PUEDO COLOCAR ONCLICK EN HTML
 */

// quiero colocar listeners a los botones

// constantes
const botonCuadrado = document.querySelector('#cuadrado');
const botonCubo = document.querySelector('#cubo');
const botonRaiz = document.querySelector('#raiz');

// escuchadores

botonCuadrado.addEventListener('click', function () {
    calcular('cuadrado');
});

// puedo evitar la constante y colocar escuchador directamente
// document.querySelector('#cuadrado').addEventListener('click', function () {
//     calcular('cuadrado');
// });

botonCubo.addEventListener('click', function () {
    calcular('cubo');
});

botonRaiz.addEventListener('click', function () {
    calcular('raiz');
});



function calcular(operacion) {

    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero');
    let numero1 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);

    // procesamiento
    switch (operacion) {
        case 'cuadrado':
            // salida = numero1 * numero1;
            salida = Math.pow(numero1, 2);
            break;
        case 'cubo':
            //salida = numero1 * numero1 * numero1;
            salida = Math.pow(numero1, 3);
            break;
        case 'raiz':
            salida = Math.sqrt(numero1);
            //salida = Math.pow(numero1, 1 / 2);
            break;
        default:
            salida = 0;
            break;
    }
    // salidas

    divSalida.innerHTML = operacion + ' :' + salida;

}