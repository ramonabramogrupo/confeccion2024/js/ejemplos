/**
 * un formulario en donde el usuario escribe su nombre y edad
 * y cuando pulsa enviar nuestra funcion debe escribirlo en la consola
 * 
 * NO PUEDO PONER ONCLICK EN HTML
 */

/*
tengo que colocar el onclick al boton de enviar
*/
// constantes y variables
const boton = document.querySelector('#boton');


// entradas

// procesamiento

// salidas

// colocar un escuchador al boton que se ejecuta cuando pulso sobre el
// Mostrar en la consola el valor de la caja de texto y del input number
boton.addEventListener('click', function () {
    // variables y constantes
    const inputNombre = document.querySelector('#nombre');
    const inputEdad = document.querySelector('#edad');

    let nombre = "";
    let edad = 0;

    // entradas
    nombre = inputNombre.value;
    edad = inputEdad.value;

    // procesamiento

    // salidas

    console.log(nombre, edad);
});

