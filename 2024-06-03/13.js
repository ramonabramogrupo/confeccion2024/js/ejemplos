// en el primer argumento recibo el objeto event
// dentro de este objeto tengo todas las propiedades del evento
function mostrar(event, objeto) {
    // la propiedad target es el objeto sobre el que 
    // has propucido el evento
    console.log(event.target.innerHTML);
    event.target.style.backgroundColor = 'red';

    // el objeto que recibo como argumento
    // pasado como this 
    // es el elemento sobre el que se ha producido el evento
    console.log(objeto);
}