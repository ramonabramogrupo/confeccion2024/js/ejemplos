/**
 * En un formulario con 2 cajas de tipo numero
 * Colocar 4 botones que nos permitan 
 * sumar
 * restar
 * multiplicar 
 * dividir 
 * 
 * Al pulsar un boton mostrar el resultado lo quiero en un div llamado salida
 * 
 * NO PUEDO PONER ONCLICK EN HTML
 */

// CONSTANTES QUE APUNTEN A LOS BOTONES

const botonSumar = document.querySelector('#sumar');
const botonRestar = document.querySelector('#restar');
const botonMultiplicar = document.querySelector('#multiplicar');
const botonDividir = document.querySelector('#dividir');

// ESCUCHADORES A CADA BOTON

botonSumar.addEventListener('click', function () {

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 + numero2;

    // salidas

    divSalida.innerHTML = "la suma vale " + salida;
});

botonRestar.addEventListener('click', function () {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 - numero2;

    // salidas

    divSalida.innerHTML = "la suma vale " + salida;
});

botonMultiplicar.addEventListener('click', function () {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 * numero2;

    // salidas

    divSalida.innerHTML = "la suma vale " + salida;
});
botonDividir.addEventListener('click', function () {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    salida = numero1 / numero2;

    // salidas

    divSalida.innerHTML = "la suma vale " + salida;
});


