/**
 * Quiero que en el display me coloque lo que vaya escribiendo con las teclas
 * 
 * utilizar listener asignados desde js
 */

document.addEventListener('keydown', function (event) {
    // creo una constante que apunte al div que simula la pantalla
    const display = document.querySelector('#display');

    display.innerHTML += event.key;
});

