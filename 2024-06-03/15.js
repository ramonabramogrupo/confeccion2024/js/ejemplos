// constante que apunte a los botones

// creo un array con todos los botones
const botones = document.querySelectorAll('.botones');

// botones[0].addEventListener('click', function (event) {
//     console.log(event.target.innerHTML);
// });

// botones[1].addEventListener('click', function (event) {
//     console.log(event.target.innerHTML);
// });

// botones[2].addEventListener('click', function (event) {
//     console.log(event.target.innerHTML);
// });

// recorro el array colocando un escuchador a cada boton
for (const boton of botones) {
    boton.addEventListener('click', function (event) {
        console.log(event.target.innerHTML);
    });
}

