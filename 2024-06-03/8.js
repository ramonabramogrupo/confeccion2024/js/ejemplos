/**
 * 
 * Cuando pulso el boton (a) me debe encender (cambiar el color de fondo a amarillo) el div correspondiente al boton
 * 
 * ejemplo:
 *  pulso boton encendersalon ==> fondo amarillo al div id="salon"
 * 
 * Consideraciones:
 * 
 * Solo una funcion llamada encender
 */

function accion(destino) {
    // variables y constantes

    const divDestino = document.querySelector('#' + destino);
    // let color = "";
    // let foto = "";

    let clase = "";


    // entradas

    // procesamiento

    // pregunto si la bombilla esta encendida
    if (divDestino.getAttribute("class") == "encendida") {
        // hay que apagar la bombilla
        // color = "white";
        // foto = "none";

        clase = "apagada";

    } else {
        // hay que encender la bombilla
        // color = "yellow";
        // foto = "url('imgs/bombillaOn.png')";

        clase = "encendida";
    }

    // salidas
    // divDestino.style.backgroundColor = color;
    // divDestino.style.backgroundImage = foto;
    divDestino.setAttribute("class", clase);
    //divDestino.className = clase;

}


