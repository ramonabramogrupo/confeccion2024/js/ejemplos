/**
 * En un formulario con 2 cajas de tipo numero
 * Colocar 4 botones que nos permitan 
 * sumar
 * restar
 * multiplicar 
 * dividir 
 * 
 * Al pulsar un boton mostrar el resultado lo quiero en un div llamado salida
 * 
 * NO PUEDO COLOCAR ONCLICK EN HTML
 */

// CONSTANTES QUE APUNTEN A LOS BOTONES

const botonSumar = document.querySelector('#sumar');
const botonRestar = document.querySelector('#restar');
const botonMultiplicar = document.querySelector('#multiplicar');
const botonDividir = document.querySelector('#dividir');

// ESCUCHADORES A CADA BOTON
botonSumar.addEventListener('click', function () {
    calcular('sumar');
});

botonRestar.addEventListener('click', function () {
    calcular('restar');
});

botonMultiplicar.addEventListener('click', function () {
    calcular('multiplicar');
});

botonDividir.addEventListener('click', function () {
    calcular('dividir');
});

function calcular(operacion) {
    // constantes y variables
    const divSalida = document.querySelector('#salida');
    const inputNumero1 = document.querySelector('#numero1');
    const inputNumero2 = document.querySelector('#numero2');

    let numero1 = 0;
    let numero2 = 0;
    let salida = 0;

    // entradas

    numero1 = Number(inputNumero1.value);
    numero2 = Number(inputNumero2.value);

    // procesamiento

    switch (operacion) {
        case 'sumar':
            salida = numero1 + numero2;
            break;
        case 'restar':
            salida = numero1 - numero2;
            break;
        case 'multiplicar':
            salida = numero1 * numero2;
            break;
        case 'dividir':
            salida = numero1 / numero2;
            break;
        default:
            salida = 0;
    }

    // salidas

    divSalida.innerHTML = operacion + ' : ' + salida;
}


