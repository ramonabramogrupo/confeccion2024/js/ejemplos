/**
 * 
 * Cuando pulso el boton (a) me debe encender (cambiar el color de fondo a amarillo) el div correspondiente al boton
 * 
 * ejemplo:
 *  pulso boton encendersalon ==> fondo amarillo al div id="salon"
 * 
 * Consideraciones:
 * 
 * Solo una funcion llamada encender
 */

function accion(destino) {
    // variables y constantes

    const divDestino = document.querySelector('#' + destino);
    let color = "";


    // entradas

    // procesamiento


    if (divDestino.style.backgroundColor == 'yellow') {
        color = "white";

    } else {
        color = "yellow";

    }

    // salidas
    divDestino.style.backgroundColor = color;







}


