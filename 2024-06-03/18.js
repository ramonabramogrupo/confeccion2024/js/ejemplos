/**
 * Cuando pulse en los botones tiene que realizar las siguientes funciones
 * 
 * rojo: cambiar el color de fondo a rojo
 * verde: cambiar el color de fondo a verde
 * azul : cambiar el color de fondo a azul
 * mostrar : muestra el div display
 * ocultar: oculta el div display
 * 
 * Cuando escriba con el teclado el texto se va colocando en display
 */

// constantes y variables

// creo una constante para cada boton
const rojo = document.querySelector('#rojo');
const azul = document.querySelector('#azul');
const verde = document.querySelector('#verde');
const mostrar = document.querySelector('#mostrar');
const ocultar = document.querySelector('#ocultar');


// colocar un escuchador al body para que cuando pulse una tecla
// me la escriba en el display
document.addEventListener('keydown', function (event) {

    const display = document.querySelector('#display');

    display.innerHTML += event.key;


});


// colocar un escuchador a cada boton
rojo.addEventListener('click', function () {
    const display = document.querySelector('#display');
    display.style.backgroundColor = 'red';
});

azul.addEventListener('click', function () {
    const display = document.querySelector('#display');
    display.style.backgroundColor = 'blue';
});

verde.addEventListener('click', function () {
    const display = document.querySelector('#display');
    display.style.backgroundColor = 'green';
});

mostrar.addEventListener('click', function () {
    const display = document.querySelector('#display');
    display.style.opacity = 1;
});

ocultar.addEventListener('click', function () {
    const display = document.querySelector('#display');
    display.style.opacity = 0;
});

