/**
 * Cuando pulso sobre los botones de los numeros
 * me debe ir escribiendo esos numeros en el display
 * 
 * Utilizamos bootstrap 5 para maquetar
 * 
 * Utilizamos el onclick en html llamando sola a una funcion denominada escribir
 * 
 * Pasamos el objeto event como argumento en la funcion para identificar todos los datos del evento
 */
function escribir(event) {
    // constantes
    const display = document.querySelector('#display');

    // variables
    let numero = "";

    // entradas
    numero = event.target.innerHTML;

    // procesamiento

    // comprobar que el display no tenga solamente un 0
    // en caso de que lo tenga borro el display
    if (Number(display.innerHTML) == 0) {
        display.innerHTML = "";
    }
    // salida
    display.innerHTML += numero;

}