let numero1 = 10;
let numero2 = 4;
let mayor = 0;

const item1 = document.querySelector('#item1');
const item2 = document.querySelector('#item2');

item1.innerHTML = numero1;
item2.innerHTML = numero2;

// quiero que me indique cual es el numero mayor

// if (condicion) {
//     si la condicion es true
// } else {
//     si la condicion es false
// }

// if (condicion) {
//     si la condicion es true
// }

if (numero1 > numero2) {
    mayor = numero1;
} else {
    mayor = numero2;
}

// muestro el numero mas grande
alert(mayor);

// me muestra algo en la consola de errores
console.log(numero1, numero2, mayor)

