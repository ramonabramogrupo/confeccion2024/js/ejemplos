// necesito una constante
// que apunte a cada elemento de html
// que quiero cambiar

const titulo = document.querySelector('h1');
const caja = document.querySelector('#caja');

// creo dos variables que contengan la
// informacion a mostrar

let tituloPrincipal = 'Ejemplo';
let texto = "Ramon Abramo";

// escribo el contenido en h1
titulo.innerHTML = tituloPrincipal;

// escribo el contenido en div
caja.innerHTML = texto;


