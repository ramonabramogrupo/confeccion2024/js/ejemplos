// aqui escribo codigo js

// declarar una variable
// var nombreVariable=valorInicial;
// let nombreVariable=valoricial;
let texto = "hola clase"; // instruccion de js

// declarar una constante
// const nombreConstante=valor;

// creo una constante que apunte al div caja
const caja = document.querySelector('#caja');

// quiero escribir el valor de texto en el body
//document.write(texto);

// quiero escribir el valor de la variable texto en el div
caja.innerHTML = texto;

// quiero cambiar aspectos visuales del div
caja.style.backgroundColor = "gray";
caja.style.color = "white";