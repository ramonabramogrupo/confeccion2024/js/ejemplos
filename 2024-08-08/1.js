// ejercicio realizado utilizando clases
// es mas versatil porque puedo utilizarlo para otros ejercicios

/**
 * Clase que recibe datos en formato JSON y los muestra en una tabla
 * La tabla tiene que estar realizada en html
 */
class Data {
    constructor(datos) {
        this.datos = datos;
    }
    /**
     * metodo que genera filas en el tbody de una tabla creada en html
     * @param {texto} elemento  : selector del elemento donde añado las filas
     * @param {array} campos  : nombre de los campos a mostrar
     */
    render(elemento, campos) {
        const destino = document.querySelector(elemento);

        destino.innerHTML = '';
        // recorro el array de datosJS
        this.datos.forEach(objeto => {
            const fila = document.createElement('tr');

            campos.forEach(campo => {
                const celda = document.createElement('td');
                celda.textContent = objeto[campo];
                fila.appendChild(celda);
            });

            destino.appendChild(fila);
        });
    };

    /**
     * Añadir un nuevo registro al array (this.datos)
     * @param {element} id 
     * @param {element} nombre 
     */
    add(id, nombre) {
        let vnombre = nombre.value;
        let vid = parseInt(id.value);

        this.datos.push({
            id: vid,
            nombre: vnombre
        });

        // por si quieres limpiar los input
        // id.value = "";
        // nombre.value = "";
    }

}


// recibo los datos del servidor en formato JSON
const datos = '[{"id":1,"nombre":"Juan"},{"id":2,"nombre":"Pedro"},{"id":3,"nombre":"Maria"}]';

// convertir el texto en formato JSON a objeto de js
const datosJS = JSON.parse(datos); // [{id:1,nombre:Juan},{id:2,nombre:Pedro},{id:3,nombre:Maria}]

// creo un objeto de tipo Data
const datosMostrar = new Data(datosJS);

// llamo al metodo render para dibujar los registros
datosMostrar.render('#tbody', ['id', 'nombre']);

// constante que apunte al boton
const boton = document.querySelector('button');

// colocar escuchador
// añadir registros a mis datos
boton.addEventListener('click', () => {

    // constante que apunte a los inputs
    const id = document.querySelector('#id');
    const nombre = document.querySelector('#nombre');
    //añadir el registro al array

    // llamo al metodo add que me tiene que añadir el registro
    datosMostrar.add(id, nombre);

    // dibujo los datos en la tabla
    datosMostrar.render('#tbody', ['id', 'nombre']);
});
