// datos que me llegan desde algun sitio
const datos = '[{"id":1,"nombre":"Juan","nombreUsuario":"JuanLopez","telefono":"654654654"},{"id":2,"nombre":"Pedro","nombreUsuario":"PedroLopez","telefono":"654654654"},{"id":3,"nombre":"Maria","nombreUsuario":"MariaLopez","telefono":"654654654"}]'; // string

// datos[0] // '['

// convertir el string (JSON) a un array de objetos de javascript
// utilizando parse

const datosJS = JSON.parse(datos); // array de objetos de js
// datosJS[0] //{id:1,nombre:"Juan",nombreUsuario:"JuanLopez",telefono:"654654654"}


/**
 * Clase que recibe datos en formato JSON y los muestra en una tabla
 * La tabla tiene que estar realizada en html
 */
class Data {
    constructor(datos) {
        this.datos = datos;
    }
    /**
     * metodo que genera filas en el tbody de una tabla creada en html
     * @param {texto} elemento  : selector del elemento donde añado las filas
     * @param {array} campos  : nombre de los campos a mostrar
     */
    render(elemento, campos) {
        const destino = document.querySelector(elemento);

        destino.innerHTML = '';
        // recorro el array de datosJS
        this.datos.forEach(objeto => {
            const fila = document.createElement('tr');

            campos.forEach(campo => {
                const celda = document.createElement('td');
                celda.textContent = objeto[campo];
                fila.appendChild(celda);
            });

            destino.appendChild(fila);
        });
    };

    /**
     * Añadir un nuevo registro al array (this.datos)
     * @param {array} campos : Nombre de los campos a utilizar 
     */
    add(campos) {
        const objeto = {};

        campos.forEach(valor => {
            objeto[valor] = document.querySelector("#" + valor).value;
        });

        this.datos.push(objeto);

    }

}

// el programa principal

//1- mostrar el array de datos en la tabla
// utilizando render

const datosMostrar = new Data(datosJS);

datosMostrar.render('#tbody', ['id', 'nombre', 'nombreUsuario', 'telefono']);


//2- Colocar un escuchador al boton para añadir un registro
// utilizando el add

const boton = document.querySelector('button');

boton.addEventListener('click', () => {
    datosMostrar.add(['id', 'nombre', 'nombreUsuario', 'telefono']);

    datosMostrar.render('#tbody', ['id', 'nombre', 'nombreUsuario', 'telefono']);
});