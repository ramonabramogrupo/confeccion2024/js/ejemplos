// ejercicio realizado utilizando clases
// es mas versatil porque puedo utilizarlo para otros ejercicios

/**
 * Clase que recibe datos en formato JSON y los muestra en una tabla
 * La tabla tiene que estar realizada en html
 */
class Data {
    constructor(datos) {
        this.datos = datos;
    }
    /**
     * metodo que genera filas en el tbody de una tabla creada en html
     * @param {texto} elemento  : selector del elemento donde añado las filas
     * @param {array} campos  : nombre de los campos a mostrar
     */
    render(elemento, campos) {
        const destino = document.querySelector(elemento);
        // recorro el array de datosJS
        this.datos.forEach(objeto => {
            const fila = document.createElement('tr');

            campos.forEach(campo => {
                const celda = document.createElement('td');
                celda.textContent = objeto[campo];
                fila.appendChild(celda);
            });

            destino.appendChild(fila);
        });
    };

}


const url = 'https://jsonplaceholder.typicode.com/users';


/** 
 * recuperar datos
 */
// fetch(url)
//     .then(response => response.json())
//     .then(datos => {
//         // aqui tu programa
//         console.log(datos);
//     });

fetch(url)
    // .then(response => response.text()) // te lo devuelve en formato texto en formato JSON
    .then(response => response.json()) // te lo devuelve en formato JSON convertido a array o objeto de js
    .then(datosJS => {
        console.log(datosJS);

        //codigo principal

        // opcion 1
        // utilizando la clase
        const datosMostrar = new Data(datosJS);
        datosMostrar.render('#tbody', ['id', 'name', 'username', 'phone']);

        // opcion 2
        // sin clase
        // const tbody = document.querySelector('#tbody');

        // datosJS.forEach(dato => {
        //     const fila = document.createElement('tr');
        //     const celdas = [];

        //     celdas[0] = document.createElement('td');
        //     celdas[1] = document.createElement('td');
        //     celdas[2] = document.createElement('td');
        //     celdas[3] = document.createElement('td');

        //     celdas[0].textContent = dato.id;
        //     celdas[1].textContent = dato.name;
        //     celdas[2].textContent = dato.username;
        //     celdas[3].textContent = dato.phone;

        //     celdas.forEach(celda => {
        //         fila.appendChild(celda);
        //     });

        //     tbody.appendChild(fila);

        // });

    })


// otro metodo de realizar el fetch
// async function recolectarDatos(url) {
//     const response = await fetch(url);
//     const data = await response.json();
//     return data;
// }

// // recibo los datos del servidor en formato JSON

// recolectarDatos(url).then(datos => {
//     console.log(datos);
// });


