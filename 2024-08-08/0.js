// ejercicio realizado sin utilizar clases
// sirve para este ejercicio

// recibo los datos del servidor en formato JSON
const datos = '[{"id":1,"nombre":"Juan"},{"id":2,"nombre":"Pedro"},{"id":3,"nombre":"Maria"}]'; // STRING

// convertir el texto en formato JSON a objeto de js
const datosJS = JSON.parse(datos); // [{id:1,nombre:Juan},{id:2,nombre:Pedro},{id:3,nombre:Maria}] // ARRAY

const tbody = document.querySelector('#tbody');

datosJS.forEach(dato => {
    const fila = document.createElement('tr');
    const celdas = [];

    celdas[0] = document.createElement('td');
    celdas[1] = document.createElement('td');

    celdas[0].textContent = dato.id;
    celdas[1].textContent = dato.nombre;

    celdas.forEach(celda => {
        fila.appendChild(celda);
    });

    tbody.appendChild(fila);

});

// constante que apunte al boxDecorationBreak: 
const boton = document.querySelector('button');

// escuchador al boton
// añadir un nuevo registro a los datos
// mostrarlo en la tabla

boton.addEventListener('click', () => {

    // constante que apunte a los inputs
    const id = document.querySelector('#id');
    const nombre = document.querySelector('#nombre');

    // añadir el registro al array
    datosJS.push({
        id: id.value,
        nombre: nombre.value
    });

    // vacio el tbody
    tbody.innerHTML = '';

    // muestro todos los datos
    datosJS.forEach(dato => {
        const fila = document.createElement('tr');
        const celdas = [];

        celdas[0] = document.createElement('td');
        celdas[1] = document.createElement('td');

        celdas[0].textContent = dato.id;
        celdas[1].textContent = dato.nombre;

        celdas.forEach(celda => {
            fila.appendChild(celda);
        });

        tbody.appendChild(fila);

    });

});